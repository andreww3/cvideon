<?php

namespace App;

use Spatie\SearchIndex\Searchable;

class Seeker extends BaseModel implements Searchable
{
    protected $fillable = ['job_title', 'status_quo', 'goal'];

    public function user(){
        return $this->morphOne('App\User', 'userable');
    }


    /**
     * Returns an array with properties which must be indexed.
     *
     * @return array
     */
    public function getSearchableBody()
    {
        $user = $this->user;


        $inputs = [
            $user->name,
            $user->email,
            $user->category->name,
            $user->tags->lists('name'),
            $this->job_title,
            $this->status_quo,
        ];

        return [
            'name' => $user->name,
            'email' => $user->email,
            'tags' => $user->tags,
            'category' => $user->category,
            'job_title' => $this->job_title,
            'status_quo' => $this->status_quo,
            'suggester' => [
                'input' => $inputs,
                'output' => $user->name,
                'payload' => [
                    'id' => $this->id,
                    'profile_picture' => $user->profile_picture,
                    'name' => $user->name,
                    'tags' => $user->tags,
                    'category' => $user->category,
                    'job_title' => $this->job_title,
                    'status_quo' => $this->status_quo,
                ],
            ]];
    }

    /**
     * Return the type of the searchable subject.
     *
     * @return string
     */
    public function getSearchableType()
    {
        return 'seeker';
    }

    /**
     * Return the id of the searchable subject.
     *
     * @return string
     */
    public function getSearchableId()
    {
        return $this->id;
    }

}
