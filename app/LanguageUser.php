<?php

namespace App;

class LanguageUser extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'language_user';

    protected $fillable = ['language_id', 'user_id'];


}
?>