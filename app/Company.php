<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\SearchIndex\Searchable;

class Company extends BaseModel implements Searchable
{
    use SoftDeletes;

    protected $fillable = ['name', 'title', 'description', 'website', 'number_of_employees'];

    public function user()
    {
        return $this->hasMany('App\User');
    }
 
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Returns an array with properties which must be indexed.
     *
     * @return array
     */
    public function getSearchableBody()
    {
        $inputs = [
            $this->name,
            $this->website,
            $this->title,
            $this->description
        ];

        return [
            'name' => $this->name,
            'title' => $this->title,
            'description' => $this->description,
            'website' => $this->website,
            'suggester' => [
                'input' => $inputs,
                'output' => $this->name,
                'payload' => [
                    'id' => $this->id,
                    'name' => $this->name,
                    'website' => $this->website,
                    'description' => $this->description
                ]
            ]
        ];
    }

    /**
     * Return the type of the searchable subject.
     *
     * @return string
     */
    public function getSearchableType()
    {
        return 'company';
    }

    /**
     * Return the id of the searchable subject.
     *
     * @return string
     */
    public function getSearchableId()
    {
        return $this->id;
    }
}
