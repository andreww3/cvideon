<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\SearchIndex\Searchable;

class Tag extends BaseModel implements Searchable
{
    use SoftDeletes;

    protected $table = 'tags';

    protected $fillable = ['tag_type_id', 'name'];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function tagType(){
        return $this->belongsTo('App\TagType');
    }

    public function users()
    {
        return $this->morphedByMany('App\User', 'taggable');
    }

    /**
      * The many-to-many relationship between tags and posts.
      *
     *  @return BelongsToMany
    **/
    public function posts()
    {
        return $this->belongsToMany('App\Post', 'post_tag_pivot');
    }


    /**
      * Add any tags needed from the list
      *
     * @param array $tags List of tags to check/add
    */
    public static function addNeededTags(array $tags)
    {
        if (count($tags) === 0) {
          return;
        }

        $found = static::whereIn('name', $tags)->lists('name')->all();

        foreach (array_diff($tags, $found) as $tag) {
          static::create([
            'name' => $tag,
            'tag_type_id' => '5'
          ]);
        }
    }

    public function getSearchableBody()
    {
        $inputs = [
            $this->name,
        ];

        return [
            'name' => $this->name,
            'tag_type' => $this->tagType,
            'suggester' => [
                'input' => $inputs,
                'output' => $this->name,
                'payload' => [
                    'id' => $this->id,
                    'name' => $this->name,
                    'type' => $this->tagType
                ],
                'context' => [
                    'type' => [
                        "any_tag",
                        "{$this->tagType->name}_tag"
                    ]
                ]
            ]];
    }

    public function getSearchableType()
    {
        return 'tag';
    }

    public function getSearchableId()
    {
        return $this->id;
    }
}

