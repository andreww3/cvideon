<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Experience extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['title', 'description', 'started_at', 'ended_at'];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function experienceType()
    {
        return $this->belongsTo('App\ExperienceType');
    }
}
