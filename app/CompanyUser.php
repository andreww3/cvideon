<?php

namespace App;


class CompanyUser extends BaseModel
{
    protected $fillable = ['company_id', 'is_active', 'is_admin'];

    protected $with = ['company'];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function user(){
        return $this->morphOne('App\User', 'userable');
    }

}
