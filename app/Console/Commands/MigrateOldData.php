<?php

namespace App\Console\Commands;

use App\Category;
use App\Company;
use App\CompanyUser;
use App\Country;
use App\Experience;
use App\ExperienceType;
use App\Language;
use App\Seeker;
use App\SubCategory;
use App\Tag;
use App\TagType;
use App\User;
use App\Video;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateOldData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cvideon:migrate_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
        {
        if (!Language::count()) {
            $json = file_get_contents('http://data.okfn.org/data/core/language-codes/r/language-codes.json');

            if (!$list = json_decode($json))
                Throw new \Exception('Language list JSON not valid');

            foreach ($list as $item) {
                Language::firstOrCreate(['name' => $item->English, 'locale' => explode(";", $item->alpha2)[0]]);
            }
        }

        if (!Country::count()) {
            $json = file_get_contents('https://restcountries.eu/rest/v1/all');

            if (!$list = json_decode($json))
                Throw new \Exception('Country list JSON not valid');

            foreach ($list as $item) {
                $country = Country::firstOrCreate(['name' => $item->name]);

                $ids = [];

                foreach ($item->languages as $index => $locale) {
                    $ids[] = Language::firstOrCreate(['locale' => $locale])->id;
                }

                $country->languages()->attach($ids);
            }
        }

        $work = ExperienceType::firstOrCreate(['name' => 'work']);
        $education = ExperienceType::firstOrCreate(['name' => 'education']);


        foreach (DB::connection('mysql2')->select('select * from jobCategories') as $item) {
            Category::firstOrCreate(['name' => $item->name]);
        }

        foreach (DB::connection('mysql2')->select('select * from subCategories') as $item) {
            SubCategory::firstOrCreate(['category_id' => $item->fromCategory, 'name' => $item->subCategoryName]);
        }

        foreach (DB::connection('mysql2')->select('select * from seekers') as $item) {
            $user = User::firstOrNew(['email' => $item->email]);

            $user->fill([
                'name' => trim("{$item->firstName} {$item->lastName}"),
                'password' => bcrypt($item->password),
                'profile_picture' => $item->profilePic,
                'cover_picture' => $item->profileCover
            ]);

            if (!$category = Category::find($item->MainCategory))
                $category = Category::first();

            $user->category()->associate($category);

            if (!$country = Country::where('name', 'LIKE', $item->Country)->first())
                $country = Country::where('name', 'Denmark')->first();

            $user->country()->associate($country);

            $user->created_at = $item->signUpTime == '0000-00-00 00:00:00' ? Carbon::now() : Carbon::parse($item->signUpTime);

            $languages = preg_replace("[a-zA-Z]", '', str_replace("&", ",", str_replace(" ", "", $item->Languages)));

            $languages = explode(',', $languages);

            foreach ($languages as $index => $language) {
                $languages[$index] = ucfirst(strtolower($language));
            }

            $languages = Language::whereIn('name', $languages)->lists('id')->toArray();

            if($user->save())
            {
                $user->languages()->sync($languages);
                
                $seeker = Seeker::create([
                    'job_title' => $item->jobTitle,
                    'status_quo' => $item->actualSituation,
                    'goal' => $item->goal,
                ]);

                $seeker->user()->save($user);
            }

        }

        foreach (DB::connection('mysql2')->select('select workExperience.*, email from workExperience inner join seekers on fromProfile = seekers.id') as $item) {
            if ($user = User::where('email', 'LIKE', $item->email)->first()) {
                $experience = Experience::firstOrNew([
                    'title' => $item->position,
                    'description' => $item->company,
                ]);

                $experience->user()->associate($user);
                $experience->experienceType()->associate($work);
                $experience->save();
            }

        }

        foreach (DB::connection('mysql2')->select('select EducationExperience.*, email from EducationExperience inner join seekers on fromProfile = seekers.id') as $item) {
            if ($user = User::where('email', 'LIKE', $item->email)->first()) {
                $experience = Experience::firstOrNew([
                    'title' => $item->title,
                    'description' => $item->institute,
                ]);

                $experience->user()->associate($user);
                $experience->experienceType()->associate($education);
                $experience->save();
            }

        }

        foreach (DB::connection('mysql2')->select('select * from companies') as $item) {
            $company = Company::firstOrNew(['name' => $item->companyName]);

            $company->fill([
                'title' => $item->companyTitle,
                'description' => $item->description,
                'website' => $item->webpage,
                'number_of_employees' => $item->nrOfEmployees
            ]);

            if ($item->signUpTime != '0000-00-00 00:00:00')
                $company->created_at = Carbon::parse($item->signUpTime);

            $company->save();

            $user = User::firstOrNew(['email' => $item->email]);

            $user->fill([
                'profile_picture' => $item->profilePic,
                'cover_picture' => $item->profileCover,
                'password' => bcrypt($item->password)
            ]);

            if (!$country = Country::where('name', 'LIKE', $item->Country)->first())
                $country = Country::where('name', 'Denmark')->first();

            if(!$user->id)
            {
                $company_user = CompanyUser::create([
                    'company_id' => $company->id,
                    'is_admin' => 1,
                    'is_active' => 1
                ]);
            }

            $user->created_at = $company->created_at;

            if (!$category = Category::find($item->mainSector))
                $category = Category::first();

            $user->category()->associate($category);
            $user->country()->associate($country);

            if($user->save() && isset($company_user))
            {
                $company_user->user()->save($user);
            }
        }

        $personal = TagType::firstOrCreate(['name' => 'personal']);
        $professional = TagType::firstOrCreate(['name' => 'professional']);
        $job = TagType::firstOrCreate(['name' => 'job']);
        $company = TagType::firstOrCreate(['name' => 'company']);

        foreach (DB::connection('mysql2')->select('select * from personalKeywords') as $item) {
            Tag::firstOrCreate([
                'tag_type_id' => $personal->id,
                'name' => $item->keyword
            ]);
        }

        foreach (DB::connection('mysql2')->select('select * from professionalKeywords') as $item) {
            Tag::firstOrCreate([
                'tag_type_id' => $professional->id,
                'name' => $item->keyword
            ]);
        }

        foreach (DB::connection('mysql2')->select('select seekerKeywords.*, email from seekerKeywords left join seekers on fromProfile = seekers.id') as $item) {
            $tag = Tag::firstOrCreate([
                'tag_type_id' => $item->keywordType ? $professional->id : $personal->id,
                'name' => $item->keyword
            ]);

            $user = User::where('email', 'LIKE', $item->email)->whereHas('tags', function ($q) use ($tag) {
                $q->where('tags.id', $tag->id);
            }, '=', 0)->first();

            if ($user) {
                $user->tags()->save($tag);
            }
        }

        foreach (DB::connection('mysql2')->select('select seekerVideos.*, email from seekerVideos join seekers on fromProfile = seekers.id') as $item) {
            if($user = User::where('email', 'LIKE', $item->email)->first())
            {
                $video = Video::firstOrCreate(['key' => 'path', 'value' => $item->fileName]);
                $user->videos()->save($video);
            }
        }

        foreach (DB::connection('mysql2')->select('select embeddedCompanyVideos.*, email from embeddedCompanyVideos join companies on fromProfile = companies.id') as $item) {
            if($user = User::where('email', 'LIKE', $item->email)->first())
            {
                $video = Video::firstOrCreate(['key' => 'url', 'value' => $item->link]);
                $user->videos()->save($video);
            }
        }
    }
}
