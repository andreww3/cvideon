<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends BaseModel
{

    protected $table = 'blog_categories';

    protected $fillable = ['name'];


    /**
      * The many-to-many relationship between tags and posts.
      *
     *  @return BelongsToMany
    **/
    public function posts()
    {
        return $this->belongsToMany('App\Post');
    }


}
