<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends BaseModel
{
    use SoftDeletes;

    protected $table = 'videos';

    protected $fillable = ['key', 'value'];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function videoable()
    {
        return $this->morphTo();
    }

    public function getValueAttribute($value)
    {
        return $this->key == 'path' ? (Helpers::getSignedS3URL("videos/{$value}") ?: $value) : $value;
    }

}
