<?php

namespace App;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class Helpers
    {

    /**
     * Return sizes readable by humans
     */
    function human_filesize($bytes, $decimals = 2)
    {
      $size = ['B', 'kB', 'MB', 'GB', 'TB', 'PB'];
      $factor = floor((strlen($bytes) - 1) / 3);

      return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) .
          @$size[$factor];
    }

    /**
     * Is the mime type an image
     */
    function is_image($mimeType)
    {
        return starts_with($mimeType, 'image/');
    }

    /**
     * Amazon upload helpers
    */
    public static function getSignedS3URL($URL, $limit = "+10 minutes")
    {
        if(!Storage::exists($URL))
            return null;

        $client = Storage::disk('s3')->getDriver()->getAdapter()->getClient();
        $command = $client->getCommand('GetObject', [
            'Bucket' => Config::get('filesystems.disks.s3.bucket'),
            'Key' => $URL
        ]);

        return (string) $client->createPresignedRequest($command, $limit)->getUri();
    }

    public static function getPublicS3URL($URL)
    {
        if(!Storage::exists($URL))
            return null;

        $client = Storage::disk('s3')->getDriver()->getAdapter()->getClient();

        return $client->getObjectUrl(Config::get('filesystems.disks.s3.bucket'), $URL);
    }
    
}