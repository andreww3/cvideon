<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function getAllCategories()
    {
        return Category::with(['subCategories'])->get();
    }

    public function postSubCategoriesByCategoryId(Request $request)
    {
        return SubCategory::where('category_id', $request->get('category_id'))->get();
    }

    public function getAllSubCategories()
    {
        return SubCategory::get();
    }
}