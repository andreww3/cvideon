<?php

namespace App\Http\Controllers;

use App\Post;
use App\BlogCategory;
use Carbon\Carbon;

class BlogController extends Controller
{
    public function index()
    {

        $categories = BlogCategory::all();

        return view('blog.index', compact('categories'));
    
    }

    public function show6()
    {
        $posts = Post::where('published_at', '<=', Carbon::now())
            ->orderBy('published_at', 'desc')->limit(6)->get();


        return $posts;
    
    }

    public function showAll()
    {
        $posts = Post::where('published_at', '<=', Carbon::now())
            ->orderBy('published_at', 'desc')->get();

        return $posts;
    
    }

    public function showPost($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        $posts = Post::orderBy('published_at', 'desc')->take(6)->get();

        $categories = BlogCategory::all();

        return view('blog.post')->withPost($post)->withPosts($posts)->withCategories($categories);
    }

}
