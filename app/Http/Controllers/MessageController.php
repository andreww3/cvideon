<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use Mail;
use App\Message;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /* * *
        * * * Get Sender & Receiver basic info * * *
                                                  * * */

    public function getUsers (Request $request) 
    {
        $messageId = $request->all();

        if ( ! $message = Message::where('id', $messageId)->first() )
            
            return 'Non-existing message';


        if ( ! $sender = User::with(['userable', 'company'])
                                ->where('id', $message->sender_id)->first() )
            
            return 'Non-existing sender??';

        if ( ! $receiver = User::with(['userable', 'company'])
                                ->where('id', $message->receiver_id)->first() )
            
            return 'Non-existing sender??';

        if ( $sender->userable->company ) {

            $senderCompany = $sender->userable->company->name;
        
        } else {

            $senderCompany = NULL;

        }

        if ( $receiver->userable->company ) {

            $receiverCompany = $receiver->userable->company->name;
        
        } else {

            $receiverCompany = NULL;

        }
             

            $messageInfo = [
                'subject' => $message->subject,
                'message' => NULL,
                'receiver' =>
                    [
                        'id' => $sender->id,
                        'email' => $sender->email,
                        'name' => $sender->name,
                        'company' => $senderCompany
                    ],

                'sender' =>
                    [
                        'id' => $receiver->id,
                        'name' => $receiver->name,
                        'company' => $receiverCompany
                    ],
            ];

        return json_encode($messageInfo); 
    }


    /**
     * Getting the last message..
     *
     * @return \Illuminate\Http\Response
     */
    public function lastMessage (Request $request)
    {
        $id = $request->all();

        if ( $message = Message::with(['sender', 'receiver'])
                                ->orderBy('created_at', 'desc')
                                ->where('receiver_id', $id)
                                ->where('read', '=', 0)->first() )
            
            return $message;
        
        if ( ! $message )
            return 0;

    }
    
    /**
     * Getting the inbox messages..
     *
     * @return \Illuminate\Http\Response
     */
    public function getUnreadMessages (Request $request)
    {
        $id = $request->all();

        if( $messages = Message::with(['sender', 'receiver'])
                                ->orderBy('created_at', 'desc')
                                ->where('read', '=', 0)
                                ->where('receiver_id', $id)->count() )
        
            return $messages;

        if ( ! $messages )
            return 0;

    }

    /**
     * Getting the inbox messages..
     *
     * @return \Illuminate\Http\Response
     */
    public function inbox (Request $request)
    {
        $id = $request->all();

        if( $messages = Message::with(['sender', 'receiver'])
                                ->orderBy('created_at', 'desc')
                                ->where('receiver_id', $id)->get() )
            return $messages;

        if ( ! $messages )
            return 0;

    }

    /**
     * Getting the send messages..
     *
     * @return \Illuminate\Http\Response
     */
    public function sentbox (Request $request)
    {
        $id = $request->all();

        if( $messages = Message::with(['sender', 'receiver'] )
                                ->orderBy('created_at', 'desc')
                                ->where('sender_id', $id)->get() )

            return $messages;
        
        if ( ! $messages )
            return 0;
    }

    /**
     * Marking message as read..
     *
     * @return \Illuminate\Http\Response
     */
    public function markRead (Request $request)
    {
        $id = $request->all();

        if( $theMessage = Message::where('id', $id)->first() )
            $theMessage->read = 1;
            $theMessage->save();            
            return $theMessage;


        if ( ! $theMessage )
            return 'Message not found';

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request)
    {

        $input = $request->all();

        $message = new Message;
        $message->fill([
            'sender_id' => $input['sender']['id'],
            'receiver_id' => $input['receiver']['id'],
            'subject' => $input['subject'],
            'message' => $input['message']
        ]);
        
        $message->save();

        \Mail::send('emails.messageReceived', 
                array(
                'subject' => $input['subject'],
                'message' => $input['message'],
                'receiverName' => $input['receiver']['name'],
                'receiverCompany' => $input['receiver']['company'],
                'senderName' => $input['sender']['name'],
                'senderCompany' => $input['sender']['company']
                ), 

            function($message) use ($input) {
                $message->from('adm@cvideon.com', 'CVideon')
                         ->to($input['receiver']['email'], 
                            $input['receiver']['name'])
                        ->subject('You got a new message. | Subject: '
                            .$input['subject']);
            }
        );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Delete message from database
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $id = $request->all();

        $message = Message::findOrFail($id)->first();

        $message->delete();
            
    }
}
