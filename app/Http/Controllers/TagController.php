<?php

namespace App\Http\Controllers;

use App\Tag;

class TagController extends Controller
{
    public function showPersonalTags()
    {
    	return $tags = Tag::select('id', 'name')->where('tag_type_id', '1')->get();
    }

    public function showProfessionalTags()
    {
    	return $tags = Tag::select('id', 'name')->where('tag_type_id', '2')->get();
    }

    public function showCompanyTags()
    {
    	return $tags = Tag::select('id', 'name')->where('tag_type_id', '4')->get();
    }
}