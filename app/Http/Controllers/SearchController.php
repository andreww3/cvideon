<?php

namespace App\Http\Controllers;

use DB;

use App\Category;
use App\Company;
use App\Language;
use App\Seeker;
use App\SubCategory;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Spatie\SearchIndex\SearchIndexFacade;

class SearchController extends Controller
{
    protected $search_client;

    public function __construct()
    {
        $this->search_client = SearchIndexFacade::getClient();
    }
    
    /*
    public function getIndex()
    {
        return view('search.test');
    }
    */

    public function getSeekers()
    {
        return view('search.seekers');
    } 

    public function getCompanies()
    {
        return view('search.companies');
    } 
    
    public function randomUserProfiles()
    {
        return User::with(['userable', 'country','videos'])->where('userable_type', 'App\Seeker')->has('videos')->get()->random(5);
    }

    public function randomCompanyProfiles()
    {
        return User::with(['userable', 'country','videos'])->where('userable_type', 'App\CompanyUser')->has('videos')->get();
    }
    
    public function postSuggest(Request $request)
    {
        $params = [
            'index' => 'main',
            'body' =>
                [
                    'text' => $request->get('text', ''),
                    'suggestions' =>
                        [
                            'completion' => [
                                'field' => 'suggester',
                                'fuzzy' => true,
                                'context' => [
                                    'type' => $request->get('type')
                                ]
                            ]
                        ]
                ]
        ];

        return $this->JSONFormattedResponse($params);
    }

    public function buildIndex()
    {
        $indexParams['index'] = 'main';

        $myTypeMapping = [
            'company' => [
                'properties' => [
                    'suggester' => [
                        'type' => 'completion',
                        'analyzer' => 'simple',
                        'search_analyzer' => 'simple',
                        'payloads' => true,
                        'context' => [
                            'type' => [
                                'type' => 'category',
                                'path' => '_type',
                            ]
                        ]
                    ]
                ]
            ],
            'seeker' => [
                'properties' => [
                    'suggester' => [
                        'type' => 'completion',
                        'analyzer' => 'simple',
                        'search_analyzer' => 'simple',
                        'payloads' => true,
                        'context' => [
                            'type' => [
                                'type' => 'category',
                                'path' => '_type',
                            ]
                        ]
                    ]
                ]
            ],
            'tag' => [
                'properties' => [
                    'suggester' => [
                        'type' => 'completion',
                        'analyzer' => 'simple',
                        'search_analyzer' => 'simple',
                        'payloads' => true,
                        'context' => [
                            'type' => [
                                'type' => 'category',
                                'default' => ["any_tag", "professional_tag", "personal_tag", "job_tab", "company_tag"]
                            ]
                        ]
                    ]
                ]
            ],
            'category' => [
                'properties' => [
                    'suggester' => [
                        'type' => 'completion',
                        'analyzer' => 'simple',
                        'search_analyzer' => 'simple',
                        'payloads' => true,
                        'context' => [
                            'type' => [
                                'type' => 'category',
                                'path' => '_type',
                            ]
                        ]
                    ]
                ]
            ],
            'subcategory' => [
                'properties' => [
                    'suggester' => [
                        'type' => 'completion',
                        'analyzer' => 'simple',
                        'search_analyzer' => 'simple',
                        'payloads' => true,
                        'context' => [
                            'type' => [
                                'type' => 'category',
                                'path' => '_type',
                            ]
                        ]
                    ]
                ]
            ],
            'language' => [
                'properties' => [
                    'suggester' => [
                        'type' => 'completion',
                        'analyzer' => 'simple',
                        'search_analyzer' => 'simple',
                        'payloads' => true,
                        'context' => [
                            'type' => [
                                'type' => 'category',
                                'path' => '_type',
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $indexParams['body']['mappings'] = $myTypeMapping;

        // Create the index
        $this->search_client->indices()->create($indexParams);
    }


    public function rebuildIndex()
    {
        $this->clearIndex();
        $this->buildIndex();
        $this->insertData();
    }

    public function clearIndex()
    {
        SearchIndexFacade::clearIndex();
    }

    public function insertData()
    {
        Seeker::has('user')->get()->each(function ($obj) {
            \SearchIndex::upsertToIndex($obj);
        });

        Company::get()->each(function ($obj) {
            \SearchIndex::upsertToIndex($obj);
        });

        Category::get()->each(function ($obj) {
            \SearchIndex::upsertToIndex($obj);
        });

        SubCategory::get()->each(function ($obj) {
            \SearchIndex::upsertToIndex($obj);
        });

        Tag::get()->each(function ($obj) {
            \SearchIndex::upsertToIndex($obj);
        });

        Language::get()->each(function ($obj) {
            \SearchIndex::upsertToIndex($obj);
        });

    }

    public function postSearchUsers(Request $request)
    {
        //---> retrieve all the job seekers
        $users = User::with(['country', 'userable', 'videos', 'languages', 'tags'])
            ->where('userable_type', 'App\\Seeker');

        if ($request->get('onlyWithVideos') == 'true')
            $users->has('videos');

        //---> prepare value and search by name
        $input = is_array($request->get('text')) ? trim($request->get('text')['text']) : trim($request->get('text'));

        if ($input)
            $users->where('name', 'like', "%{$input}%");

        //---> search users by category
        if ($request->has('category_id')) {
            $users->where('category_id', $request->get('category_id'));
        }

        if ($request->get('subcategories')) {
            $users->whereHas('subcategories', function ($q) use ($request) {
                $q->whereIn('sub_categories.id', $request->get('subcategories'));
            }, '=', count($request->get('subcategories')));
        }

        if ($request->has('country_id')){
            $users->where('country_id', $request->get('country_id'));
        }

        if ($request->get('languages')) {
            $users->whereHas('languages', function($q) use ($request) {
                $q->whereIn('languages.id', $request->get('languages'));
            }, '=', count($request->get('languages')));
        }

        if ($request->get('tags')) {
            $users->whereHas('tags', function($q) use ($request) {
                $q->whereIn('tags.id', $request->get('tags'));
            }, '=', count($request->get('tags')));
        }
        
        return $users->get();

    }

    public function postSearchCompanies(Request $request)
    {
        
        $companies = User::with(['country', 'userable', 'videos', 'tags'])
            ->where('userable_type', 'App\\CompanyUser');
        
        if ($request->get('onlyWithVideos') == 'true')
            $companies->has('videos');

        $input = is_array($request->get('text')) ? trim($request->get('text')['text']) : trim($request->get('text'));
        
        if ($input)
           $companies->where('name', 'like', "%{$input}%");

         //return $companies->whereHas('userable.company.name', 'like', "%{$input}%" )->get();

        if ($request->has('category_id')) {
            $companies->where('category_id', $request->get('category_id'));
        }

        if ($request->get('subcategories')) {
            $companies->whereHas('subcategories', function ($q) use ($request) {
                $q->whereIn('sub_categories.id', $request->get('subcategories'));
            }, '=', count($request->get('subcategories')));
        }
 
        if ($request->has('country_id')){
            $companies->where('country_id', $request->get('country_id'));
        }

        if ($request->get('languages')) {
            $companies->whereHas('languages', function($q) use ($request) {
                $q->whereIn('languages.id', $request->get('languages'));
            }, '=', count($request->get('languages')));
        }

        if ($request->get('tags')) {
            $companies->whereHas('tags', function($q) use ($request) {
                $q->whereIn('tags.id', $request->get('tags'));
            }, '=', count($request->get('tags')));
        }       
        return $companies->get();
        /*

        $companies = Company::select('name', 'title', 'description', 'website', 'number_of_employees');

        $input = is_array($request->get('text')) ? trim($request->get('text')['text']) : trim($request->get('text'));

        if ($input)
            $companies->where('name', 'like', "%{$input}%");
        /*
        if ($request->has('category_id')) {
            $companies->user->where('category_id', $request->get('category_id'));
        }

        if ($request->get('subcategories')) {
            $companies->user->whereHas('subcategories', function ($q) use ($request) {
                $q->whereIn('sub_categories.id', $request->get('subcategories'));
            }, '=', count($request->get('subcategories')));
        }
        */

    }


    private function JSONFormattedResponse($params)
    {
        return response()->json($this->search_client->suggest($params));
    }


}
