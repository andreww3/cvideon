<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\BlogCategoryCreateRequest;
use App\Http\Requests\TagUpdateRequest;

use App\BlogCategory;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{

    protected $fields = [
        'name' => '',
        ];

    protected $fillable = ['tag_type_id', 'name'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = BlogCategory::all();
        return view('admin.category.index')
                ->withCategories($categories);
    }

    /**
     * Show form for creating new tag
    */
    public function create()
    {
        $data = [];
        foreach ($this->fields as $field => $default) {
          $data[$field] = old($field, $default);
        }

        return view('admin.category.create', $data);
    }

    /**
     * Store the newly created tag in the database.
     *
     * @param TagCreateRequest $request
     * @return Response
    */
    public function store(BlogCategoryCreateRequest $request)
    {
   

        if (!$categoryExists = BlogCategory::find($request->get('name'))) {

            $category = new BlogCategory;
            $category->fill([
                'name' => $request->get('name')
            ]);
            
            $category->save();

            return redirect('/admin/category')
                ->withSuccess("The tag '$category->name' was created.");
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = BlogCategory::findOrFail($id);
        $category->delete();

        return redirect('/admin/category')
            ->withSuccess("The '$category->name' category has been deleted.");
    }
}
