<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use DB;
use App\User;
use App\Role;
use App\Video;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    protected $fields = [
        'name' => '',
        'email' => '',

        ];

    protected $fillable = ['name', 'email'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('admin.user.index')
                ->withUsers($users);
    }

    /**
     * Show form for creating new tag
    */
    public function create()
    {
        $data = [];
        foreach ($this->fields as $field => $default) {
          $data[$field] = old($field, $default);
        }

        return view('admin.user.create', $data);
    }

    /**
     * Store the newly created tag in the database.
     *
     * @param TagCreateRequest $request
     * @return Response
    */
    public function store(Reguest $request)
    {
    
        $input = $request->all();

        if (!$user = User::find($input('email'))) {

            if ($request->get('name') != $user->name) {
                $user->name = $input('name');
            }

            if ($user->userable_type == "App\Seeker") {

                if ($request->get('userable')['status_quo'] != $user->userable->status_quo) {
                    $user->userable->status_quo = $request->get('userable')['status_quo'];
                }


            }
            
            return response()->json(['success' => $user->push() ? 1 : 0]);

            return redirect('/admin/user')
                ->withSuccess("The profile for '$user->name' was created.");
        } else {
            return redirect('/admin/user')
                ->withError("There no user with such email.");

        }

    }
    
    /**
     * Add video for user.
     *
     * @return \Illuminate\Http\Response
     */

    public function addVideo(Request $request)
    {
        $video = $request->file('file');

        if ( ! $user = User::whereId($request->user)->first() )
            return 'non-existent user';

        $file_name = str_random(16) . Carbon::now()->format("_dmY_Hms") . '-' . $video->getClientOriginalExtension() .'.mp4';

        //$file_name = str_random(16) . Carbon::now()->format("_dmY_Hms") . '.mp4';

        $success = Storage::put("videos/{$file_name}", fopen($video, 'r+'));

        if ($success) {
            $user->videos()->save(new Video([
                'value' => $file_name,
                'key' => 'path'
            ]));
        }

        return response()->json(['success' => $success]);
    }


    public function deleteVideo(Request $request)
    {

                
        $videoId = $request->all();

        $id = $videoId[0];

        if ( !$videoRow = DB::table('videos')->whereId($id)->get() )
            return 'video not found';
            
            $video = $videoRow[0];

            $videoLink = 'videos/'. $video->value . '';

            //return $videoLink;

            if ( $fileExists = Storage::disk('s3')->exists($videoLink)) 
                
                Storage::disk('s3')->delete($videoLink);

            
            $videoObject = Video::whereId($id)->get()->first();
            $videoObject->delete();

        return 'Video Deleted';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
    	$input = $request->all();

        if ( ! $user = User::with(['category', 'userable', 'languages', 'country', 'videos', 'roles'])->find($id))
            return redirect()->back()->with('messages', ['User not found']);

        /* if the user is a member already
        if ( $user->isMember() )
            return $user->roles;*/

        return view('admin.user.edit', $user);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = $request->all();

        $userId = $input['id'];

        if ( ! $user = User::find($userId) ) 
            return "The user can't be found.. ";

            if( $input['role'] ) {

                if ( $user->isMember() )
                    $user->role->delete();

                if ( ! $user->isMember() )
                    $user->makeMember($input['role']);
                    return "Role Added... none was there..!";
   
            }
            
            if ($input['name'] != $user->name) 
                $user->name = $input['name'];
                
            if ($user->userable_type == "App\Seeker") {

                if ($input['status_quo'] != $user->userable->status_quo)
                    $user->userable->status_quo = $input['status_quo'];

            }

            if ($user->userable_type == "App\CompanyUser") {

                if ($input['company_title'] != $user->userable->company->title)
                    $user->userable->company->title = $input['company_title'];

            }            
            
            $user->push();

        return redirect("/admin/user/$userId/edit")
            ->withSuccess("The changes have been saved.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->userable()->delete();
        $user->delete();

        return redirect('/admin/user')
            ->withSuccess("'$user->name' is no longer a user here.. | deleted.");
    }
}
