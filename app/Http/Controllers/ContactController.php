<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactFormRequest;

class ContactController extends Controller 
{
   	public function create()
   	{
   		return view('facade.contact');
   	}

   	public function store(ContactFormRequest $request)
   	{
   		\Mail::send('emails.contact',
   			array(
   				'name' => $request->get('name'),
   				'email' => $request->get('email'),
   				'visitor_message' => $request->get('message')
   				), function($message)
   			{
   				$message->from('it@cvideon.com');
   				$message->to('it@cvideon.com', 'Admin')->subject('From Cvideon');
   			});
   		return \Redirect::route('contact')
   			->with('message', 'Thanks for contacting us!');
   	}
}
