<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Mail;
use App\CompanyUser;
use App\Seeker;
use App\User;
use App\Company;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectPath = '/user/profile';
    protected $redirectTo = '/auth/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'country' => 'required|exists:countries,id',
            'category' => 'required|exists:categories,id',
            'terms' => 'required',
            //'company' => 'required_with:company|max:255',
            //'company' => 'required_with:company|exists:companies,id'
        ]);
    }
 
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $input = Input::only(
            'name',
            'email'
        );
        
        $confirmation_code = str_random(30);

        $codeData = array('confirmation_code' => $confirmation_code);
        
        \Mail::send('emails.verify', 
            array('confirmation_code' => $confirmation_code), 
            function($message) {
                $message->from('it@cvideon.com', 'CVideon')
                         ->to(Input::get('email'), Input::get('name'))
                        ->subject('Verify your email address');
            }
        );

        //Flash::message('Thanks for registering! Please check your email and also your bulk for the activation email.');

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => $confirmation_code,
            'country_id' => $data['country'],
            'category_id' => $data['category']
        ]);

        if(!array_key_exists('company', $data))  {

            $this->redirectPath = '/thanks';

            $inherited = Seeker::create();
            $inherited->user()->save($user);
            
        } else {

            $this->redirectPath = '/thanks';
            // IF user is registering as a company  

            if (!$existentCompany = Company::where('name', $data['company'])->first()) {

                $company = new Company;
                $company->fill([
                    'name' => $data['company']
                ]);

                $company->save();

                $inherited = CompanyUser::create(['company_id' => $company->id]);
                $inherited->user()->save($user);

            } else {   

                $inherited = CompanyUser::create(['company_id' => $existentCompany->id]);
                $inherited->user()->save($user);

            }

        }

        return $user;                

    }

    //-----> Verifying the user's email address

    public function confirm($confirmation_code)
    {

        if ( ! $confirmation_code ) 
        {
            throw new InvalidConfirmationCodeException;
        }

        $theuser = User::where('confirmation_code', $confirmation_code)->first();

        //return $theuser;
        
        if ( ! $theuser )
        {
            throw new InvalidConfirmationCodeException;
        }        
        //return $theuser->name;

        $theuser->confirmed = 1;
        $theuser->confirmation_code = null;

        $theuser->save();

        if ($theuser->userable_type == "App\Seeker") {
            \Mail::send('emails.welcome-seeker', 
                array('name' => $theuser->name), 
                function($message) use ($theuser) {
                    $message->from('it@cvideon.com', 'CVideon')
                            ->to($theuser->email, $theuser->name)
                            ->subject('Welcome to CVideon');
                }
            );
        }

        if ($theuser->userable_type == "App\CompanyUser") {
            return $theuser;
            \Mail::send('emails.welcome-company', 
                array('name' => $theuser->name), 
                function($message) use ($theuser) {
                    $message->from('it@cvideon.com', 'CVideon')
                            ->to($theuser->email, $theuser->name)
                            ->subject('Welcome to CVideon');
                }
            );
        }

        //Flash::message('You have successfully verified your account.');

        return view('auth.confirmed');       
    }
}
