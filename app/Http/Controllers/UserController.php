<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use App\Category;
use App\Country;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Image;
use App\Language;
use App\SubCategory;
use App\SubcategoryUser;
use App\User; 
use App\Video;
use App\Experience;
use App\ExperienceType;
use App\Tag;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * Verifying the user's email address
    **/

    public function confirm($confirmation_code)
    {

        if ( ! $confirmation_code ) 
        {
            throw new InvalidConfirmationCodeException;
        }

        $theuser = User::where('confirmation_code', $confirmation_code)->first();

        //return $theuser;
        
        if ( ! $theuser )
        {
            throw new InvalidConfirmationCodeException;
        }        
        //return $theuser->name;

        $theuser->confirmed = 1;
        $theuser->confirmation_code = null;

        $theuser->save();

        if ($theuser->userable_type == "App\Seeker") {
            \Mail::send('emails.welcome-seeker', 
                array('name' => $theuser->name), 
                function($message) use ($theuser) {
                    $message->from('it@cvideon.com', 'CVideon')
                            ->to($theuser->email, $theuser->name)
                            ->subject('Welcome to CVideon');
                }
            );
        }

        if ($theuser->userable_type == "App\CompanyUser") {
            return $theuser;
            \Mail::send('emails.welcome-company', 
                array('name' => $theuser->name), 
                function($message) use ($theuser) {
                    $message->from('it@cvideon.com', 'CVideon')
                            ->to($theuser->email, $theuser->name)
                            ->subject('Welcome to CVideon');
                }
            );
        }

        //Flash::message('You have successfully verified your account.');

        return view('auth.confirmed');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        if (!$user = User::with(['category', 'subCategories', 'userable', 'languages', 'country', 'tags', 'experiences', 'videos'])->find($id))
            return redirect()->back()->with('messages', ['User not found']);

        return $user;

    }

    public function showLoggedInUser()
    {
        if (Auth::check()) {
            return Auth::user()->id;
        } else {
            return 0;
        }

    }  

    /**
     * Get input and edit the profile information.
     *
     */

    public function uploadProfilePicture(Request $request)
    {

        $image = $request->get('image');

        if (!$user = $request->user())
            return response()->json(['success' => false]);

        $file_name = str_random(16) . Carbon::now()->format("_dmY_Hms") . '.png';

        $success = Storage::put('images/profile-pictures/' . $user->id . '/' . $file_name, Image::make($image)->fit(220, 220)->stream()->__toString());

        if ($success) {
            $user->profile_picture = $file_name;
            $success = $user->save();
        }

        //return response()->json(['success' => $success]);
        return redirect('/user/profile');
    }

    public function uploadProfileCover(Request $request)
    {
        $image = $request->get('image');

        if (!$user = $request->user())
            return response()->json(['success' => false]);

        $file_name = str_random(16) . Carbon::now()->format("_dmY_Hms") . '.png';

        $success = Storage::put('images/profile-covers/' . $user->id . '/' . $file_name, Image::make($image)->fit(1140, 300)->stream()->__toString());

        if ($success) {
            $user->cover_picture = $file_name;
            $success = $user->save();
        }

        //return response()->json(['success' => $success]);
        return redirect('/user/profile');
    }
    
    public function addInfo(Request $request)
    {

        $input = $request->all();

        if (!$user = $request->user())
            throw new \Exception('Login required');

        if ($user->id != $input['id'])
            throw new \Exception('Request not authorized');


       if ($request->get('newVideo')) {
            
            $user->videos()->delete();

            $video = new Video;
            $video->fill([
                'key' => 'url',
                'value' => $request->get('newVideo')
                ]);
            $video->save();

            $user->videos()->save($video);

        }

        if ($request->has('newtagid')) {
            $keyword = Tag::find($request->get('newtagid'));
            $user->tags()->attach($keyword);
        }

        if ($request->has('newTag')) {

            if (!$tagExists = Tag::find($request->get('newTag')['name'])) {

                $keyword = new Tag;
                $keyword->fill([
                    'tag_type_id' => $request->get('newTag')['tag_type_id'],
                    'name' => $request->get('newTag')['name']
                ]);
                
                $keyword->save();

                $user->tags()->attach($keyword->id);

            }
        }
        

        if ($request->has('newExperience')) {

            if (!$experience_type = ExperienceType::find($request->get('newExperience')['experience_type_id']))
                die('ExperienceType False');

            $experience = new Experience;

            $experience->fill([
                'title' => $request->get('newExperience')['title'],
                'description' => $request->get('newExperience')['description'],
                'started_at' => $request->get('newExperience')['started_at'],
                'ended_at' => $request->get('newExperience')['ended_at']
            ]);
            
            $experience->experienceType()->associate($experience_type);
            $experience->user()->associate($user);
            $experience->save();
            /*
            $user->experiences()->associate($experience->id);
            $experience->user()->associate($user);
            */
        }

        return response()->json(['success' => $user->push() ? 1 : 0]);

    } 


    public function editInfo(Request $request)
    {
        $input = $request->all();

        if (!$user = $request->user())
            throw new \Exception('Login required');

        if ($user->id != $input['id'])
            throw new \Exception('Request not authorized');


        if ($user->country_id != $input['country_id'] && $country = Country::find($input['country_id'])) {
            $user->country()->associate($country);
        }

        if ($user->category_id != $input['category']['id'] && $category = Category::find($input['category']['id'])) {
            $user->category()->associate($category);
        }


        $selected_subcategories = array_map(function ($item) {
            return $item['id'];
        }, $input['sub_categories']);


        $sub_categories = SubCategory::where('category_id', $input['category']['id'])
            ->whereIn('id', $selected_subcategories)
            ->lists('id')
            ->all();

        $user->subCategories()->sync($sub_categories); 

        if ($request->get('deleteVideoId')) {
            $user->videos()->delete();
        }

        if ($user->userable_type == "App\Seeker") {

            if ($request->get('name') != $user->name) {
                $user->name = $request->get('name');
            }

            if ($request->get('userable')['job_title'] != $user->userable->job_title) {
                $user->userable->job_title = $request->get('userable')['job_title'];
            }

            if ($request->get('userable')['status_quo'] != $user->userable->status_quo) {
                $user->userable->status_quo = $request->get('userable')['status_quo'];
            }

            if ($request->get('userable')['goal'] != $user->userable->goal) {
                $user->userable->goal = $request->get('userable')['goal'];
            }

            if ($request->has('languages')) {
                $selected_languages = array_map(function ($item) {
                    return $item['id'];
                }, $input['languages']);

                $languages = Language::whereIn('id', $selected_languages)->lists('id')->all();
                $user->languages()->sync($languages);
            }

            if ($request->has('tagToDelete')) {

                $keywordId = $request->get('tagToDelete');

                if (!$tag = Tag::find($keywordId))
                    die('non-existent');

                $user->tags()->detach($keywordId);
            }

            if ($request->has('experience')) {

                $experienceId = $request->get('experience')['id'];

                if (!$experience = Experience::find($experienceId))
                    die('Non-existent Experience');

                $experience->title = $request->get('experience')['title'];
                $experience->description = $request->get('experience')['description'];
                $experience->started_at = $request->get('experience')['started_at'];
                $experience->ended_at = $request->get('experience')['ended_at'];
                if (!$experience->save())
                    die('not saved');
            
            }

            if ($request->has('experienceToDelete')) {

                $experienceId = $request->get('experienceToDelete');

                if (!$experience = Experience::find($experienceId))
                    die('non-existent');

                $experience->delete();
            }

        }

        if ($user->userable_type == "App\CompanyUser") {
            if ($request->get('userable')['company']['name'] != $user->userable->company->name) {
                $user->userable->company->name = $request->get('userable')['company']['name'];
            }

            if ($request->get('userable')['company']['title'] != $user->userable->company->title) {
                $user->userable->company->title = $request->get('userable')['company']['title'];
            }

            if ($request->get('userable')['company']['website'] != $user->userable->company->website) {
                $user->userable->company->website = $request->get('userable')['company']['website'];
            }

            if ($request->get('userable')['company']['number_of_employees'] != $user->userable->company->number_of_employees) {
                $user->userable->company->number_of_employees = $request->get('userable')['company']['number_of_employees'];
            }

            if ($request->get('userable')['company']['description'] != $user->userable->company->description) {
                $user->userable->company->description = $request->get('userable')['company']['description'];
            }

            if ($request->has('tagToDelete')) {

                $keywordId = $request->get('tagToDelete');

                if (!$tag = Tag::find($keywordId))
                    die('non-existent');

                $user->tags()->detach($keywordId);
            }

        }

        return response()->json(['success' => $user->push() ? 1 : 0]);
    }

    public function addVideo(Request $request)
    {
        $video = $request->file('file');

        if (!$user = $request->user())
            return response()->json(['success' => false]);

        $file_name = str_random(16) . Carbon::now()->format("_dmY_Hms") . '-' . $video->getClientOriginalExtension() .'.mp4';

        //$file_name = str_random(16) . Carbon::now()->format("_dmY_Hms") . '.mp4';

        $success = Storage::put("videos/{$file_name}", fopen($video, 'r+'));

        if ($success) {
            $user->videos()->save(new Video([
                'value' => $file_name,
                'key' => 'path'
            ]));
        }
 
        return response()->json(['success' => $success]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
