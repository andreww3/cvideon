<?php

namespace App\Http\Controllers;

use App\SubCategory;

class SubCategoryController extends Controller
{
    public function show()
    {
        return $subCategories = SubCategory::all();
    }
}
?>