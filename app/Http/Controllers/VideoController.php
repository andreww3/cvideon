<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User; 
use App\Video;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showVideoCV($id)
    {
        if (!$user = User::with(['videos'])->find($id))
            return redirect()->back()->with('messages', ['User not found']);


        $video = $user->videos->first();

        $videoId = $video['id'];
        $videoLink = $video['value'];


        return view('profile.video', 
                array('videoLink' => $videoLink ));

    }

    public function showVideoCVIframe($id)
    {
        if (!$user = User::with(['videos'])->find($id))
            return redirect()->back()->with('messages', ['User not found']);

        $userId = $user->id;
        
        $video = $user->videos->first();

        $videoId = $video['id'];
        $videoLink = $video['value'];


        return view('profile.videoIframe', 
                array('userId' => $userId ));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
