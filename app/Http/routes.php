<?php

use App\User;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * Facade/Frontpages Routes
*/
Route::get('/', function(){
    return view('facade.home');
});

Route::get('/company', function(){
    return view('facade.company');
});

Route::get('/about', function(){
    return view('facade.about');
});

Route::get('/terms', function(){
    return view('facade.terms');
});

Route::get('/thanks', function () {
    return view('auth.thanks');
});

Route::get('/contact', ['as' => 'contact', 'uses' => 'ContactController@create']);

Route::post('contact', ['as' => 'contact_store', 'uses' => 'ContactController@store']);

Route::get('/showLoggedInUser', ['uses' => 'UserController@showLoggedInUser']);
/**
 * End Facade
*/

/* Get all emails */

Route::get('get/all/emails', function () {
    $emails = User::select('email')->get();

    return $emails;
});

/*******************************
 * Blog Routes
*/

Route::get('blog', 'BlogController@index');
Route::get('blog/{slug}', 'BlogController@showPost');


// Admin area

get('admin', function () {   
    
    if ( Auth::user()->hasRole('edit_users') )
    {
        return redirect('/admin/user');    
    
    }  
    
    if ( Auth::user()->hasRole('edit_articles') )
    {
        return redirect('/admin/post');    
    
    }   

    return 'No access to the admin area for you yet';
    
});


$router->group([
    'namespace' => 'Admin',
    'middleware' => 'auth',
], function () {
    resource('admin/user', 'UserController', ['except' => 'show']);
    resource('admin/post', 'PostController', ['except' => 'show']);
    resource('admin/category', 'CategoryController', ['except' => 'show']);
    resource('admin/tag', 'TagController', ['except' => 'show']);
    get('admin/upload', 'UploadController@index');

    post('admin/upload/file', 'UploadController@uploadFile');
    delete('admin/upload/file', 'UploadController@deleteFile');
    post('admin/upload/folder', 'UploadController@createFolder');
    delete('admin/upload/folder', 'UploadController@deleteFolder');
});
 
// Logging in and out
get('auth/login', 'Auth\AuthController@getLogin');
post('auth/login', 'Auth\AuthController@postLogin');
get('auth/logout', 'Auth\AuthController@getLogout');


/**
 * End Blog 
*/
Route::post('admin/add/video', ['uses' => 'Admin\UserController@addVideo']);
Route::post('admin/delete/video', ['uses' => 'Admin\UserController@deleteVideo']);
/**
 * Registering Routes
*/
Route::get('/register', function(){
    return view('auth.register');
});

Route::get('/register-company', function(){
    return view('auth.register-company');
});

/**
 * End Registering
*/

Route::controllers([
    'auth'      =>  'Auth\AuthController',
    'password'  =>  'Auth\PasswordController'
]);

Route::get('/retrieve-password', function(){
    return view('auth.password');
});

Route::get('/register', ['uses' => 'Auth\AuthController@getRegister']);

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'UserController@confirm'
]);

Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::post('password/email', 'Auth\PasswordController@postEmail');


// --- Routes pulled out of the middleware so you can see profiles without loggin in ----- 
Route::get('user/{id}', function(){
    return view('profile.seeker');
});

Route::get('videoCV/{id}', ['uses' => 'VideoController@showVideoCV']);
Route::get('videoCVIframe/{id}', ['uses' => 'VideoController@showVideoCVIframe']);
//Route::get('video/{id}', ['uses' => 'VideoController@showVideo']);

Route::get('company/{id}', function(){
    return view('profile.company');
});


// --- Routes for the api data
Route::group(['prefix' => 'api'], function () {
    Route::get('user/{id}', ['uses' => 'UserController@show']);
    Route::get('countries', ['uses' => 'CountryController@show']);

    Route::get('personalTags', ['uses' => 'TagController@showPersonalTags']);
    Route::get('professionalTags', ['uses' => 'TagController@showProfessionalTags']);
    Route::get('companyTags', ['uses' => 'TagController@showCompanyTags']);

    Route::controller('category', 'CategoryController');
    Route::get('subcategories', ['uses' => 'SubCategoryController@show']);
    Route::get('languages', ['uses' => 'LanguageController@show']);

    Route::get('6/posts', ['uses' => 'BlogController@show6']);
    Route::get('posts', ['uses' => 'BlogController@showAll']);
}); 

Route::get('/user/company', function () {

        if ( Auth::user()->userable_type == "App\CompanyUser")
        {
            return Auth::user()->userable->company->name;    
        }
        
        if ( Auth::user()->userable_type == "App\Seeker")
        {
            return 'You are a job seeker, not a company';    
        }
});

// -- ^^ Routes to put in a guest middleware --- |||


Route::group(['middleware' => 'auth'], function(){

    Route::get('/login', ['uses' => 'Auth\AuthController@getLogin']);

    /// ---> Registration step routes

    Route::get('registration/seeker/', function(){
        return redirect('registration/seeker/' . Auth::user()->id);
    });
    
    Route::get('registration/seeker/{id}', function(){
        return view('registration/seeker/page');
    });

    Route::get('registration/company/', function(){
        return redirect('registration/company/' . Auth::user()->id);
    });

    Route::get('registration/company/{id}', function(){
        return view('registration/company/page');
    });

    /// ---> end registration step routes


    Route::get('user/{id}/{welcome}', function(){
        return view('profile.seeker');
    });

    Route::get('company/{id}/{welcome}', function(){
        return view('profile.company');
    });


    Route::get('user/profile', function(){

        if ( Auth::user()->userable_type == "App\CompanyUser")
        {
            return redirect('company/' . Auth::user()->id)->with('status', 'You have successfully logged in');    
        }
        
        if ( Auth::user()->userable_type == "App\Seeker")
        {
            return redirect('user/' . Auth::user()->id)->with('status', 'You have successfully logged in');    
        }
    });

    // ---- Search Routes

    Route::controller('search', 'SearchController');

    Route::get('random/user/profiles', ['uses' => 'SearchController@randomUserProfiles']);
    Route::get('random/company/profiles', ['uses' => 'SearchController@randomCompanyProfiles']);

    Route::get('/search/seekers', ['uses' => 'SearchController@getSeekers']);  
    Route::get('/search/companies', ['uses' => 'SearchController@getCompanies']);  

    Route::post('/search/search-companies', ['uses' => 'SearchController@postSearchCompanies']);

    // -- Messaging routes
    Route::get('messages', function () {
        return redirect('messages/' . Auth::user()->id);
    });

    Route::get('messages/{id}', function(){
        return view('messaging.index');
    });
    
    Route::post('messages/getUnreadMessages', ['uses' => 'MessageController@getUnreadMessages']);

    Route::post('messages/getLastMessage', ['uses' => 'MessageController@lastMessage']);
    Route::post('messages/getInbox', ['uses' => 'MessageController@inbox']);
    Route::post('messages/getSent', ['uses' => 'MessageController@sentbox']);

    Route::post('read/message', ['uses' => 'MessageController@markRead']);
    Route::post('delete/message', ['uses' => 'MessageController@destroy']);

    Route::post('message/getUsers', ['uses' => 'MessageController@getUsers']);

    Route::post('/sendMessage', ['uses' => 'MessageController@store']);

    // -- End Search routes
    Route::get('user/business-card/', function(){
        return redirect('user/business-card/' . Auth::user()->id);
    });

    Route::get('user/business-card/{id}', function(){
        return view('user.business-card');
    });

    Route::group(['prefix' => 'es'], function(){

        Route::get('rebuild-index', ['uses' => 'SearchController@rebuildIndex']);

        Route::get('clear-index', ['uses' => 'SearchController@clearIndex']);

        Route::get('build-index', ['uses' => 'SearchController@buildIndex']);

        Route::get('insert-data', ['uses' => 'SearchController@insertData']);

    });

    Route::post('profile/edit/uploadProfilePicture', ['uses' => 'UserController@uploadProfilePicture']); 


    Route::post('profile/edit/uploadProfileCover', ['uses' => 'UserController@uploadProfileCover']);


    Route::post('profile/add/info', ['uses' => 'UserController@addInfo']);


    Route::post('profile/edit/info', ['uses' => 'UserController@editInfo']);


    Route::post('profile/addVideo', ['uses' => 'UserController@addVideo']);

});
?>
