<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\SearchIndex\Searchable;

class Category extends BaseModel implements Searchable
{
    use SoftDeletes;

    protected $fillable = ['name'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function subCategories()
    {
        return $this->hasMany('App\SubCategory');
    }

    public function getSearchableBody()
    {
        $inputs = [
            $this->name,
        ];

        return [
            'name' => $this->name,
            'suggester' => [
                'input' => $inputs,
                'output' => $this->name,
                'payload' => [
                    'id' => $this->id,
                    'name' => $this->name,
                ],
            ]];
    }

    public function getSearchableType()
    {
        return 'category';
    }

    public function getSearchableId()
    {
        return $this->id;
    }
}
