<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Message extends BaseModel
{
    use SoftDeletes;

    protected $table = 'messages';

    protected $fillable = ['sender_id', 'receiver_id', 'subject', 'message', 'read'];

    protected $dates = ['deleted_at'];



    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\User', 'receiver_id');
    }

}

