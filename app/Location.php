<?php

namespace App;

use JeroenDesloovere\Geolocation\Geolocation;

class Location extends Geolocation
{
    public function getAddress($latitude, $longitude)
    {
        $addressSuggestions = $this->getAddresses($latitude, $longitude);

        return !$addressSuggestions ? '' : $addressSuggestions[0];
    }
}
