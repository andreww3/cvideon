<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\SearchIndex\Searchable;

class SubCategory extends BaseModel implements Searchable
{
    use SoftDeletes;

    protected $fillable = ['category_id', 'name'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    protected $dates = ['deleted_at'];

    public function users()
    {
        return $this->morphedByMany('App\User', 'categoryable');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function getSearchableBody()
    {
        $inputs = [
            $this->name,
            $this->category->name
        ];

        return [
            'name' => $this->name,
            'category' => $this->category,
            'suggester' => [
                'input' => $inputs,
                'output' => $this->name,
                'payload' => [
                    'id' => $this->id,
                    'name' => $this->name,
                    'category' => $this->category
                ],
            ]];
    }

    public function getSearchableType()
    {
        return 'subcategory';
    }

    public function getSearchableId()
    {
        return $this->id;
    }

}
