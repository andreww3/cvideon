<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    protected $location;

    public function instantiateLocation()
    {
        if(is_null($this->location) || !$this->location instanceof Location)
            $this->location = new Location;
    }


}
