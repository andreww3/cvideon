<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\SearchIndex\Searchable;

class Language extends BaseModel implements Searchable
{
    use SoftDeletes;

    protected $fillable = ['name', 'locale'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function getSearchableBody()
    {
        $inputs = [
            $this->name,
            $this->locale
        ];

        return [
            'name' => $this->name,
            'locale' => $this->locale,
            'suggester' => [
                'input' => $inputs,
                'output' => $this->name,
                'payload' => [
                    'id' => $this->id,
                    'name' => $this->name,
                    'locale' => $this->locale
                ],
            ]];
    }

    public function getSearchableType()
    {
        return 'language';
    }

    public function getSearchableId()
    {
        return $this->id;
    }
}
