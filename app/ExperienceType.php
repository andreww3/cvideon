<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class ExperienceType extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
