<?php

namespace App;

use App\Role;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Jackpopp\GeoDistance\GeoDistanceTrait;
use Spatie\SearchIndex\Searchable;

class User extends BaseModel implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes, GeoDistanceTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'category_id', 'country_id', 'lat', 'lng', 'confirmation_code', 'profile_picture', 'cover_picture'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['deleted_at'];

    /* * * *
          * * * * Get the roles a user has * * *
                                              * * * */
    public function roles ()
    {
        return $this->belongsToMany('Role', 'user_roles');
    }

    /* *
      * Find out if User is an admin, based on if has any roles
     *  @return boolean
    * */
    public function isMember ()
    {
        $roles = $this->roles->toArray();
        return !empty($roles);
    }

    /* *
      * Find out if user has a specific role 
     * @return boolean
    * */
    public function hasRole ($check)
    {
        return in_array($check, array_fetch($this->roles->toArray(), 'name'));
    }

    /* *
      * Get key in array with corresponding value
     * @return int
    */
    private function getIdInArray($array, $term)
    {
        foreach ($array as $key => $value) {
            if ($value == $term) {
                return $key;
            }
        }

        throw new UnexpectedValueException;
    }    


    /* *
      * 
     *  Add roles to user to make them a concierge
    */
    public function makeMember($title)
    {

        $assigned_roles = array();
        $roles = array_fetch(Role::all()->toArray(), 'name');

        switch ($title) {

            case 'admin':
                $assigned_roles[] = $this->getIdInArray($roles, 'edit_es_data');
                $assigned_roles[] = $this->getIdInArray($roles, 'edit_users');
            
            case 'editor':
                $assigned_roles[] = $this->getIdInArray($roles, 'edit_data');

            case 'author':
                $assigned_roles[] = $this->getIdInArray($roles, 'edit_article');
                $assigned_roles[] = $this->getIdInArray($roles, 'create_article');

            case 'member':
                $assigned_roles[] = $this->getIdInArray($roles, 'comment');
                break;
            
            default:
                throw new \Exception("The role entered does not exist");
        }

        $this->roles()->attach($assigned_roles);
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function languages()
    {
        return $this->belongsToMany('App\Language');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function subCategories()
    {
        return $this->morphToMany('App\SubCategory', 'categoryable');
    }
    
    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    public function experiences()
    {
        return $this->hasMany('App\Experience');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }

    public function videos()
    {
        return $this->morphMany('App\Video', 'videoable');
    }

    public function userable()
    {
        return $this->morphTo();
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function getProfilePictureAttribute($value)
    {
        return Helpers::getPublicS3URL("images/profile-pictures/{$this->id}/{$value}") ?: '/images/profile-pics/defaultpic.png';
    }

    public function getCoverPictureAttribute($value)
    {
        return Helpers::getPublicS3URL("images/profile-covers/{$this->id}/{$value}") ?: '';
    }

    public function getAddressAttribute()
    {
        $this->instantiateLocation();

        return $this->location->getAddress($this->lat, $this->lng);
    }

}
