<?php
return [
	'title' => 'CVideon Blog',
	'description' => 'Your learning tool for creating sucessful video CVs.',
	'posts_per_page' => 6,
  	'uploads' => [
	    'storage' => 'local',
	    'webpath' => '/blog-uploads',
  	],
]; 