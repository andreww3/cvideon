@extends('layouts.default')
@section('title', 'Page missing')

@section('menu')

    @include('layouts.menu')

@endsection

@section('content')
    <div class="container">
        <h1 style="color: black;">404</h1>

        <h2 class="text-muted">The page you requested does not exist</h2>
    </div>
@endsection