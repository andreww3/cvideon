@extends('layouts.default')

@inject('countries', 'App\Country')
@inject('categories', 'App\Category')
@inject('companies', 'App\Company')

@section('title', 'Registration')

@section('header')
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
@endsection

@section('content')
</div> <!-- <<< Closing Container little hack >>> -->

<div id="home-bg-cover" style="background-image:url('/assets/images/video-frame.png'); background-size:cover; height:auto; min-height:750px; text-align:left;">

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header ">
				<button type="button" class="navbar-toggle collapsed home-menu-top-margin" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="navbar-brand col-lg-3 col-md-3 col-sm-3 hidden-xs"><a href="{{ url('/') }}" target="_self"><img class="img-responsive header-logo-image" src="/assets/images/CVideon-logo-white.png"></a></div>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right home-menu-top-margin">
					<li><a href="/register-company" target="_self">REGISTER COMPANY</a></li>
					<li><a href="#login-box" class="login-window">LOG IN</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>

    <div class="visible-xs">
        <a href="{{ url('/') }}"><img id="mobile-logo" class="img-responsive col-xs-7 col-xs-push-2" src="/assets/images/CVideon-logo-white.png"></a>
    	<div class="clearfix"></div>
    </div>

	<div class="clearfix spacer"></div>

    @include('auth.login-panel')

	<div class="container-fluid" style="min-height:520px;" ng-controller="regCtrl">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default register-panel">
					<div class="panel-heading">
						<h4 class="text-center" style="font-size:24px;">Signing up as a job seeker</h4> 
					</div>
					<div class="panel-body">
						<form name="userSignUpForm" class="form-horizontal" role="form" novalidate>
							<div class="form-group">
								<label class="col-md-4 control-label">Full Name:</label>
								<div class="col-md-6 input-tooltip">
									<input type="text" class="form-control" name="name" ng-model="userForm.name" value="{{ old('name') }}"
									placeholder="Your Full Name.."
									ng-class="{'input-error' : signupErrorMap.name}"
									ng-blur="removeErrorMsg('name')"
									tooltip-enable="signupErrorMap.name"
									uib-tooltip="{[signupErrorMap.name[0]]}"
									tooltip-placement="right"
	  								tooltip-trigger="mouseenter"
	  								tooltip-class="tooltip-error"
	  								required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">E-Mail Address:</label>
								<div class="col-md-6">
									<input type="email" class="form-control" name="email" ng-model="userForm.email" value="{{ old('email') }}"
									placeholder="Your Email Address.."
	  								ng-class="{'input-error' : signupErrorMap.email}"
	  								ng-blur="removeErrorMsg('email')"
	  								tooltip-enable="signupErrorMap.email"
	  								uib-tooltip="{[signupErrorMap.email[0]]}"
									tooltip-placement="right"
	  								tooltip-trigger="mouseenter"
	  								tooltip-class="tooltip-error"
									required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Work Field:</label>
								<div class="col-md-6">
									<select class="form-control" name="category" ng-model="userForm.category"
										ng-class="{'input-error' : signupErrorMap.category}"
										ng-blur="removeErrorMsg('category')"
	  									tooltip-enable="signupErrorMap.category"
	  									uib-tooltip="{[signupErrorMap.category[0]]}"
										tooltip-placement="right"
	  									tooltip-trigger="mouseenter"
	  									tooltip-class="tooltip-error"
										required>
										<option value default selected>Select your work category</option>
										<option ng-repeat="category in allCategories" ng-value="category.id">
											{[category.name]}
										</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Country:</label>
								<div class="col-md-6">
									<select class="form-control" name="country" ng-model="userForm.country" 
										ng-class="{'input-error' : signupErrorMap.country}"
										ng-blur="removeErrorMsg('country')"
										tooltip-enable="signupErrorMap.country"
	  									uib-tooltip="{[signupErrorMap.country[0]]}"
										tooltip-placement="right"
	  									tooltip-trigger="mouseenter"
	  									tooltip-class="tooltip-error"
										required>
										<option value default selected>Select your country</option>
										<option ng-repeat="country in allCountries" ng-value="country.id">
											{[country.name]}
										</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Password:</label>
								<div class="col-md-6">
									<input type="password" class="form-control" name="password" ng-model="userForm.password"
										ng-class="{'input-error' : signupErrorMap.password}"
										ng-blur="removeErrorMsg('password')"
										tooltip-enable="signupErrorMap.password"
	  									uib-tooltip="{[signupErrorMap.password[0]]}"
										tooltip-placement="right"
	  									tooltip-trigger="mouseenter"
	  									tooltip-class="tooltip-error" 
										required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Confirm Password:</label>
								<div class="col-md-6">
									<input type="password" class="form-control" name="password_confirmation" ng-model="userForm.password_confirmation"
									ng-class="{'input-error' : signupErrorMap.password}"
									ng-blur="removeErrorMsg('password_confirmation')"
									tooltip-enable="signupErrorMap.password"
	  								uib-tooltip="{[signupErrorMap.password[0]]}"
									tooltip-placement="right"
	  								tooltip-trigger="mouseenter"
	  								tooltip-class="tooltip-error"
									required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-4"></div>
								<div class="col-md-6">
									<label style="cursor:pointer; font-family:'Raleway-Thin';color:white; text-align:center;">
										<input style="float:left; width:15px; height:15px; margin-top:4px; margin-right:10px;" type="checkbox"
										class="form-control" name="terms" ng-model="userForm.terms"
										ng-required="!termsChecked"
										ng-class="{'input-error' : signupErrorMap.terms}"
										ng-blur="removeErrorMsg('terms')"
	  									tooltip-enable="signupErrorMap.terms"
	  									uib-tooltip="{[signupErrorMap.terms[0]]}"
										tooltip-placement="right"
	  									tooltip-trigger="mouseenter"
	  									tooltip-class="tooltip-error"> 
										I agree with the <a href="/terms" class="termsLink" target="_blank">terms and contitions</a>
									</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 col-md-offset-4">
									<input type="submit" class="button front-page-button" value="Register" ng-click="processForm(userSignUpForm)"></input>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="visible-xs clearfix spacer"></div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('footer')

    	@include('layouts.footer')

@endsection

@section('javascript')
	<script>
		$(document).ready(function() {
			$('a.login-window').click(function() {

				//Getting the variable's value from a link
				var loginBox = $(this).attr('href');

				//Fade in the Popup
				$(loginBox).fadeIn(300);

				//Set the center alignment padding + border see css style
				var popMargTop = ($(loginBox).height() + 24) / 2;
				var popMargLeft = ($(loginBox).width() + 24) / 2;

				$(loginBox).css({
					'margin-top' : -popMargTop,
					'margin-left' : -popMargLeft
				});

				// Add the mask to body
				$('body').append('<div id="mask"></div>');
				$('#mask').fadeIn(300);

				return false;
			});

			// When clicking on the button close or the mask layer the popup closed
			$('a.close-login, #mask').on('click', function() {
				$('#mask , .login-popup').fadeOut(300 , function() {
					$('#mask').remove();
				});
				return false;
			});

		});
	</script>
@endsection
