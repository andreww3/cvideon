@extends('layouts.default')

@section('title', 'Welcome')

@section('content')
    </div> <!-- lille hack closing container -->
    <div id="home-bg-cover" style="height:600px; background-image:url('/assets/images/About.png'); background-size:cover;">

        <div id="home-top-content">

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header ">
                        <button type="button" class="navbar-toggle collapsed home-menu-top-margin" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="navbar-brand col-lg-3 col-md-3 col-sm-3 hidden-xs"><a href="{{ url('/') }}"><img class="img-responsive header-logo-image" src="/assets/images/CVideon-logo-white.png"></a></div>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right home-menu-top-margin">
                            @if (Auth::check())
                                <li><a href="/blog" target="_self">BLOG</a></li>
                                <li><a href="user/profile" target="_self">MY PROFILE</a></li>
                                <li><a href="/auth/logout" target="_self">LOGOUT</a></li>
                            @else
                            <li><a href="#login-box" class="login-window">LOG IN</a></li>
                            @endif
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
            <div class="col-md-3"></div>

                <div class="col-md-6 text-center">

                    <div class="clearfix spacer"></div>
                    <div class="clearfix spacer"></div>
                    <div class="hidden-xs"><h1 style="text-shadow: 0px 1px 1px black;">Confirmed!</h1></div>
                    <h3 class="hidden-xs" style="text-shadow: 0px 1px 1px black;">Thanks for confirming your email address and activating your profile.</h3>
                    <div class="clearfix spacer"></div>
                        <a href="#login-box" class="button front-page-button login-window" style="font-size:25px;">LOG IN</a>
                    <div class="clearfix spacer"></div>  
                </div>
            <div class="col-md-3"></div>         
            </div>
                <div class="clearfix spacer"></div>
                <div class="clearfix spacer"></div>  
        </div> 
        <div class="clearfix spacer"></div>
    </div>            
    @include('auth.login-panel')

@endsection

@section('footer')

    @include('layouts.footer')

@endsection

@section('javascript')
    <script>
        $(document).ready(function(){
            if (Modernizr.touch) {
                // show the close overlay button
                $(".close-overlay").removeClass("hidden");
                // handle the adding of hover class when clicked
                $(".box").click(function(e){
                    if (!$(this).hasClass("hover")) {
                        $(this).addClass("hover");
                    }
                });
                // handle the closing of the overlay
                $(".close-overlay").click(function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    if ($(this).closest(".box").hasClass("hover")) {
                        $(this).closest(".box").removeClass("hover");
                    }
                });
            } else {
                // handle the mouseenter functionality
                $(".box").mouseenter(function(){
                    $(this).addClass("hover");

                })
                // handle the mouseleave functionality
                .mouseleave(function(){
                    $(this).removeClass("hover");
                });
            }
        });     

        $(document).ready(function() {
            $('a.login-window').click(function() {
                //Getting the variable's value from a link
                var loginBox = $(this).attr('href');

                //Fade in the Popup
                $(loginBox).fadeIn(300);

                //Set the center alignment padding + border see css style
                var popMargTop = ($(loginBox).height() + 24) / 2;
                var popMargLeft = ($(loginBox).width() + 24) / 2;

                $(loginBox).css({
                    'margin-top' : -popMargTop,
                    'margin-left' : -popMargLeft
                });

                // Add the mask to body
                $('body').append('<div id="mask"></div>');
                $('#mask').fadeIn(300);

                return false;
            });

            // When clicking on the button close or the mask layer the popup closed
            $('a.close-login, #mask').on('click', function() {
                $('#mask , .login-popup').fadeOut(300 , function() {
                    $('#mask').remove();
                });
                return false;
            });

        });

    </script>
@endsection