@extends('layouts.default')

@section('title', 'Login')

@section('header')
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
@endsection


@section('content')
</div> <!-- <<< Closing Container little hack >>> -->

<div id="home-bg-cover" style="background-image:url('/assets/images/video-frame.png'); background-size:cover; height:620px; text-align:left;">

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header ">
				<button type="button" class="navbar-toggle collapsed home-menu-top-margin" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="navbar-brand col-lg-3 col-md-3 col-sm-3 hidden-xs"><a href="{{ url('/') }}"><img class="img-responsive header-logo-image" src="/assets/images/CVideon-logo-white.png"></a></div>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right home-menu-top-margin">
					<li><a href="{{ url('/') }}">FOR JOB SEEKERS</a></li>
					<li><a href="{{ url('/company') }}">FOR COMPANIES</a></li>
					<li><a href="{{ url('/auth/register') }}">REGISTER</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>

    <div class="visible-xs">
        <a href="{{ url('/') }}"><img id="mobile-logo" class="img-responsive col-xs-7 col-xs-push-2" src="/assets/images/CVideon-logo-white.png"></a>
    	<div class="clearfix"></div>
    </div>

	<div class="clearfix spacer"></div>
	<div class="container-fluid" style="min-height:450px;">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default  register-panel">
					<div class="panel-heading">Login</div>
					<div class="panel-body">
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<strong>Whoops!</strong> There were some problems with your input.<br><br>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">

							<div class="form-group">
								<label class="col-md-4 control-label">E-Mail Address</label>
								<div class="col-md-6">
									<input type="email" class="form-control" name="email" value="{{ old('email') }}">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Password</label>
								<div class="col-md-6">
									<input type="password" class="form-control" name="password">
									<label style="color:#999; float:right; font-family:'Raleway-Light'; padding-top:7px; margin-right:5px;">
										<input type="checkbox" name="remember"> Remember Me
									</label>
									<input type="submit" class="button" value="Login" style="margin-top:20px;">	<br />
									<a class="" style="color:#999; float:right; font-family:'Raleway-Light'; margin-top:7px; margin-right:5px;" href="{{ url('/password/email') }}">Forgot Your Password?</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection 
 
@section('footer')

	    @include('layouts.footer')
	    
@endsection
