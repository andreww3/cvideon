<div id="login-box" class="login-popup">
    <a href="#" class="close-login"><i class="fa fa-close"></i></a>

    <form method="post" class="signin" action="{{ url('/auth/login') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset class="textbox">
            <h2>Sign in</h2>
            <label class="username">
                <input class="form-control" name="email" type="text" autocomplete="on" placeholder="Your Email" required>
            </label>
            <label class="password">
                <input class="form-control" id="password" name="password" type="password" placeholder="Your Password" required>
                <p style="text-align:right; margin-bottom:0px;">
                    <a class="forgot" href="{{ url('/retrieve-password') }}" target="_self">Forgot your password?</a>
                </p>
            </label>
            <input class="form-control" id="login-button" type="submit" value="Sign in">
            <p>Not a user yet? <a href="{{ url('/register') }}">Sign Up</a></p>
        </fieldset> 
    </form>
</div>