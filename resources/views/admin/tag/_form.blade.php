
<div class="form-group">
  <label for="tagtype" class="col-md-3 control-label">
    Tag Type
  </label>
  <div class="col-md-8">
    <select class="form-control" name="tag_type_id">
           	<option @if ($tag_type_id == 0) selected @endif value="0">Choose a tag type</option>
            <option @if ($tag_type_id == 1) selected @endif value="1">Personal</option>
            <option @if ($tag_type_id == 2) selected @endif value="2">Professional</option>
            <option @if ($tag_type_id == 3) selected @endif  value="3">Job</option>
            <option @if ($tag_type_id == 4) selected @endif  value="4">Company</option>
            <option @if ($tag_type_id == 5) selected @endif  value="5">Blog</option>
    </select>
  </div>
</div>