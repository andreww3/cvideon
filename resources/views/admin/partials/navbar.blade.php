<ul class="nav navbar-nav" style="overflow:auto;">
  @if (Auth::check())
    <li @if (Request::is('admin/user*')) class="active" @endif>
      <a href="/admin/user">Users</a>
    </li>
    <li @if (Request::is('admin/post*')) class="active" @endif>
      <a href="/admin/post">Posts</a>
    </li>
    <li @if (Request::is('admin/category*')) class="active" @endif>
      <a href="/admin/category">Categories</a>
    </li>
    <li @if (Request::is('admin/tag*')) class="active" @endif>
      <a href="/admin/tag">Tags</a>
    </li>
    <li @if (Request::is('admin/upload*')) class="active" @endif>
      <a href="/admin/upload">Uploads</a>
    </li>
  @endif
  <li><a href="/blog">Blog Frontpage</a></li>
</ul>

<ul class="nav navbar-nav navbar-right">
  @if (Auth::guest())
    <li><a href="/auth/login">Login</a></li>
  @else
    <li><a>{{ Auth::user()->name }} </a></li>
    <li><a href="/user/{{ Auth::user()->id }}" target="_blank">Profile</a></li>
    <li><a href="/messages/{{ Auth::user()->id }}" target="_blank">
        <!--<span ng-show="unreadMessages != 0">
          ({[unreadMessages]} )
        </span>-->
                    Inbox</a></li>
    <li><a href="/auth/logout">Logout</a></li>
  @endif
</ul>