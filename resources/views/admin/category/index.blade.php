@extends('layouts.admin')

@section('content')
  <div class="container-fluid">
    <div class="row page-title-row">
      <div class="col-md-6">
        <h3>Categories <small>» Listing</small></h3>
      </div>
      <div class="col-md-6 text-right">
        <a href="/admin/category/create" class="btn btn-success btn-md">
          <i class="fa fa-plus-circle"></i> New Category
        </a>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">

        @include('admin.partials.errors')
        @include('admin.partials.success')

        <table id="tags-table" class="table table-striped table-bordered">
          <thead>
          <tr>
            <th>Category</th>
            <th>Posts</th>
            <th data-sortable="false">Actions</th>
          </tr>
          </thead>
          <tbody>
          @foreach ($categories as $category)
            <tr>
              <td>{{ $category->name }}</td>
              <td>{{-- Number of posts inside a category --}}</td>
              <td>
                  <form method="POST" action="/admin/category/{{ $category->id }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="DELETE">

                    <button type="submit" class="btn btn-xs btn-danger">
                      <i class="fa fa-times-circle"></i> Delete
                    </button>
                  </form>
                </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

@stop

@section('scripts')
  <script>
    $(function() {
      $("#tags-table").DataTable({
      });
    });
  </script>
@stop