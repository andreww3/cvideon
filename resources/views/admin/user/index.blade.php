@extends('layouts.admin')

@section('content')
  <div class="container-fluid">
    <div class="row page-title-row">
      <div class="col-md-6">
        <h3>Users <small>» Listing</small></h3>
      </div>
      <div class="col-md-6 text-right">
        <a href="/admin/user/create" class="btn btn-success btn-md">
          <i class="fa fa-plus-circle"></i> New User
        </a>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">

        @include('admin.partials.errors')
        @include('admin.partials.success')

        <table id="tags-table" class="table table-striped table-bordered">
          <thead>
          <tr>
            <th>User</th>
            <th>Company</th>
            <th>Email</th>
            <th>Title</th>
            <th data-sortable="false">Actions</th>
          </tr>
          </thead>
          <tbody>
          @foreach ($users as $user)
            <tr>
              <td>{{ $user->name }}</td>
              <td>{{ $user->userable->company['name'] }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->userable->status_quo}}{{$user->userable->company['title']}}</td>
              <td>
                <a href="/admin/user/{{ $user->id }}/edit"
                  style="float:left; margin-right:10px;"
                   class="btn btn-xs btn-info">
                  <i class="fa fa-edit"></i> Edit
                </a>

                <form method="POST" style="float:left; margin-right:10px;" action="/admin/user/{{ $user->id }}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-xs btn-danger">
                    <i class="fa fa-times-circle"></i> Delete
                  </button>
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  <script>
    $(function() {
        // ----> enabling paging <--- //
        $("#tags-table").DataTable({
        });
    });
  </script>
@stop