@extends('layouts.admin')

@section('content')
  <div class="container-fluid">
    <div class="row page-title-row">
      <div class="col-md-12">
        <h3>Users <small>» Edit user</small></h3>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row" ng-controller="adminCtrl">


      <div class="col-md-7">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">User Info</h3>
          </div>
          <div class="panel-body">

            @include('admin.partials.errors')
            @include('admin.partials.success')

            <form class="form-horizontal" role="form" method="POST"
                  action="/admin/user/{{ $id }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="PUT">
              <input type="hidden" name="id" value="{{ $id }}">

              <div class="form-group">
                <label for="tag" class="col-md-3 control-label">Name</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="name"
                         value="{{ $name }}" autofocus>
                </div>
              </div>

              <div class="form-group">
                <label for="tag" class="col-md-3 control-label">Email</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="email"
                         value="{{ $email }}" autofocus>
                </div>
              </div>

              @if($userable_type == "App\Seeker")

              <div class="form-group">
                <label for="tag" class="col-md-3 control-label">Status Quo</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="status_quo"
                         value="{{ $userable['status_quo'] }}" autofocus>
                </div>
              </div>
              @endif


              @if($userable_type == "App\CompanyUser")

              <div class="form-group">
                <label for="tag" class="col-md-3 control-label">Profile Title</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="company_title"
                         value="{{ $userable['company']['title'] }}" autofocus>
                </div>
              </div>
              @endif

              <br />
              <div class="clearfix"></div>
              <div class="form-group">
                <div class="col-md-7 col-md-offset-3">
                  <button type="submit" class="btn btn-primary btn-md">
                    <i class="fa fa-save"></i>
                      Save Changes
                  </button>
                  <button type="button" class="btn btn-danger btn-md"
                          data-toggle="modal" data-target="#modal-delete">
                    <i class="fa fa-times-circle"></i>
                    Delete
                  </button>
                </div>
              </div>

            </form>

          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">User Role</h3>
          </div>
          <div class="panel-body">

            @include('admin.partials.errors')
            @include('admin.partials.success')

            <form class="form-horizontal" role="form" method="POST"
                  action="/admin/user/{{ $id }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="PUT">
              <input type="hidden" name="id" value="{{ $id }}">

              <div class="form-group">
                @if (!empty($roles))
                  <label class="col-md-3 control-label">
                    Current Roles
                  </label>
                  <label class="col-md-9">
                    @foreach ($roles as $role)
                      <span style="margin-right:10px;">
                          "<i style="padding-top:10px;">{{ $role['name'] }}</i>"
                      </span>
                    @endforeach
                    <br />
                  </label>
                  <input type="hidden" value="" name="role">
                @endif
                <div class="clearfix"></div>

                @if ( empty($roles) )
                <label for="role" class="col-md-3 control-label">Give role:</label>
                <div class="col-md-7">
                  <select name="role" class="form-control">
                      <option value default>Subscriber</option>
                      <option value="member">Member</option>
                      <option value="author">Author</option>
                      <option value="editor">Editor</option>
                      <option value="admin">Administrator</option>
                  </select><br />                
                </div>
                @endif
              </div>
              
              <br />
              <div class="clearfix"></div>
              <div class="form-group">
                <div class="col-md-7 col-md-offset-3">
                  <button type="submit" class="btn btn-primary btn-md">
                    <i class="fa fa-save"></i>
                      Save Changes
                  </button>
                </div>
              </div>

            </form>

          </div>
        </div>
      </div>

      <div class="col-md-5">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Video</h3>
          </div>
          <div class="panel-body">       

              <div class="form-group">
                @if (empty($videos))

                  <div class="col-md-12">

                      <div class="uploadVideoTable"
                        style="width: 100%; height: 280px; background-color: #C4D8E2; padding-top:110px; text-align: center;">
                          
                          <div class="col-md-12" ng-if="upload_loaded == true">
                              <h4 style="margin-top:15px;">New video uploaded!</h4>
                          </div>

                          <div class="col-md-12" ng-if="upload_loaded == false" ng-hide="loadingVideoUpload">

                            <div class="col-md-12 video-form-options">
                              <button ng-hide="loadingVideoUpload" class="btn btn-primary btn-lg" ngf-select="upload({{ $id }}, $file, '/admin/add/video')" ng-model="file" name="file" ngf-pattern="'video/*'" accept="video/*" ngf-max-size="100MB"><i class="fa fa-save"></i>
                                  Upload Video
                              </button>
                            </div>
                            <br />
                            <br /><br />
                            <div class="clearfix spacer"></div>
                            <p><i>* Upload a video of maximum 90 seconds or 70 mb.</i></p>
                          </div>

                         <table ng-show="loadingVideoUpload"
                              style="width:100%; text-align:center; margin-top:10px; font-family:'Raleway-Regular';">
                            <tr>
                                <td><img src="assets/images/loader.gif"></td>
                            </tr>
                            <tr>
                                <td><p style="padding-top:10px; color:#5a1d4f;">Please wait until the video is uploaded...</p><td>
                            </tr>
                        </table>                         
                      </div>

                  </div>
                @endif

                @if (!empty($videos))

                  @foreach ($videos as $video)
                      <br />
                        
                      <video width="100%" height="260" controls>
                        <source src="{{ $video['value'] }}" type="video/mp4">
                        Your browser does not support the video tag.
                      </video>
                      <br />
                      <br /></br>

                      <label for="role" class="control-label">Remove user video:</label>
                      <br />
                      <button class="btn btn-danger btn-md" ng-click="deleteVideo( {{ $video['id'] }} )">Remove</button>

                  @endforeach

                @endif

              </div>
          </div>
        </div>
      </div>

    </div>

  </div>

  {{-- Confirm Delete --}}
  <div class="modal fade" id="modal-delete" tabIndex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            ×
          </button>
          <h4 class="modal-title">Please Confirm</h4>
        </div>
        <div class="modal-body">
          <p class="lead">
            <i class="fa fa-question-circle fa-lg"></i>  
            Are you sure you want to delete this user?
          </p>
        </div>
        <div class="modal-footer">
          <form method="POST" action="/admin/user/{{ $id }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="DELETE">
            <button type="button" class="btn btn-default"
                    data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-danger">
              <i class="fa fa-times-circle"></i> Yes
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>

@stop