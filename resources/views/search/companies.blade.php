@inject('countries', 'App\Country')
@inject('categories', 'App\Category')

@extends('layouts.default')

@section('title', 'Search for companies')

@section('menu')

    @include('search.menu')

@endsection

@section('content')

    <div class="container" ng-controller="searchCtrl">
        <hr/>
        <div class="col-md-12" ng-controller="searchCompanies">
            <div class="col-md-4">
            <h3 class="filters-title">Filters:</h3>
                <div class="search-pannel">

                <form name="forms.search">
                    <!--<div class="search-box">
                        <h5>Company</h5>
                            <input type="text" class="form-control" placeholder="Search by name.."
                                   ng-model="searchinput"
                                   uib-typeahead="obj as obj.text for obj in getSuggestions($viewValue, 'company')"
                                   typeahead-on-select="onSelect($item, $model, $label)"
                                   typeahead-select-on-exact="true"
                                   typeahead-wait-ms="500"
                                    >
                    </div>-->

                    <div class="search-box">
                        <h5>Categories</h5>
                        <select class="form-control" ng-model="category">
                            <option value="">Select</option>
                            <option ng-repeat="obj in categories" value="{[obj.id]}">{[obj.name]}</option>
                        </select>
                    </div>

                    <div class="search-box" ng-show="category.id != shownCategory" style="padding-top:15px">
                        <h5>Sub-categories</h5>
                        <div ng-repeat="category in categories">
                            <label class="col-md-12 checklist-item" ng-hide="category.id != shownCategory"
                                   ng-repeat="subcategory in category.sub_categories">
                                <input type="checkbox" checklist-model="subcategories" checklist-value="subcategory.id"
                                       ng-change="check(subcategory.id, checked)"/>
                                {[ subcategory.name ]}
                            </label>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="search-box">
                        <h5>Location</h5>
                        <select class="form-control" ng-model="country">
                            <option value="">Select a country</option>
                            <option ng-repeat="country in countries" value="{[country.id]}">{[country.name]}</option>
                        </select>
                    </div>
                    
                    <div class="search-box">
                        <h5>Languages</h5>
                        <div     
                            isteven-multi-select
                            input-model="languages"
                            output-model="search.languages"
                            button-label="icon name"
                            item-label="icon name maker"
                            tick-property="ticked">
                        </div>
                    </div> 

                    <div class="search-box">
                        <h5>Keywords</h5>

                        <div     
                            isteven-multi-select
                            input-model="companyTags"
                            output-model="search.companyTags"
                            button-label="icon name"
                            item-label="icon name maker"
                            tick-property="ticked">
                        </div>
                    </div>

                    <div class="search-box"> 
                        <h5>Extra</h5>

                        <label class="col-md-12 checklist-item">
                            <input type="checkbox" name="onlyVideos" value="yes" ng-model="videoFilterModel" checked>
                            Display only results with a video
                        </label>
                        <div class="clearfix"></div>
                    </div>

                    <div class="search-box">
                        <h5></h5>
                        <button class="btn search-button btn-block" type="submit" ng-click="searchCompanies()">Search</button>
                    </div>
                </form>
                </div>
            </div>

            <div class="col-md-7 results-pannel">
                <h3 class="results-title">List of companies:</h3>

                <div class="row half-spacer result-box" ng-init="getRandom" ng-repeat="result in searchResults" ng-style="{'background-image': 'url( {[result.cover_picture]} )'}">
                    <div class="col-md-3">
                        <a href="#" class="btn-block" data-toggle="modal" data-target="#video-modal" ng-click="selectResult(result)">

                            <img class="img-thumbnail img-responsive"
                                 ng-src="{[ result.profile_picture ]}"/>
                        </a>
                    </div>

                    <div class="col-md-9">
                        <div style="background:rgba(255,255,255,0.6); height:142px; margin-top:0px; padding:7px; border-radius: 4px;">
                            <div class="col-md-9">
                                <div class="text text-primary">
                                    <h4 style="font-size:18px">
                                        <b>{[ result.userable.company.name ]}</b>
                                    </h4>
                                </div>

                                <div class="text text-muted">

                                    <p><b>{[ result.userable.company.title ]}</b></p>

                                    <h5>{[ result.userable.company.number_of_employees ]}</h5> 

                                    <p>{[ result.country.name ]}</p>
                                    <!--<h6 ng-if="result.videos.length > 0">Has video</h6>-->
                                </div>           
                            </div>          
       
                            <div class="col-md-3 result-options">
                                <a href="#" ng-if="result.videos.length > 0" class="btn options-button btn-block" ng-click="selectResult(result)" data-toggle="modal" data-target="#video-modal">Video</a>
                                <a href="company/{[result.id]}" class="btn options-button btn-block" target="_blank">Profile</a>
                                <a class="btn options-button btn-block" ng-click="selectResult(result); openOnlyContact(result);" data-toggle="modal" data-target="#video-modal">Contact</a>
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="alert alert-info" ng-hide="zeroMatches">
                    There are no matching results for now.
                </div>
            </div>


            <div class="modal fade" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="video-modalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fa fa-times"></i></span>
                            </button>
                            <h3 id="video-modalLabel">{[ selectedResult.userable.company.name ]}</h3> 
                            <h4 ng-if="selectedResult.userable.company.title">{[selectedResult.userable.company.title]}</h4>

                            <div ng-show="contactOnly" class="text-center">
                                <a ng-click="contactOnly = false" ng-show="contactOnly">
                                    <h4 style="color:rgb(30, 60, 100);">View video</h4>
                                </a>
                            </div>   
                        </div>
                        <div class="modal-body" ng-hide="contactOnly">
                            <videogular vg-theme="config.theme" ng-if="selectedResult.videos.length" style="height: 300px;">
                                <vg-media vg-src="config.sources"></vg-media>

                                <vg-controls>
                                    <vg-play-pause-button></vg-play-pause-button>
                                    <vg-time-display>{[ currentTime | date:'mm:ss' ]}</vg-time-display>
                                    <vg-scrub-bar>
                                        <vg-scrub-bar-current-time></vg-scrub-bar-current-time>
                                    </vg-scrub-bar>
                                    <vg-time-display>{[ timeLeft | date:'mm:ss' ]}</vg-time-display>
                                    <vg-volume>
                                        <vg-mute-button></vg-mute-button>
                                        <vg-volume-bar></vg-volume-bar>
                                    </vg-volume>
                                    <vg-fullscreen-button></vg-fullscreen-button>
                                </vg-controls>

                                <vg-overlay-play></vg-overlay-play>
                                <vg-buffering></vg-buffering>
                            </videogular>
                            <div class="alert alert-info" ng-hide="selectedResult.videos.length">
                                The user has not uploaded a video yet.
                            </div>
                        </div>

                        @include('messaging.component')
                    </div>

                </div>
            </div>
        <div class="clearfix spacer"></div>
        </div>
    </div>
@endsection

@section('footer')

        @include('layouts.footer')

@endsection

@section('javascript')
    <script>
        $(function () {
            $('#video-modal').on('hidden.bs.modal', function () {
                $('video').trigger('pause');
                $('source').remove();
                $('video').load();
            });
        });
    </script>
@endsection
