@extends('layouts.default')

@section('title', 'About')

@section('content')
</div> <!-- <<< Closing Container little hack >>> -->
    <div id="home-bg-cover" style="height:340px; background-image:url('/assets/images/About.png'); background-size:cover;">
        <div id="home-top-content">

            @include('facade.menu')

            <div id="homeIntro" class="text-center" style="margin-top:10px;">
                <div class="visible-xs">
                    <a href="{{ url('/') }}"><img id="mobile-logo" class="img-responsive col-xs-7 col-xs-push-2" src="/assets/images/CVideon-logo-white.png"></a>
                </div>
                <div class="clearfix spacer"></div>
                <div><h1 class="text-center">Who are we?</h1></div> 
                <div class="clearfix spacer"></div>
            </div>
        </div>
    </div>
    @include('auth.login-panel')

    <div class="on-white-background">
        
        <div id="boxes">
            <div class="clearfix spacer"></div>
            <h2 class="text-center" style="font-size:38px; margin-top:0px; margin-bottom:30px;">THE TEAM</h2>

            <div class="col-md-3">
                <div class="picture-box">
                    <img src="assets/images/team/Alfonso.jpg" />
                    <div class="overlay">
                        <h3>Alfonso Segarra</h3>
                        <h4>Co-founder</h4>
                        <h4>Business Developer</h4>
                        <h4><a href="mailto:alfonso@cvideon.com">Alfonso@cvideon.com</a></h4>
                        <a class="close-overlay hidden">x</a>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="picture-box">
                    <img src="assets/images/team/Stefan.png" />
                    <div class="overlay">
                        <h3>Stefan Pavic</h3>
                        <h4>Co-founder</h4>
                        <h4>Market Developer</h4>
                        <h4><a href="mailto:stefan@cvideon.com">Stefan@cvideon.com</a></h4>
                        <a class="close-overlay hidden">x</a>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="picture-box">
                    <img src="assets/images/team/Andrei.png" />
                    <div class="overlay">
                        <h3>Andrei Cristian</h3>
                        <h4>Co-creator</h4>
                        <h4>IT Coordinator</h4>
                        <h4><a href="mailto:andrei@cvideon.com">Andrei@cvideon.com</a></h4>
                        <a class="close-overlay hidden">x</a>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="picture-box">
                    <img src="assets/images/team/David.png" />
                    <div class="overlay">
                        <h3>David Hoiness</h3>
                        <h4>Marketing Coordinator</h4>
                        <h4><a href="mailto:david@cvideon.com">David@cvideon.com</a></h4>
                        <a class="close-overlay hidden">x</a>
                    </div>
                </div>
            </div>

            <div class="half-spacer col-md-12 hidden-xs"></div>


            <div class="col-md-3">
                <div class="picture-box">
                    <img src="assets/images/team/Thibault.png" />
                    <div class="overlay">
                        <h3>Thibault Pean</h3>
                        <h4>Video Designer</h4>
                        <h4><a href="mailto:thibault@cvideon.com">Thibault@cvideon.com</a></h4>
                        <a class="close-overlay hidden">x</a>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="picture-box">
                    <img src="assets/images/team/Allan.png" />
                    <div class="overlay">
                        <h3>Allan Isaksen</h3>
                        <h4>Sales Coordinator</h4>
                        <h4><a href="mailto:Allan@cvideon.com">Allan@cvideon.com</a></h4>
                        <a class="close-overlay hidden">x</a>
                    </div>
                </div>
                <br />
            </div>

            <div class="col-md-3">
                <div class="picture-box">
                    <img src="assets/images/team/Andrea.png" />
                    <div class="overlay">
                        <h3>Andrea Fabris</h3>
                        <h4>Front-end Developer</h4>
                        <h4><a href="mailto:Andrea@cvideon.com">Andrea@cvideon.com</a></h4>
                        <a class="close-overlay hidden">x</a>
                    </div>
                </div>
                <br />
            </div>

            <div class="col-md-3">
                <div class="picture-box">
                    <img src="assets/images/team/Hendrik.png" />
                    <div class="overlay">
                        <h3>Hendrik Mahling</h3>
                        <h4>Business Developer</h4>
                        <h4><a href="mailto:hendrik@cvideon.com">Hendrik@cvideon.com</a></h4>
                        <a class="close-overlay hidden">x</a>
                    </div>
                </div>
                <br />
            </div>
            <div class="half-spacer col-md-12"></div>
        </div>
            
        <h2 class="text-center" style="font-size:38px; margin-top:0px; margin-bottom:30px;">OUR STORY:</h2>

        <img src="assets/images/Storyline.png" style="width:80%; margin-left:10%;" />
    </div>
    <div class="spacer"></div>    <div class="half-spacer"></div>
@endsection

@section('footer')
    
    @include('layouts.footer')

@endsection

@section('javascript')
    <script>
        $(document).ready(function(){
        if (Modernizr.touch) {
            // show the close overlay button
            $(".close-overlay").removeClass("hidden");
            // handle the adding of hover class when clicked
            $(".picture-box").click(function(e){
                if (!$(this).hasClass("hover")) {
                    $(this).addClass("hover");
                }
            });
            // handle the closing of the overlay
            $(".close-overlay").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                if ($(this).closest(".picture-box").hasClass("hover")) {
                    $(this).closest(".picture-box").removeClass("hover");
                }
            });
        } else {
            // handle the mouseenter functionality
            $(".picture-box").mouseenter(function(){
                $(this).addClass("hover");

            })
            // handle the mouseleave functionality
            .mouseleave(function(){
                $(this).removeClass("hover");
            });
        }
    });  

    $(document).ready(function() {
        $('a.login-window').click(function() {

            //Getting the variable's value from a link
            var loginBox = $(this).attr('href');

            //Fade in the Popup
            $(loginBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(loginBox).height() + 24) / 2;
            var popMargLeft = ($(loginBox).width() + 24) / 2;

            $(loginBox).css({
                'margin-top' : -popMargTop,
                'margin-left' : -popMargLeft
            });

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return false;
        });

        // When clicking on the button close or the mask layer the popup closed
        $('a.close-login, #mask').on('click', function() {
            $('#mask , .login-popup').fadeOut(300 , function() {
                $('#mask').remove();
            });
            return false;
        });

    });
    </script>
@endsection