@extends('layouts.default')

@section('title', 'Contact Us')

@section('header')
    <script src="https://maps.googleapis.com/maps/api/js"></script>
@endsection

@section('content')
</div> <!-- <<< Closing Container little hack >>> -->
    <div id="home-bg-cover" style="height:340px; background-image:url('/assets/images/contact.png'); background-size:cover;">
    
        <div id="home-top-content">

            @include('facade.menu')

            <div id="homeIntro" class="text-center" style="margin-top:10px;">
                <div class="visible-xs">
                    <a href="{{ url('/') }}"><img id="mobile-logo" class="img-responsive col-xs-7 col-xs-push-2" src="/assets/images/CVideon-logo-white.png"></a>
                </div>
                <div class="clearfix spacer"></div>
                <div><h1 class="text-center">CONTACT</h1></div> 
                <div class="clearfix spacer"></div>
            </div>

            <h2 class="text-center" style="font-family:'Raleway-SemiBold'; font-size:50px; margin-top:120px;">CONTACT</h2>
        
        </div>
    </div>
    <div class="clearfix spacer"></div>

    @include('auth.login-panel')

    <div class="clearfix"></div>
    <div class="half-spacer"></div>
    <div class="col-md-1"></div>
    <div class="on-white-background col-md-10">
         <div class="col-md-4 text-left">
            <h3>Get in touch</h3>
            <p>Please feel free to contact us if you have any enquiries about the website, suggestions, business and investor relations. </p>
            <h3>Call or send us an email</h3>
            <p>info@cvideon.com</p>
            <p>+45 22 25 73 30</p>
            <br />
            <h3>Find us at</h3>
            <p>Porcelænshaven 26</p>
            <p>2000 Frederiksberg</p>
            <p>Denmark</p> 
            <br />
            <div id="map" style="width:78%; height:190px; background-color:#1e3c64; border-radius:5px;"></div>
        </div>       
        
        <div class="col-md-8">
            <div class="half-spacer"></div>
            <div class="visible-xs"><h3>Write to us here..</h1></div>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul> 

            @if(Session::has('message'))
                <div class="alert alert-info">
                    {{Session::get('message')}}
                </div>
            @endif

            {!! Form::open(array('route' => 'contact_store', 'class' => 'form')) !!}

            <div class="form-group">
                {!! Form::text('name', null,
                    array('required',
                        'class' => 'form-control',
                        'placeholder' => 'Your name')) !!}
            </div>

            <div class="form-group">
                {!! Form::text('email', null,
                        array('required',
                            'class' => 'form-control',
                            'placeholder' =>'Your email address')) !!}
            </div>

            <div class="form-group">
                {!! Form::text('phone', null,
                        array('class' => 'form-control',
                            'placeholder' =>'Your phone number')) !!}
            </div>

            <div class="form-group">
                {!! Form::text('subject', null,
                        array('class' => 'form-control',
                            'placeholder' =>'The subject')) !!}
            </div>

            <div class="form-group">
                {!! Form::textarea('message', null,
                            array('required',
                                'class' => 'form-control',
                                'placeholder' => 'Your message')) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Contact Us!',
                    array('class' => 'button')) !!}
            </div>

            {!! Form::close() !!}
        </div>


    </div>
    <div class="col-md-1"></div>
    <div class="spacer"></div> 
    <div class="clearfix spacer"></div>
       <div class="half-spacer"></div>
@endsection

@section('footer')

    @include('layouts.footer')
    
@endsection

@section('javascript')
    <script>
        function initialize() {
            var mapCanvas = document.getElementById('map');
            var myLatLng = {lat: 55.677864, lng: 12.521467};
            var map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map
            });
            
        }

        google.maps.event.addDomListener(window, 'load', initialize);


        $(document).ready(function() {
            $('a.login-window').click(function() {

                //Getting the variable's value from a link
                var loginBox = $(this).attr('href');

                //Fade in the Popup
                $(loginBox).fadeIn(300);

                //Set the center alignment padding + border see css style
                var popMargTop = ($(loginBox).height() + 24) / 2;
                var popMargLeft = ($(loginBox).width() + 24) / 2;

                $(loginBox).css({
                    'margin-top' : -popMargTop,
                    'margin-left' : -popMargLeft
                });

                // Add the mask to body
                $('body').append('<div id="mask"></div>');
                $('#mask').fadeIn(300);

                return false;
            });

            // When clicking on the button close or the mask layer the popup closed
            $('a.close-login, #mask').on('click', function() {
                $('#mask , .login-popup').fadeOut(300 , function() {
                    $('#mask').remove();
                });
                return false;
            });

        });
    </script>
@endsection