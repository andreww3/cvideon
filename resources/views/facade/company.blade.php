@extends('layouts.default')

@section('title', 'Company')

@section('content')
</div> <!-- <<< Closing Container little hack >>> -->
    <div id="home-bg-cover" style="background-image:url('/assets/images/Looking_at_cvideo.png'); background-size:cover;">

        <div id="home-top-content">

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header ">
                        <button type="button" class="navbar-toggle collapsed home-menu-top-margin" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="navbar-brand col-lg-3 col-md-3 col-sm-3 hidden-xs"><a href="{{ url('/') }}"><img class="img-responsive header-logo-image" src="/assets/images/CVideon-logo-white.png"></a></div>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right home-menu-top-margin">
                            <li><a href="{{ url('/register-company') }}">SIGN UP</a></li>
                            <li><a href="#login-box" class="login-window">LOG IN</a></li>
                            <li><a href="{{ url('/') }}">FOR JOB SEEKERS</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div id="homeIntro" class="text-center">
                <div class="visible-xs">
                    <a href="{{ url('/') }}"><img id="mobile-logo" class="img-responsive col-xs-7 col-xs-push-2" src="/assets/images/CVideon-logo-white.png"></a>
                </div>
                <div class="clearfix spacer"></div>
                <div><h1 class="text-center">Humanize your company!</h1></div>
                <h3 class="hidden-xs">Promote your company through videos.<br /> Search candidates and watch them perform.</h3> 
                <div class="clearfix spacer"></div>
                <a href="{{ url('/register-company') }}" class="button front-page-button hidden-xs" style="font-size:30px;">Register your company</a>
                <div class="clearfix spacer"></div>
                <img class="hidden-xs" src="assets\images\Arrow.png" style="width:70px; height:auto;"/>
            </div>
        </div>
    </div>

    @include('auth.login-panel')

    <div class="spacer" id="home-description">
        <div class="col-md-1"></div>
        <div class="col-md-10"> 
            <h1 class="text-center">What is CVideon?</h2>
            <h3 class="text-center">The centralized video-sharing platform where users and companies can promote themselves through videos.</h3>
        </div>
        <div class="col-md-1"></div>
        <div class="clearfix spacer hidden-xs"></div>
        <div class="spacer"></div>
        <div class="col-md-4">
            <div class="icon-bg"><img src="/assets/images/icons/Icon_CandidateSearch.png"></div>
            <h3>Candidate Search</h3>
            <p>Browse video CVs and get a better impression of the person behind the skills. This creates better matches.</p>
        </div>
        <div class="col-md-4">
            <div class="icon-bg"><img src="/assets/images/icons/Icon_VideoDatabase.png"></div>
            <h3>Video Database</h3>
            <p>Place all your company video material into one centralized platform and let the jobseekers learn more about you.</p>
        </div>
        <div class="col-md-4">
            <div class="icon-bg"><img src="/assets/images/icons/Icon_Promotion.png"></div>
            <h3>Employer Branding</h3>
            <p>Get a company profile and use videos to promote your business and stand out. Or share a video job post with your followers.</p>
        </div>
                <div class="clearfix"></div>
        <div class="spacer"></div><div class="spacer"></div>
    </div>
    <div class="spacer"></div> <div class="spacer"></div> 
    <div class="clearfix"></div>
    <div class="row spacer" id="company-sub-description">
        <div class="spacer"></div>    <div class="half-spacer"></div> 
        <div class="spacer"></div>
        <div class="col-md-1"></div>
        <div class="col-md-10"> 
            <h1 class="text-center">Why use CVideon?</h1>
            <h3 class="text-center">Because job seekers can learn more about your company before applying and you can learn more about the candidate before the interview. </h3>
        </div>
        <div class="col-md-1"></div>
        <div class="clearfix spacer"></div>   
        <div class="spacer hidden-xs"></div><div class="spacer hidden-xs"></div>
            <div id="boxes">
                <div class="col-md-3">
                    <div class="box">
                        <p>LEARN MORE IN
                        <br />LESS TIME</p>
                        <div class="overlay">
                            <div class="inBoxParagraph">

                                <p>You absorb more information through a video CV than you would on a paper based CV.</p>
                                <br />
                                <p>Facilitate your screening <br />process and make it more enjoyable by browsing through video CVs.</p>
                            </div>
                            <a class="close-overlay hidden">x</a>
                        </div>
                    </div>  
                </div>

                <div class="col-md-3">
                    <div class="box">
                        <p>SEE CANDIDATE’S
                        <br />PERSONALITY</p>                
                        <div class="overlay"> 
                            <div class="inBoxParagraph">
                                <p>Video CVs allow candidates to showcase their personality <br />in a way that would not be possible on a traditional paper based CV. </p>
                                <br />
                                <p>Learn more about the person <br />whom you consider to hire.</p>
                            </div>
                            <a class="close-overlay hidden">x</a>
                        </div>                  
                    </div>
                </div>

                <div class="col-md-3">                
                    <div class="box"> 
                        <p>SHOW YOUR <br />WORK CULTURE</p>
                        <div class="overlay">
                            <div class="inBoxParagraph">
                                <p>Help candidates get a glimpse <br /> of what it’s like to work for your company.</p>
                                <br />
                                <p> Showcase your offices and the people behind your company to display your work culture <br />to future employees.</p>
                            </div>
                            <a class="close-overlay hidden">x</a>
                        </div>                  
                    </div>
                </div>

                <div class="col-md-3">                
                    <div class="box">
                        <p>STAND OUT AND <br />BE DIFFERENT</p>
                        <div class="overlay">
                            <div class="inBoxParagraph">
                                <p>Approach recruiting and <br />employer branding differently from your competitors. </p>
                                <br />
                                <p>Use video material to promote <br />your company and facilitate encounters with candidates.</p>
                            </div>
                            <a class="close-overlay hidden">x</a>
                        </div>                  
                    </div>
                </div>
                
            </div>

        <div class="clearfix spacer"></div>
        <div class="spacer"></div> <div class="spacer"></div>   
        <div class="clearfix spacer"></div>   
        <div class="spacer"></div> 
        <div class="half-spacer"></div> 
        <div class="container text-center">
            <a href="{{ url('/register-company') }}" class="button front-page-button hidden-xs" style="font-size:30px; margin:0 auto;">Join for free</a> 
        </div>
        <div class="clearfix spacer"></div>   
        <div class="half-spacer"></div> 
@endsection

@section('footer')

    @include('layouts.footer')
    
@endsection

@section('javascript')
    <script>
        $(document).ready(function(){
            if (Modernizr.touch) {
                // show the close overlay button
                $(".close-overlay").removeClass("hidden");
                // handle the adding of hover class when clicked
                $(".box").click(function(e){
                    if (!$(this).hasClass("hover")) {
                        $(this).addClass("hover");
                    }
                });
                // handle the closing of the overlay
                $(".close-overlay").click(function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    if ($(this).closest(".box").hasClass("hover")) {
                        $(this).closest(".box").removeClass("hover");
                    }
                });
            } else {
                // handle the mouseenter functionality
                $(".box").mouseenter(function(){
                    $(this).addClass("hover");

                })
                // handle the mouseleave functionality
                .mouseleave(function(){
                    $(this).removeClass("hover");
                });
            }
        }); 
        $(document).ready(function() {
            $('a.login-window').click(function() {

                //Getting the variable's value from a link
                var loginBox = $(this).attr('href');

                //Fade in the Popup
                $(loginBox).fadeIn(300);

                //Set the center alignment padding + border see css style
                var popMargTop = ($(loginBox).height() + 24) / 2;
                var popMargLeft = ($(loginBox).width() + 24) / 2;

                $(loginBox).css({
                    'margin-top' : -popMargTop,
                    'margin-left' : -popMargLeft
                });

                // Add the mask to body
                $('body').append('<div id="mask"></div>');
                $('#mask').fadeIn(300);

                return false;
            });

            // When clicking on the button close or the mask layer the popup closed
            $('a.close-login, #mask').on('click', function() {
                $('#mask , .login-popup').fadeOut(300 , function() {
                    $('#mask').remove();
                });
                return false;
            });

        });
    </script>
@endsection