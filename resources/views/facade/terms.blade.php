@extends('layouts.default')

@section('title', 'Terms and Conditions')

@section('content')
</div> <!-- <<< Closing Container little hack >>> -->
    <div id="home-bg-cover" style="height:340px; background-image:url('/assets/images/terms-bg.jpg'); background-size:cover;">
        <div id="home-top-content">

            @include('facade.menu')

            <div id="homeIntro" class="text-center" style="margin-top:10px;">
                <div class="visible-xs">
                    <a href="{{ url('/') }}"><img id="mobile-logo" class="img-responsive col-xs-7 col-xs-push-2" src="/assets/images/CVideon-logo-white.png"></a>
                </div>
                <div class="clearfix spacer"></div>
                <div><h1 class="text-center">The terms and conditions</h1></div> 
                <div class="clearfix spacer"></div>
            </div>
        </div>
    </div>
    @include('auth.login-panel')

    <div class="on-white-background">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h1 class="text-center"></h1>
            <p>Please read the following Terms and Conditions for being a user on CVideon in order for you to rightfully understand and accept them. By registering and/or signing in to CVideon you automatically agree to the Terms and Conditions for using CVideon.</p>
            <p>Version: 1.0</p>
            <p>Date of revision: 26th of August 2015</p>
            <p>These Terms and Conditions are valid until next revision.</p>
            <p>The responsible for the following Terms and Conditions are:</p>
            <p>CVideon IVS<br />Vodroffsvej 10, 1.<br />1900 Frederiksberg<br />Email: info@cvideon.com<br />CVR: 36902124</p>
        
            <p>In the following referred to as ʼCVideonʼ. The ʼCVideon platformʼ refers to the online platform administrated and created by CVideon and accessible through the website www.cvideon.com.</p>
            
            <h2>Registering and Creating a Profile</h2>
            <p>When you register and create a profile on the CVideon platform, the profile and its affiliated information is representative for you as a person or a company that you are authorized to create online profiles on behalf of.</p>
            <p>You are not allowed to register and create a profile with another personʼs or companyʼs email address or using fake email addresses. You are not allowed to register and create a profile that poses or can be misinterpreted as being another person other than yourself or representing a company other than one you have the authority to create a profile on behalf of.</p>
            <p>In the following, the term ʼUserʼ will include users registering both a personal (private) profile and a company (corporate) profile.</p>

            <h2>User Rules</h2>
            <p>As a User on the CVideon platform you are responsible for:</p>
            <ul>
                <li>Filling in correct and truthful information</li>
                <li>Any activity that takes place on a Userʼs profile and any potential third party involvement</li>
                <li>Any damage to CVideon, both financial and material, that could occur as a consequence of mismanagement of a Userʼs profile or wrong handling of the information provided on the profile</li>
                <li>Using the CVideon platform purely for its intent as a personal branding, employer branding and networking platform</li>
                <li>Not using a personal profile for marketing or promotion of other goods or services apart from you as a human being and not with other intents than personal branding and networking</li>
                <li>Not exposing any material on the CVideon platform, which relates to religious, political, sexual or racist aspects</li>
                <li>Not distributing any content or unwanted emails to other Users repeatedly or following a pattern that can be associated with spam</li>
                <li>Taking care of yourself and demonstrate proper precautions against scam, spam and phishing</li>
            </ul>

            <h2>Liability</h2>
            <p>CVideon is not liable towards Users registered on the CVideon platform in relation to:</p>
            <ul>
                <li>Any behaviour of other Users on the CVideon platform</li>
                <li>Any connection either written or verbal made between Users on or outside the CVideon platform</li>
                <li>Any direct or indirect economic or emotional loss to the User other than the fee paid by the User to CVideon, as a consequence of using the CVideon platform</li>
                <li>Any loss of data from the CVideon platform. The User is fully responsible for backing up data stored on the CVideon platform, including but not limited to text, images, audio and video. CVideon cannot be held responsible for the economic or emotional loss as a consequence of any actual or potential loss of data from the CVideon platform</li>
            </ul>

            <h2>User Privacy</h2>
            <p>When you register and sign up to the CVideon platform you agree to let CVideon contact you at any time regarding the following: </p>
            <ul>
                <li>Newsletters sent to your registered email address, containing information and data about personal users, companies, pages, job postings among others. The amount of information provided to you will be dependent on the amount of personal users, companies or pages that you follow. You can at any time request not to receive these emails by writing to support@cvideon.com</li>
                <li>CVideon holds the right to contact the User by email at any time regarding questionnaires about the platform, feedback requests or other purpose intended to improve or make changes to the CVideon platform. If you do not wish to receive these emails please contact support@cvideon.com</li>
                <li>CVideon holds the right to contact the User by email at any time regarding administrative, functional, legal or economic changes to the CVideon platform or use hereof</li>
                <li>CVideon holds the right to contact the User by email at any time under the suspicion or proven act of the Userʼs misuse of the CVideon platform or violation of the current Terms and Conditions</li>
            </ul>

            <h2>Data Collection </h2>
            <p>When a User register to the CVideon platform the User accepts that CVideon will keep personal information stored in CVideonʼs databases under surveillance and administration of CVideon. The User accepts that CVideon cannot be held responsible for any economical or emotional loss the User might experience due to personal information being disclosed to third parties without the knowledge or awareness of CVideon.</p>
            <p>Personal information about a User includes</p>
            <ul>
                <li>Name</li>
                <li>Profile picture</li>
                <li>Video-presentation </li>
                <li>City and country of current location </li>
                <li>Email address</li>
                <li>Phone number</li>
            </ul>
            <p>Please do not disclose any additional personal information on the CVideon platform, regarding such as health issues, membership of unions, previous criminal offence, sexual, religious or racial relations.</p>
            <p>A User accepts that part or all of this information is visible to or can be shared with other Users in order to facilitate a connection between these parties, with the purpose of branding and networking.</p>
            <p>CVideon is not responsible for any additional information to the above-mentioned information submitted by the User that can further identify the User. The User should be advised not to enclose any sensitive information in text, images, audio or video material uploaded to the CVideon platform.</p>
            <p>Logging on to the CVideon platform will be tracked by the use of cookies, which registers the Userʼs email address and access to the site, and remembers this information for the next login. Cookies can be disabled in the Internet browserʼs settings. The IP address of the computer from which the CVideon platform is accessed will also be registered. This is in order to locate the computer geographically to give a better understanding of the Userʼs behaviour and to counteract potential or actual illegal activities. CVideon holds the right to track the Userʼs behaviour on the CVideon platform in order to optimise the user experience or functionality of the CVideon platform.</p>
            <p>CVideon will also keep information about the Userʼs login password, in order to support the User in case of lost password. CVideon will at no point deliberately disclose the password to any party other than the owner of the email address registered to the User, who has lost its password. It is solely the email address ownerʼs responsibility that the owner alone has access to information forwarded to that email address.</p>
            <p>Any email sent to CVideon will be kept on the CVideon email databases for internal use. The email received will not be treated as confidential unless an agreement explicitly made between CVideon and the sender states otherwise.</p>

            <h2>CVideon Use of Personal Information </h2>
            <p>The User agree that CVideon can use the personal information provided on the CVideon platform in the following matters:</p>
            <p>Name: A Userʼs name is used to identify the person or company behind the profile. </p>
            <p>Profile picture: CVideon can use any photographical material uploaded to the CVideon platform as part of CVideons marketing material. CVideon will not pass on a Userʼs photographical content to any third party without the Userʼs permit. CVideon holds the right to inspect the Userʼs photographical material for violation of the Terms and Conditions without prior notice.</p>
            <p>Video-presentation: CVideon can use any video- and audio material uploaded to the CVideon platform as part of CVideons marketing material. CVideon will not pass on a Userʼs audio- and video content to any third party without the Userʼs permit. CVideon holds the right to inspect Userʼs audio- and video material for violation of the Terms and Conditions without prior notice.</p>
            <p>City and country of current location: The location of a User is used by CVideon to locate the Users of the CVideon platform geographically in order to understand User behaviour for marketing, networking or business development purposes.</p>
            <p>Email adress: A Userʼs email adress is used to send relevant information to the User in terms of newsletters or other information supplied by CVideon, which is more elaborately described in section 4.</p>

            <h2>CVideonʼs Distribution of Personal Data </h2>
            <p>A personal user is aware, that its profile will be visible for company members of the CVideon platform looking for candidates. The profile will also be available for nonmembers knowing the unique link.</p>
            <p>A company user is to be aware, that its profile will be visible for all registered Users on the CVideon platform. </p>
            <p>The User accepts that in case any service provided by CVideon is misused, CVideon will forward any information about the User in CVideonʼs disposal, to relevant authorities in case this is required.</p>
            <p>CVideon is allowed to distribute all information about a User on the CVideon platform to third party if the information is anonymous or if third party is included in a nondisclosure agreement or other agreement about confidentiality. Such distribution of information can take place in case of statistical analysis about the website, market research, marketing campaigns, general branding, or improvement of the services provided by CVideon.</p>
            
            <h2>Third Party Use of Data</h2>
            <p>CVideon can at any time make use of an external company to carry out the technical maintenance of the CVideon platform. If this happens, the external company will handle data under CVideonʼs responsibility.</p>
            <p>Any external company handling data under CVideonʼs responsibility will operate under the same regulations as CVideon in terms of confidentiality and otherwise explained in these Terms and Conditions.</p>
            <p>In case that CVideon is merged with or acquisitioned by another company the User accepts that any information provided to CVideon can be transferred to this new company, as long as the new company handles information the same way as CVideon and as described in these Terms and Conditions.</p>
            
            <h2>Conditions for Uploading Media </h2>
            <p>In the following, ʻMediaʼ refers to any text, picture, photo, audio or video material that can be entered directly in or uploaded to the CVideon platform.</p>
            <p>All Media uploaded to the CVideon platform must follow these guidelines:</p>
            <ul>
                <li>All Media material must be free from copyrights and/or other restricted or limited use. The User must possess the complete rights to any Media uploaded by the User to the CVideon platform</li>
                <li>If Media includes or identifies another person other than the User itself, these people must have given their acceptance of public display. </li>
                <li>All Media material must be clear and in good quality. CVideon holds the right to evaluate any Mediaʼs quality and delete the Media if considered inadequate.  </li>
                <li>Media uploaded by personal users must not contain logos, links or other references to external webpages or companies and/or their products. </li>
                <li>Appearance of any of the following, in any Media directly entered in or uploaded to the CVideon platform is not tolerated: 
                    <ul>
                        <li>violence and physical damage or encouragement hereof </li>
                        <li>political and religious war or encouragement hereof </li>
                        <li>threats</li>
                        <li>weapons </li>
                        <li>illegal drugs</li>
                        <li>un-sober behaviour </li>
                        <li>sexual behaviour or pornographic content </li>
                        <li>discriminating behaviour towards individuals or groups based on ethnicity, religion or sexual orientation</li>
                    </ul>
                </li>
            </ul>
            <p>In case any User fails to comply with these above mentioned guidelines CVideon will be forced to take immediate action against the User without prior warning. This will include deleting the violating Media content; exclude the User from the CVideon platform or proceed with further legal prosecution if necessary. CVideon cannot be held liable for any non-compliant behaviour of a User.</p>
        
            <h2>Illegal Activites </h2>
            <p>CVideon holds the right to report any sign of illegal activity performed on the CVideon platform by registered Users to the relevant law enforcement authorities. Illegal activities include but is not limited to:</p>
            <ul>
                <li>Appearance of non-tolerated subjects in Media uploaded to the CVideon platform as described in section 9 </li>
                <li>Activities that relate to vira and other computer codes with the purpose of harming or destroy IT equipment belonging to CVideon or the Users on the CVideon platform.</li>
                <li>Activities that relate to phishing or other attempt to acquire sensitive information from CVideon or the Users on the CVideon platform. </li>
                <li>Activities that motivates to illegal activities or distributes knowledge about it.</li>
                <li>Activities that relate or motivates to violent behaviour performed by individuals or groups of people.</li>
            </ul>
            <p>This Legal Notice shall be governed by Danish Law. Any dispute arising out of or in relation to this Legal Notice, which cannot be solved amicably shall be decided by the Danish Courts.</p>
            
            <h2>Change in Business Principles</h2>
            <p>CVideon maintains the right to change the business principles and current Terms and Conditions at any time, with prior notice to all registered Users of CVideon. A notice will be given at the time of the change where after CVideon will give 30 days for Users to get familiar with the changes. After 30 days CVideon will assume that all Users that are still registered will have accepted the changes and complied with them.</p>

            <h2>Final Remark </h2>
            <p>The ultimate purpose of the CVideon platform is to facilitate a connection between jobseekers and companies with both-ways video based communication. CVideon strives to create a positive and dynamic environment for all parties and therefore expects our Users to share this philosophy. A User should join CVideon to work with us in the direction we want it to evolve and not against us. Therefore, CVideon maintains the rights to exclusively evaluate if a Userʼs behaviour is constructive or deconstructive to the purpose of the CVideon platform. CVideon has the exclusive right to expel existing members if CVideon evaluates that their behaviour conflict with the scope of the CVideon platform. It is also possible for CVideon to permanently delete and block email addresses to register to the CVideon platform if CVideon or other Users have reported these emails or found them destructive to our scope.</p>
            <p>The CVideon Team hope, that your use of CVideon will be rewarding for you and your professional life.</p>
        </div>  
        <div class="col-md-1"></div>
    </div>

    <div class="spacer"></div>    <div class="half-spacer"></div>
@endsection

@section('footer')

        @include('layouts.footer')
        
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            $('a.login-window').click(function() {

                //Getting the variable's value from a link
                var loginBox = $(this).attr('href');

                //Fade in the Popup
                $(loginBox).fadeIn(300);

                //Set the center alignment padding + border see css style
                var popMargTop = ($(loginBox).height() + 24) / 2;
                var popMargLeft = ($(loginBox).width() + 24) / 2;

                $(loginBox).css({
                    'margin-top' : -popMargTop,
                    'margin-left' : -popMargLeft
                });

                // Add the mask to body
                $('body').append('<div id="mask"></div>');
                $('#mask').fadeIn(300);

                return false;
            });

            // When clicking on the button close or the mask layer the popup closed
            $('a.close-login, #mask').on('click', function() {
                $('#mask , .login-popup').fadeOut(300 , function() {
                    $('#mask').remove();
                });
                return false;
            });

        });
    </script>
@endsection