@extends('layouts.default')

@section('title', 'Welcome')

@section('content')
</div> <!-- <<< Closing Container little hack >>> -->
    <div id="home-bg-cover">
        <video class="hidden-xs hidden-sm" autoplay loop poster="/assets/images/video-frame.png" id="bgvid">
            <!--<source src="/assets/CVideoBg.webm" type="video/webm">-->
            <source src="/assets/CVideoBg.mp4" type="video/mp4">
        </video>

        <div id="home-top-content">

            @include('facade.menu')

            <div id="homeIntro" class="text-center">
                <div class="visible-xs">
                    <a href="{{ url('/') }}"><img id="mobile-logo" class="img-responsive col-xs-7 col-xs-push-2" src="/assets/images/CVideon-logo-white.png"></a>
                </div>
                <div class="clearfix spacer"></div>
                <div><h1 class="text-center">You. On display.</h1></div>
                <h3 class="text-center hidden-xs">Create your video CV. Promote yourself. <br />Stand out. Get hired.</h3>
                <div class="clearfix spacer"></div>
                <a href="{{ url('/register') }}" class="button front-page-button hidden-xs" style="font-size:25px;">Sign Up as a Job Seeker</a>
                <div class="clearfix spacer"></div>
                <img class="hidden-xs hidden-sm" src="/assets/images/Arrow.png" style="width:70px; height:auto;"/>
            </div>
        </div> 
    </div>

    @include('auth.login-panel')
    <div class="clearfix spacer hidden-xs"></div>
    <div class="spacer" id="home-description">
        <div class="col-md-1"></div>
        <div class="col-md-10"> 
            <h1 class="text-center">What is CVideon?</h1>
            <h3 class="text-center">The centralized video-sharing platform where users and companies can promote themselves through videos.</h3>
        </div>
        <div class="col-md-1"></div>
        <div class="clearfix spacer"></div>   
        <div class="spacer hidden-xs"></div>     
        <div class="col-md-4">
            <div class="icon-bg"><img src="/assets/images/icons/Icon_VideoPromotion.png"></div>
            <h3>Video CV Platform</h3>
            <p>Upload your very own video resume <br />and start promoting yourself visually. <br />Stand out from the crowd!</p>
        </div>
        <div class="col-md-4">
            <div class="icon-bg"><img src="/assets/images/icons/Icon_CompanyDatabase.png"></div>
            <h3>Company Database</h3>
            <p>Search companies and gain access <br />to all their video-based material and<br /> learn more about them.</p>
        </div>
        <div class="col-md-4">
            <div class="icon-bg"><img src="/assets/images/icons/Icon_Interaction.png"></div>
            <h3>Interaction</h3>
            <p>Interact with companies that show <br />interest in your profile, and get a step <br />closer to landing your dream job!</p>
        </div>
        <div class="clearfix"></div>
        <div class="spacer"></div><div class="spacer"></div>
    </div>
    <div class="spacer"></div> <div class="spacer"></div> 
    
    <div class="clearfix"></div>
    <div class="row spacer" id="home-sub-description">
        <div class="spacer"></div>    <div class="spacer"></div> 
        <div class="spacer"></div>
        <div class="col-md-1"></div>
        <div class="col-md-10"> 
            <h1 class="text-center">Why use CVideon?</h1>
            <h3 class="text-center">Because videos are the future of jobseeking and we provide <br />a platform to share them on.</h3>
        </div>
        <div class="col-md-1"></div>
        <div class="clearfix spacer"></div>   
        <div class="spacer hidden-xs"></div><div class="spacer hidden-xs"></div>
            <div id="boxes">

                <div class="col-md-3">
                    <div class="box">
                        <p>TELL MORE IN <br />LESS TIME</p>
                        <div class="overlay">
                            <div class="inBoxParagraph">
                                <p>Have you ever heard of the <br /> saying: <br />
                                “a minute of video is worth 1.8 <br /> million words?”</p>
                                <br />
                                <p>Well this is especially true for <br /> one of a well created video <br /> CV. </p>
                            </div>
                            <a class="close-overlay hidden">x</a>
                        </div>
                    </div>  
                </div>
                
                <div class="col-md-3">
                    <div class="box">
                            <p>SHOW YOUR <br />PERSONALITY</p>                
                        <div class="overlay"> 
                            <div class="inBoxParagraph">
                                <p>Statistics show that employers <br />are highly interested in your <br />personality and not just in your qualifications.</p>
                                <br />
                                <p>There is no better way to express <br />your individuality than on<br /> a video CV. </p>
                            </div>
                            <a class="close-overlay hidden">x</a>
                        </div>                  
                    </div>
                </div>
                
                <div class="col-md-3">                
                    <div class="box"> 
                        <p>EMPLOYERS WANT <br />TO SEE YOU</p>
                        <div class="overlay">
                            <div class="inBoxParagraph">
                                <p>Around 9 of 10 employers <br />believe that paper based CVs <br />do not show<br /> the full picture of the candidate.</p>
                                <br />
                                <p>Video CVs show your character <br /> and charm - which cannot <br /> be captured on paper</p>
                            </div>
                            <a class="close-overlay hidden">x</a>
                        </div>                  
                    </div>
                </div>
                
                <div class="col-md-3">                
                    <div class="box">
                        <p>BE VISIBLE <br />AND STAND OUT</p>
                        <div class="overlay">
                            <div class="inBoxParagraph">
                                <p>Distinguish yourself from <br />‘traditional’ job seekers that rely <br /> solely on the CV and cover letter combo.</p>
                                <br />
                                <p>Create your very own video CV, promote yourself and stand out <br /> from the crowd.</p>
                            </div>
                            <a class="close-overlay hidden">x</a>
                        </div>                  
                    </div>
                </div>
            </div>

        <div class="clearfix spacer"></div>
        <div class="spacer"></div>     
        <br /> 
        <div class="container text-center">
            <a href="{{ url('/register') }}" class="button front-page-button hidden-xs" style="font-size:25px; margin:0 auto;">Sign Up as a Job Seeker</a> 
        </div>
        <div class="clearfix spacer"></div>   
        <div class="half-spacer"></div> 
    </div>

    <div class="spacer" id="home-description">
        <br /><br />
        <div class="col-md-1"></div>
        <div class="col-md-10"> 
            <h1 class="text-center">Video CV consultancy</h1>
            <h3 class="text-center">You don’t have a video CV? No problem - we’re here to help. <br />If you are based in Copenhagen, we offer personal consultancy. <br />Otherwise, check our blog for more information.</h3>

            <div class="clearfix spacer"></div>
            <a class="button" style="font-size:22px;" ng-click="showTheOffers = true" ng-hide="showTheOffers">See Offers</a>
        </div>
        <div class="col-md-1"></div>
        <div class="clearfix spacer"></div>   
        <div class="spacer hidden-xs"></div>
        <div ng-show="showTheOffers">     
            <div class="col-md-3">
                <div class="packageBox">
                    <h3>BRONZE</h3>

                    <p class="priceTag text-center">
                        <span style="font-size:26px">995</span>
                        <span style="font-size:12px">DKK</span>
                    </p>

                    <div class="packageServices">
                        <ul>
                            <li>Receive our instructional e-book</li>
                            <li>1-hour consultancy and recording</li>
                            <li>Length: 30-60 seconds video CV</li>
                            <li>Pro camera, light and sound quality</li>
                            <li>Basic text effects and headlines</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="packageBox">
                    <h3>SILVER</h3>

                    <p class="priceTag text-center">
                        <span style="font-size:26px">1495</span>
                        <span style="font-size:12px">DKK</span>
                    </p>

                    <div class="packageServices">
                        <ul>
                            <li>Receive our instructional e-book</li>
                            <li>1-hour consultancy and recording</li>
                            <li>Length: 60-90 seconds video CV</li>
                            <li>Pro camera, light and sound quality</li>
                            <li>Recording in a location of your choice</li>
                            <li>Text, headlines and pictures</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="packageBox">
                    <h3>GOLD</h3>


                    <p class="priceTag text-center">
                        <span style="font-size:26px">1995</span>
                        <span style="font-size:12px">DKK</span>
                    </p>

                    <div class="packageServices">
                        <ul>
                            <li>Receive our instructional e-book</li>
                            <li>1 hour consultancy + 2 hours recording</li>
                            <li>Length: 60-120 seconds video CV</li>
                            <li>Pro camera, light and sound quality</li>
                            <li>Recording in 2 locations of your choice</li>
                            <li>Text, headlines and pictures</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="packageBox">
                    <h3>GROUP</h3>

                    <p class="priceTag text-center">
                        <span style="font-size:12px">FROM</span>
                        <span style="font-size:26px">4995</span>
                        <span style="font-size:12px">DKK</span>
                    </p>

                    <div class="packageServices">
                        <ul>
                            <li>More than 5 people together? Order our workshop including:</li>
                            <li>Elevator pitch training</li>
                            <li>Body language</li>
                            <li>Group exercises</li>
                            <li>Camera techniques</li>
                            <li>Bronze recording of each participant</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="clearfix spacer"></div><div class="spacer"></div>
            <br />
            <a target="_self" href="/contact" class="button front-page-button" style="font-size:22px; background-color: #5a1d4f;">
                Hear More
            </a>
            <br />
            <div class="spacer"></div><div class="spacer"></div>
            <div class="clearfix spacer"></div>
        </div>
        <br />
        <div class="spacer"></div>
    </div>
@endsection

@section('footer')

    @include('layouts.footer')

@endsection

@section('javascript')
    <script>
        $(document).ready(function(){
            if (Modernizr.touch) {
                // show the close overlay button
                $(".close-overlay").removeClass("hidden");
                // handle the adding of hover class when clicked
                $(".box").click(function(e){
                    if (!$(this).hasClass("hover")) {
                        $(this).addClass("hover");
                    }
                });
                // handle the closing of the overlay
                $(".close-overlay").click(function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    if ($(this).closest(".box").hasClass("hover")) {
                        $(this).closest(".box").removeClass("hover");
                    }
                });
            } else {
                // handle the mouseenter functionality
                $(".box").mouseenter(function(){
                    $(this).addClass("hover");

                })
                // handle the mouseleave functionality
                .mouseleave(function(){
                    $(this).removeClass("hover");
                });
            }
        });     

        $(document).ready(function() {
            $('a.login-window').click(function() {
                //Getting the variable's value from a link
                var loginBox = $(this).attr('href');

                //Fade in the Popup
                $(loginBox).fadeIn(300);

                //Set the center alignment padding + border see css style
                var popMargTop = ($(loginBox).height() + 24) / 2;
                var popMargLeft = ($(loginBox).width() + 24) / 2;

                $(loginBox).css({
                    'margin-top' : -popMargTop,
                    'margin-left' : -popMargLeft
                });

                // Add the mask to body
                $('body').append('<div id="mask"></div>');
                $('#mask').fadeIn(300);

                return false;
            });

            // When clicking on the button close or the mask layer the popup closed
            $('a.close-login, #mask').on('click', function() {
                $('#mask , .login-popup').fadeOut(300 , function() {
                    $('#mask').remove();
                });
                return false;
            });

        });

    </script>
@endsection