<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <base href="/">
  <title>{{ config('blog.title') }} Admin</title>

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
        rel="stylesheet">

  @yield('styles')

  <script src="/assets/js/admin.js"></script>

  <script type="text/javascript" src="{{ asset('assets/tinymce/js/tinymce/tinymce.min.js') }}"></script>

  <script src="/js/cvideon.js"></script>   

</head> 
<body ng-app="cvideon">

{{-- Navigation Bar --}}
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div style="width:1300px; margin:0 auto;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed"
                data-toggle="collapse" data-target="#navbar-menu">
          <span class="sr-only">Toggle Navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="text-center col-md-12">
        <a class="navbar-brand" href="/admin" target="_self">cVideon Dashboard</a>
        </div>
        <br />
        <div class="clearfix"></div>
      </div>
      <div class="collapse navbar-collapse" id="navbar-menu">
        @include('admin.partials.navbar')
      </div>
    </div>
  </div>
</nav>
    
<div style="width:1300px; margin:0 auto;">
  @yield('content')
</div>

  <script>
    tinymce.init({
      selector: '#contenttextarea',
      plugins: 'code, image, media',
      menubar: 'insert edit view format',
      toolbar: 'media, image, code, undo, redo, bold, italic, underline, blockquote, alignleft, aligncenter, alignright, alignjustify, bullist, numlist, link',
      image_caption: true
    });
  </script>

@yield('scripts')

</body>
</html>