<!DOCTYPE html>
<html ng-app="cvideon">
    <head>
        <meta charset="utf-8">
        <base href="/">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="{{ elixir("css/custom.css") }}">
        <script src="/js/app.js"></script>
        <script src="/js/cvideon.js"></script>        
        @yield('header')
        @include('facade.favicons')
        <title>CVideon - @yield('title')</title>
    </head>
    <body>
        <div class="menu">
            @yield('menu')
        </div>

        <div class="container ng-cloak">
            @yield('content')
        </div>

        <footer>
            @yield('footer')
        </footer>
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
              ga('create', 'UA-66711353-1', 'auto');
              ga('send', 'pageview');
            </script>
            
            @yield('javascript')
    </body>
</html>