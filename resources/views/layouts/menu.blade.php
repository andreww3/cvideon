<nav class="navbar navbar-default profile-navbar" controller="profileCtrl">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header ">
            <button type="button" class="navbar-toggle collapsed home-menu-top-margin" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand col-lg-3 col-md-3 col-sm-3"><a href="/" id="logo" target="_self"><img style="margin-top:3px" src="/assets/images/CVideon-logo.png"></a></div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            @if (Auth::check())
            <ul class="nav navbar-nav navbar-right profile-menu-top-margin">
                <!--<li><a href="#">My CVideon</a></li>-->
                <li><a ng-href="#" href="user/profile" target="_self">My Profile</a></li>
                <!--<li><a href="#">Companies</a></li>
                <li><a href="#">Jobs</a></li>-->
                <li><a href="auth/logout" target="_self">Logout</a></li>
            </ul>
            @else
            <ul class="nav navbar-nav navbar-right profile-menu-top-margin">
                <li><a href="{{ url('/register') }}">SIGN UP</a></li>
                <li><a href="#login-box" class="login-window">LOG IN</a></li>
            </ul>
            @endif
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

