    <div class="container">
        <div class="half-spacer"></div>
        <div class="col-md-2">
            <a href="."><img id="footer-logo" src="/assets/images/CVideon-logo-white.png"></a>
            <!--<p>CVideon is the online video platform that allows businesses and job seekers to connect and interact by means of video material.</p>-->
        </div>
        <div class="col-md-7">
            <ul>
                <li class="hidden-xs"><a href="/blog" target="_self">BLOG</a></li>
                <li><a href="/about">THE TEAM</a></li>
                <li><a href="/contact">CONTACT</a></li>
                <li><a href="/terms">TERMS & CONDITIONS</a></li>
            </ul>
        </div>
        <div class="visible-xs clearfix spacer"></div>
        <div class="col-md-3" style="margin-top:20px">
            <span>
                <a href="https://www.facebook.com/cvideonIVS/" target="_blank"><img src="/assets/images/sn-icons/facebook-icon.png"></a>
                <a href="https://twitter.com/wearecvideon" target="_blank"><img src="/assets/images/sn-icons/twitter-icon.png"></a>
                <a href="https://www.linkedin.com/company/cvideon" target="_blank"><img src="/assets/images/sn-icons/linkedin-icon.png"></a>
                <a href="https://www.instagram.com/wearecvideon/" target="_blank"><img src="/assets/images/sn-icons/instagram-icon.png"></a>
            </span>
        </div>
    </div>
    <div class="clearfix spacer"></div>