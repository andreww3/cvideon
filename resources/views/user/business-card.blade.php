@inject('countries', 'App\Country')
@inject('categories', 'App\Category')

@extends('layouts.default')

@section('title', 'Business Card')

@section('header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" type="text/css"/>
    
    <style>

	    h2 {
	    	margin:0px;
			font-size:12px; 
			color:black;
	    }

	    h3{
	    	margin:0px;
	    	font-size:11px; 
	    	color:black;
	    }

	    .business-card{
	    	margin:0 auto;
			position:relative; 
			margin-top:150px; 
			width:318px; 
			height:206px;
				box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.75);
				-webkit-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.75);
				-moz-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.75);
	    }

	    #qrCode{
	    	width:80px;
	    	height:80px;
	    }


    @media print {
	  	.business-card {
	  		border: 1px solid #bbb;
	    	margin:0 auto;
			position:relative; 
			margin-top:150px; 
			width:318px; 
			height:206px;
				box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.75);
				-webkit-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.75);
				-moz-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.75);
	  	}
	}

  </style>

@endsection

@section('content')

	<div ng-controller="bCardController">
		<!--<p>This is your business card, <b>{[ user.name ]}</b></p>-->
		
		<div class="clearfix spacer"></div>

		<div class="business-card">
			
			<div class="profile-cover" ng-style="{'background-color': '#C4D8E2' , 'width':'100%' , 'height':'115px'}">
				<img style="position:absolute; top:0; left:0; z-index:10; width:100%; height:115px;" ng-src="{[ user.cover_picture ]}">
			
				<div class="profile-picture">
			        <img style="position:absolute; top:23px; margin-left:-7px; z-index:100; width:85px; height:85px; margin-top:-10px;" ng-src="{[ user.profile_picture ]}">
			    </div>
		    </div> 

			<div>
				<div style="float:left; margin-left:35px; margin-top:20px;">
					<h2>{[ user.name ]}</h3>
					<h3>{[ user.userable.job_title ]}</h3>
					<h3 style="font-size:10px; margin-top:7px;">{[ user.email ]}</h3>
					<h3 style="font-size:9px;"><a>cvideon.com/user/{[ user.id ]}</a></h3>
				</div>
				
				<div style="float:right; margin-right:20px; margin-top:5px;">
					<img id="qrCode" src="https://chart.googleapis.com/chart?chs=70x70&cht=qr&chl=http%3A%2F%2Fcvideon.com/user/{[ user.id ]}%2F&choe=UTF-8" title="Link to My Profile" />
				</div>
				
				<br /><br clear="both"/>
			</div>		    

		</div>

		<div class="business-card">
			
			<div class="profile-cover" ng-style="{'background-color': '#C4D8E2' , 'width':'100%' , 'height':'115px'}">
				<img style="position:absolute; top:0; left:0; z-index:10; width:100%; height:115px;" ng-src="{[ user.cover_picture ]}">
			
				<div class="profile-picture">
			        <img style="position:absolute; top:23px; margin-left:-7px; z-index:100; width:85px; height:85px; margin-top:-10px;" ng-src="{[ user.profile_picture ]}">
			    </div>
		    </div> 

			<div>
				<div style="float:left; margin-left:35px; margin-top:20px;">
					<h2>{[ user.name ]}</h3>
					<h3>{[ user.userable.job_title ]}</h3>
					<h3 style="font-size:10px; margin-top:7px;">{[ user.email ]}</h3>
					<h3 style="font-size:9px;"><a>cvideon.com/user/{[ user.id ]}</a></h3>
				</div>
				
				<div style="float:right; margin-right:20px; margin-top:5px;">
					<img id="qrCode" src="https://chart.googleapis.com/chart?chs=70x70&cht=qr&chl=http%3A%2F%2Fcvideon.com/user/{[ user.id ]}%2F&choe=UTF-8" title="Link to My Profile" />
				</div>
				
				<br /><br clear="both"/>
			</div>		    

		</div>
	</div>

@endsection
