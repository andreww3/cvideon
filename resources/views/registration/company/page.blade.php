@inject('countries', 'App\Country')
@inject('categories', 'App\Category')

@extends('layouts.default')

@section('title', 'Job Seeker Registration')

@section('header')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" type="text/css"/>
@endsection

@section('menu') 

@include('registration.menu')

@endsection

@section('content')
<div ng-controller="regCtrl">

	<md-content class="md-padding">
		<md-tabs md-dynamic-height style="background:rgba(30, 60, 100, 1); height:auto;" md-selected="selectedIndex" md-border-bottom md-autoselect>

			<!-- tab 0 "Membership Types"-->
			@include('registration/company/step0')

			<!-- tab 1 "complete your profile info"-->
			@include('registration/company/step1')

			<!-- tab 2 "add your keywords and experience"-->
			@include('registration.company.step2')


			<!-- tab 3 "Upload or embed your cvideo" -->
			@include('registration/company/step3')

		</md-tabs>
	</md-content>
</div>

@endsection