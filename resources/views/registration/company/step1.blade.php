<md-tab ng-disabled="clickableTab1" label="Step 2 - Add Company Information">
	<div class="aTab tab{[1]}">
		<div class="col-md-2"></div> 
		<div class="col-md-8">
			<p class="categoryTitle">Main Category:</p>
			<p class="categoryText">
				<select id="JobCategories" class="form-control" name="category_id" ng-model="userChanges.category" 
				ng-options="category.name for category in allCategories"></select>
			</p>

			<div ng-show="userChanges.category">
		        <p class="categoryTitle">Subcategories:</p>
		        <p class="categoryText">
		        	<div	     
		            isteven-multi-select
		            placeholder="Select a category first"
		            input-model="userChanges.category.sub_categories"
		            output-model="userChanges.sub_categories"
		            button-label="icon name"
		            item-label="icon name maker"
		            tick-property="ticked">
		            </div>
		        </p>
	        </div>

			<p class="categoryTitle">Location:</p>
			<p class="categoryText">
				<select class="form-control" name="country_id" ng-model="userChanges.country_id" 
				ng-options="country.id as country.name for country in allCountries"></select>
			</p>

          	<p class="categoryTitle">Website:</p>
            <p class="categoryText"><input class="form-control" ng-model="userChanges.userable.company.website" type="text" name="website" ng-value="userChanges.userable.company.website"></p>
            
            <p class="categoryTitle">Number of Employees:</p>
            <p class="categoryText">
                <select class="form-control" name="number_of_employees" ng-model="userChanges.userable.company.number_of_employees">
                    <option value="1-10">1-10</option>
                    <option value="11-50">11-50</option>
                    <option value="51-200">51-200</option>
                    <option value="201-500">201-500</option>
                    <option value="501-1000">501-1000</option>
                    <option value="1001-5000">1001-5000</option>
                    <option value="5001-10000">5001-10000</option>
                    <option value="10000+">10000+</option>
                </select>
            </p>

			<p class="categoryTitle">Summary:</p>
			<p class="categoryText">
				<textarea name="summary" class="form-control" style="min-height:100px;" ng-model="userChanges.userable.company.description"
						placeholder="Write a short summary about the mission, vision, and brand culture of your company">
					{[ user.userable.company.description ]}
				</textarea><br />
				<br />
			</p>
			<input type="hidden" name="id" value="{[user.id]}" />
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			<div class="text-center">
				<!--<pre>{[someInputEmpty]}</pre>-->
				<button type="submit" class="button" ng-click="submitFirstStep(); selectedIndex = 2" ng-disabled="someInputEmpty">Next Step</button>
			</div>
			<div class="spacer"></div>
		</div> <!-- /col-md-8 -->
		<div class="col-md-2"></div>
	</div> <!-- /atab closing -->
</md-tab> <!-- /mtab closing "complete your profile info" -->