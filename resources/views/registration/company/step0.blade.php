<md-tab ng-disabled="clickableTab0" label="Step 1 - Select Membership Type">
	<div class="aTab tab{[0]}">

        <div class="col-md-12 company-memberships">
            <div class="col-md-4">
                <img src="/assets/images/memberships/Package_Free.png" />
            </div>

            <div class="col-md-4">
                <img src="/assets/images/memberships/Package_Basic.png" />
            </div>

            <div class="col-md-4">
                <img src="/assets/images/memberships/Package_Premium.png" />
            </div>
        </div>   
		<!-- button to step 3 -->
		<div class="col-md-12 text-center">
			<button type="submit" class="button" ng-click="clickableTab0 = 0; selectedIndex = 1">Next Step</button>
		</div>
		<!-- /button to step 3 -->
		<br/>
	</div> <!-- /atab closing -->
</md-tab> <!-- /mtab closing -->