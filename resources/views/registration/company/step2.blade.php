<md-tab ng-disabled="clickableTab2" label="Step 3 - Add Keywords">
	<div class="aTab tab{[2]}">
		<div class="keywordsTitle"> 
			<h3>KEYWORDS</h3>
			<p style="padding-left:10px;">-<i>Job seekers can search for your keywords so choose those that describes your company best</i></p>
		</div>
		<div class="clearfix"></div>
        <div class="spacer"></div>

        <ul class="profile-keywords">
            <div id="professionalKeywords">
                <li class="keyword" ng-repeat="keyword in user.tags | filter: { tag_type_id: '4' }" ng-mouseover="hoverIn()" ng-mouseleave="hoverOut()">
                    {[ keyword.name ]}
                    <a ng-if="ownsProfile" class="pull-right" style="margin-right:10px;" ng-click="deleteKeyword(keyword.id)" ng-show="hoverEdit">
                        <div class="glyphicon glyphicon-remove" style="color:grey;"></div>
                    </a>
                </li>
                <li ng-if="ownsProfile" class="keyword" style="padding:5px;">
                    <a id="addProfessionalKeyword" ng-hide="addCompanyKeyword" ng-click="addCompanyKeyword = true">+Add Keyword</a>
                        <div ng-show="addCompanyKeyword">
                            <input autofocus type="text" class="form-control pull-left keyword-input" placeholder="Add keyword..."
                            name="companyTag"
                            ng-model="newCompanyTagName"
                            uib-typeahead="obj as obj.text for obj in getSuggestions($viewValue, 'company_tag')"
                            typeahead-on-select="onSelect($item, $model, $label); newCompanyTagName = ''"
                            uib-tooltip="You can't add a duplicate keyword. Please add a new one."
                            tooltip-class="tooltip-error"
                            tooltip-placement="top"
                            tooltip-trigger="focus"
                            tooltip-enable="duplicateCompanyTag"
                            tooltip-is-open="duplicateCompanyTag || false">
                            <a href="#" class="pull-right" style="padding-top:7px; padding-right:3px; color:#5a1d4f;"
                            ng-click="addKeyword(newCompanyTagName, 4); newCompanyTagName = ''">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <br />
                </li>
            </div>
        </ul>

		<br clear="both"/>
		<br />
		<!-- button to step 3 -->
		<div class="col-md-12 text-center">
			<button type="submit" class="button" ng-click="clickableTab2 = 0; selectedIndex = 3">Next Step</button>
		</div>
		<!-- /button to step 3 -->
        <br clear="both"/>
        <br />
	</div> <!-- /atab closing -->
</md-tab> <!-- /mtab closing "add your keywords and experience" -->