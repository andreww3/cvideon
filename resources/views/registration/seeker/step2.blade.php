<!-- tab 2 "add your keywords and experience"-->
<md-tab ng-disabled="clickableTab2" label="Step 2 - Add your keywords and experience">
	<div class="aTab tab{[2]}">
		<div class="keywordsTitle">
			<h3>KEYWORDS</h3>
			<p style="padding-left:10px;" class="hidden-xs"><i>Companies can search for your keywords so choose those that describes you best</i></p>
		</div>
		<div class="clearfix"></div>
		<p>PROFESSIONAL <i>Choose 5 keywords that best describe your professional skills. </i></p>
		<ul class="profile-keywords">
			<div id="professionalKeywords" ng-show="!showingPersonalKeywords">
				<li class="keyword" style="background-color:white;" ng-repeat="keyword in user.tags | filter: { tag_type_id: '2' }" ng-mouseover="hoverIn()"
				ng-mouseleave="hoverOut()">
					{[ keyword.name ]}
					 <a class="pull-right" style="margin-right:10px;"  ng-click="deleteKeyword(keyword.id)" ng-show="hoverEdit">
						<div class="glyphicon glyphicon-remove"></div>
					</a>
				</li>

				<li class="keyword" class="keyword addKeywordBox" style="background-color:white;" ng-hide="addProfessionalKeyword">
					<a id="addProfessionalKeyword" style="" ng-click="addProfessionalKeyword = true">+Add Keyword</a>
				</li>
				
				<li class="keyword" ng-show="addProfessionalKeyword" style="background-color:white; padding:5px;">
					<input autofocus type="text" class="form-control pull-left keyword-input" placeholder="Add keyword..."
						name="professionalTag"
						ng-model="newProfTagName"
						uib-typeahead="obj as obj.text for obj in getSuggestions($viewValue, 'professional_tag')"
						typeahead-on-select="onSelect($item, $model, $label); newProfTagName = ''"
                        uib-tooltip="You can't add a duplicate keyword. Please add a new one."
                        tooltip-class="tooltip-error"
                        tooltip-placement="top"
                        tooltip-trigger="focus" 
                        tooltip-enable="duplicateProfessionalTag"
                        tooltip-is-open="duplicateProfessionalTag || false"
                        > 
					<a href="#" class="pull-right" style="padding-top:7px;" ng-click="addKeyword(newProfTagName, 2); newProfTagName = ''">
						<i class="fa fa-plus"></i>
					</a>
					<br />
				</li>
			</div>
		</ul>
		
		<div class="clearfix spacer"></div><br />

		<p>PERSONAL <i>Choose 5 keywords that best describe your personality</i></p>
		<ul class="profile-keywords">
			<div id="personalKeywords">
				<li class="keyword" style="background-color:white;" ng-repeat="keyword in user.tags | filter: { tag_type_id: '1' }" 
					ng-mouseover="hoverIn()" ng-mouseleave="hoverOut()">
					{[ keyword.name ]}
					<a class="pull-right" style="margin-right:10px;"  ng-click="deleteKeyword(keyword.id)" ng-show="hoverEdit"><div class="glyphicon glyphicon-remove"></div></a>
				</li>
				<li class="keyword addKeywordBox" style="background-color:white;" ng-hide="addPersonalKeyword">
					<a id="addProfessionalKeyword" ng-click="addPersonalKeyword = true">+Add Keyword</a>
				</li>
				<li class="keyword" ng-show="addPersonalKeyword" style="background-color:white; padding:5px;">
					<input autofocus type="text" name="newtag" class="form-control pull-left keyword-input" placeholder="Add keyword..."
					name="personalTag"
					ng-model="newPersTagName"
					uib-typeahead="obj as obj.text for obj in getSuggestions($viewValue, 'personal_tag')"
					typeahead-on-select="onSelect($item, $model, $label); newPersTagName = ''"
					uib-tooltip="You can't add a duplicate keyword. Please add a new one."
                    tooltip-class="tooltip-error"
                    tooltip-placement="top"
                    tooltip-trigger="focus"
                    tooltip-enable="duplicatePersonalTag"
                    tooltip-is-open="duplicatePersonalTag || false"
					>
					<a href="#" class="pull-right" style="padding-top:7px;"  ng-click="addKeyword(newPersTagName, 1); newPersTagName = ''"><i class="fa fa-plus"></i></a>
					<br />
				</li>
			</div>
		</ul>

		<div class="clearfix spacer"></div>
		<br clear="both"/>

		<div class="keywordsTitle">
			<h3>EXPERIENCE</h3>
			<p style="padding-left:10px;" class="hidden-xs"><i> Add your latest and most relevant experience to give companies a full picture of you</i></p>
		</div>

		<br clear="both"/>
		<div class="grey-section experience-box"><!-- experience box -->
			<div id="work-box" class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<h3>WORK</h3>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-12">
						<ul>
							<li class="workExperience" ng-repeat="experience in user.experiences | filter: { experience_type_id: '1' } | orderBy:'ended_at':true" ng-mouseover="hoverIn()" ng-mouseleave="hoverOut()">
								<div class="row">
									<div class="col-md-10 col-sm-10 col-xs-10">
										<p>{[experience.title]}</p>
										<p>{[experience.description]}</p>
										<p>
											<span>{[experience.started_at | amDateFormat: 'MMMM YYYY' ]}</span> -
											<span ng-if="experience.ended_at">{[experience.ended_at | amDateFormat:'MMMM YYYY' ]}</span>
											<span ng-if="!experience.ended_at">Present</span>
										</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2">
										<!-- icons of delete "X" and edit "pencil" -->
										<span ng-show="hoverEdit">
	            		        		    <a class="pull-right" ng-click="deleteExperience(experience.id)" ng-show="hoverEdit"><div class="glyphicon glyphicon-trash"></div></a><br />
	            		        		    <!--
	            		        		    <a class="pull-right a-link" ng-click="showEditExperience = true" ng-hide="showEditExperience"><div class="glyphicon glyphicon-pencil"></div></a>
	            		        		    -->
	            		        		</span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="editWorkExperienceForm" ng-show="showEditExperience">
											<label>Job Title</label>
											<input type="text" ng-model="experience.title" class="form-control" placeholder="The job title" required>
											<br /><br />
											<label>Company Name</label>
											<input type="text" ng-model="experience.description" class="form-control" placeholder="The name of the company" required>
											<br /><br />
											<label>Time Period</label>
											<br />
											<div class="selectTimeFrame">
												<!-- UI Bootstrap Datepicker -->
												<div class="col-md-6" style="padding-left:0px;">
													<label>Starting month</label>
                                        			<div class="input-group">
                                        			    <input type="text" class="form-control date-input" uib-datepicker-popup="MM/yyyy" ng-model="addStartingDate" is-open="status.startingDateOpened" min-mode="'month'" min-date="minDate" max-date="maxDate" show-weeks="false" datepicker-mode="'month'" ng-required="true" close-text="Close" />
                                        			    <span class="input-group-btn">
                                        			        <button type="button" class="btn btn-default" ng-click="startingDateOpen($event)"><i class="fa fa-calendar fa-lg"></i></button>
                                        			    </span>
                                        			</div> 													
													<!--
													<uib-datepicker ng-model="addStartingDate" min-date="minDate" show-weeks="false" class="uibdatepicker" datepicker-mode="'month'" min-mode="month">
													</uib-datepicker>
													-->
												</div>
												<div class="col-md-6">
                                        			<label>Ending month</label>
                                        			<div class="input-group">
                                        			    <input type="text" class="form-control" ng-disabled="isCurrentWork" uib-datepicker-popup="MM/yyyy" ng-model="addEndingDate" is-open="status.endingDateOpened"  min-mode="'month'" min-date="minDate" max-date="maxDate" show-weeks="false" datepicker-mode="'month'" ng-required="true" close-text="Close" />
                                        			    <span class="input-group-btn">
                                        			        <button type="button" class="btn btn-default" ng-disabled="isCurrentWork" ng-click="endingDateOpen($event)"><i class="fa fa-calendar fa-lg"></i></button>
                                        			    </span>
                                        			</div>
                                        			<div class="input-group">
                                        			    <label><input type="checkbox" ng-model="isCurrentWork">I currently work here</label>
                                        			</div>													
													<!--
													<uib-datepicker ng-model="addEndingDate" min-date="minDate" show-weeks="false" class="uibdatepicker" datepicker-mode="'month'" min-mode="month">
													</uib-datepicker>
													-->
												</div>
												<br />
			    		    			      <!--<p id="setToPresent" ng-show="upToPresent"> Present<br clear="both"/>
			    		    			      <div class="clearfix"></div>
			    		    			      <p style="font-size:15px;"><input style="width:15px; margin-top:7px; margin-right:10px;" type="checkbox" name="toPresent" id="presentCheckbox" ng-click="upToPresent = true">I currently work here</p><br /></p>-->
											</div>
											<br />
											<a class="button" style="padding-top:5px; padding-bottom:5px; font-size:15px;"
												ng-click="editExperience(experience.id, experience.title, experience.description, addStartingDate, addEndingDate); showEditExperience = false">
												Save
											</a>
											<a id="cancelWorkExperience" class="button" style="padding-top:5px; padding-bottom:5px; margin-left:10px; font-size:15px;"
												ng-click="showEditExperience = false">
												Close
											</a>
										</div> 										
									</div>
								</div>
								<br clear="both"/>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<a id="addWorkExperience" ng-click="showAddExperience = true" ng-hide="showAddExperience"> +Add Work Experience </a>
		        		<div id="addWorkExperienceForm" ng-show="showAddExperience">
		        			<label>Job Title</label>
		        			<input type="text" ng-model="experience.title" class="form-control" placeholder="The job title" required>
		        			<br />
		        			<label>Company Name</label>
		        			<input type="text" ng-model="experience.description" class="form-control" placeholder="The name of the company" required />
		        			<br />
		        			<div class="selectTimeFrame">
		        				<!-- UI Bootstrap Datepicker -->
		        				<div class="col-md-6" style="padding-left:0px;">
									<label>Starting month</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control date-input" uib-datepicker-popup="MM/yyyy" ng-model="addStartingDate" is-open="status.startingDateOpened" min-mode="'month'" min-date="minDate" max-date="maxDate" show-weeks="false" datepicker-mode="'month'" ng-required="true" close-text="Close" />
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default" ng-click="startingDateOpen($event)"><i class="fa fa-calendar fa-lg"></i></button>
                                        </span>
                                    </div>		        					
		        					<!--
		        					<uib-datepicker ng-model="addStartingDate" min-date="minDate" show-weeks="false" class="uibdatepicker" datepicker-mode="'month'" min-mode="month">
		        					</uib-datepicker>
		        					-->
		        				</div>
		        				<div class="col-md-6" style="padding-left:0px;">
                                	<label>Ending month</label>
                                	<div class="input-group">
                                	    <input type="text" class="form-control" ng-disabled="isCurrentWork" uib-datepicker-popup="MM/yyyy" ng-model="addEndingDate" is-open="status.endingDateOpened"  min-mode="'month'" min-date="minDate" max-date="maxDate" show-weeks="false" datepicker-mode="'month'" ng-required="true" close-text="Close" />
                                	    <span class="input-group-btn">
                                	        <button type="button" class="btn btn-default" ng-disabled="isCurrentWork" ng-click="endingDateOpen($event)"><i class="fa fa-calendar fa-lg"></i></button>
                                	    </span>
                                	</div>
                                	<div class="input-group" style="padding-left:0px;">
                                	    <input type="checkbox" ng-model="isCurrentWork">I currently work here
                                	</div>		        				
		        					<!--
		        					<uib-datepicker ng-model="addEndingDate" min-date="minDate" show-weeks="false" class="uibdatepicker" datepicker-mode="'month'" min-mode="month">
		        					</uib-datepicker>
		        					-->
		        				</div>
		        				<br />
		        				<!--<p id="setToPresent" ng-show="upToPresent"> Present<br clear="both"/>
		        				<div class="clearfix"></div>
		        				<p style="font-size:15px;"><input style="width:15px; margin-top:7px; margin-right:10px;" type="checkbox" name="toPresent" id="presentCheckbox" ng-click="upToPresent = true">I currently work here</p><br /></p>-->
		       				</div>
		       				<br />
		       				<a class="button" style="padding-top:5px; padding-bottom:5px; font-size:15px;" ng-click="addExperience(1, experience.title, experience.description, addStartingDate, addEndingDate); showAddExperience = false">Add Experience</a>
		       				<a id="cancelWorkExperience" class="button" style="padding-top:5px; padding-bottom:5px; margin-left:10px; font-size:15px;" ng-click="showAddExperience = false">Close</a>
		      			</div>
					</div>
				</div>
				<br />
			</div> <!-- /work box -->
			<div id="education-box" class="col-md-6"><!-- education box -->
    		  	<div class="row">
    		  		<div class="col-md-12">
    		  			<h3>EDUCATION</h3>
    		  		</div>
    		  	</div>
    		  	<br />
    		  	<div class="row">
    		  		<div class="col-md-12">
    		  			<ul>
    		  				<li class="educationExperience" ng-repeat="education in user.experiences | filter: { experience_type_id: '2' } | orderBy:'ended_at':true" ng-mouseover="hoverIn()" ng-mouseleave="hoverOut()">
    		  					<div class="row">
    		  						<div class="col-md-10 col-sm-10 col-xs-10">
    		  							<p>{[education.title]}</p>
    		  							<p>{[education.description]}</p>
    		  							<p>
											<span>{[experience.started_at | amDateFormat: 'MMMM YYYY' ]}</span> -
											<span ng-if="experience.ended_at">{[experience.ended_at | amDateFormat:'MMMM YYYY' ]}</span>
											<span ng-if="!experience.ended_at">Present</span>    		  							
    		  							</p>
    		  						</div>
    		  						<div class="col-md-2 col-sm-2 col-xs-2">
    		  							<span ng-show="hoverEdit">
    		                				<a class="pull-right" ng-click="deleteExperience(education.id)" ng-show="hoverEdit"><div class="glyphicon glyphicon-trash"></div></a><br />
    		                				<!--
    		                				<a class="pull-right a-link" ng-click="showEditEducation = true" ng-hide="showEditExperience"><div class="glyphicon glyphicon-pencil"></div></a>
    		                				-->
    		            				</span>
    		  						</div>
    		  					</div>
    		  					<div class="row">
    		  						<div class="col-md-12">
    		  							<div class="editEducationForm" ng-show="showEditEducation">
    		  								<label>Degree</label>
    		  								<input type="text" ng-model="education.title" class="form-control" placeholder="The job title" required>
    		  								<br /><br />
    		  								<label>Institute</label>
    		  								<input type="text" ng-model="education.description" class="form-control" placeholder="The name of the company" required>
    		  								<br /><br />
    		  								<label>Time Period</label>
    		  								<br />
    		  								
    		  								<div class="selectTimeFrame">
    		  									<!-- UI Bootstrap Datepicker -->
    		  									<div class="col-md-6" style="padding-left:0px;">
    		  										<uib-datepicker ng-model="addStartingDate" min-date="minDate" show-weeks="false" class="uibdatepicker" datepicker-mode="'month'" min-mode="month">
													</uib-datepicker>
    		  									</div>
    		  									<div class="col-md-6">
    		  										<uib-datepicker ng-model="addEndingDate" min-date="minDate" show-weeks="false" class="uibdatepicker" datepicker-mode="'month'" min-mode="month">
    		  										</uib-datepicker>
    		  									</div>
    		  								</div>
    		  								<br />
											<!--<p id="setToPresent" ng-show="upToPresent"> Present<br clear="both"/>
											<div class="clearfix"></div>
											<p style="font-size:15px;"><input style="width:15px; margin-top:7px; margin-right:10px;" type="checkbox" name="toPresent" id="presentCheckbox" ng-click="upToPresent = true">I currently work here</p><br /></p>-->
											<!--</div>-->
											<br />
											<a class="button" style="padding-top:5px; padding-bottom:5px; font-size:15px;" ng-click="editExperience(education.id, 2, education.title, education.description, addStartingDate, addEndingDate); showEditEducation = false">
												Save
											</a>
											<a id="cancelWorkEducation" class="button" style="padding-top:5px; padding-bottom:5px; margin-left:10px; font-size:15px;" ng-click="showEditEducation = false">
												Close
											</a>
										</div><!-- /edit education form -->    		  						
    		  						</div>
    		  					</div>
								<br clear="both"/> 
							</li>
						</ul>
					</div>
    		  	</div><!-- /row -->
    		  	<div class="row"><!-- row add education -->
    		  		<div class="col-md-12">
    		  			<a id="addEducation" ng-click="showAddEducation = true" ng-hide="showAddEducation">+ Add Education</a>
    		  			<br />
    		  			<div id="addEducationForm" ng-show="showAddEducation">
							<label>Degree</label>
							<input type="text" ng-model="education.title" class="form-control" placeholder="The job title" required />
							<br /><br />
							<label>Institute</label>
							<input type="text" ng-model="education.description" class="form-control" placeholder="The name of the company" required>
							<br /><br />
							<div class="selectTimeFrame">
								<!-- UI Bootstrap Datepicker -->
    		      				<div class="col-md-6" style="padding-left:0px;">
    		      					<label>Starting month</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control date-input" uib-datepicker-popup="MM/yyyy" ng-model="addStartingDate" is-open="status.startingDateOpened" min-mode="'month'" min-date="minDate" max-date="maxDate" show-weeks="false" datepicker-mode="'month'" ng-required="true" close-text="Close" />
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default" ng-click="startingDateOpen($event)"><i class="fa fa-calendar fa-lg"></i></button>
                                        </span>
                                    </div>	
    		      					<!--
    		      					<uib-datepicker ng-model="addStartingDate" min-date="minDate" show-weeks="false" class="uibdatepicker" datepicker-mode="'month'" min-mode="month">
    		      					</uib-datepicker>
    		      					-->
    		      				</div>
								<div class="col-md-6" style="padding-left:0px;">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<label>Ending month</label>
                                			<div class="input-group">
                                	    		<input type="text" class="form-control" ng-disabled="isCurrentWork" uib-datepicker-popup="MM/yyyy" ng-model="addEndingDate" is-open="status.endingDateOpened"  min-mode="'month'" min-date="minDate" max-date="maxDate" show-weeks="false" datepicker-mode="'month'" ng-required="true" close-text="Close" />
                                	    		<span class="input-group-btn">
                                	    		    <button type="button" class="btn btn-default" ng-disabled="isCurrentWork" ng-click="endingDateOpen($event)"><i class="fa fa-calendar fa-lg"></i></button>
                                	    		</span>
                                			</div>										
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
                                			<!--<div class="input-group">-->
                                			    <!--<label style="padding-left:0px; background-color:yellow; border: 1px solid black;"><input style="margin-left:0px;" type="checkbox" ng-model="isCurrentWork">I currently study here</label>-->
                                			<!--</div>-->
                                			<input style="margin-left:0px;" type="checkbox" ng-model="isCurrentWork">I currently study here
										</div>									
									</div>
									<!--
									<uib-datepicker ng-model="addEndingDate" min-date="minDate" show-weeks="false" class="uibdatepicker" datepicker-mode="'month'" min-mode="month">
									</uib-datepicker>
									-->
    		      				</div>
    		      				<br />
    		       				<!--<p id="setToPresent" ng-show="upToPresent"> Present<br clear="both"/>
    		       				<div class="clearfix"></div>
    		       				<p style="font-size:15px;"><input style="width:15px; margin-top:7px; margin-right:10px;" type="checkbox" name="toPresent" id="presentCheckbox" ng-click="upToPresent = true">I currently work here</p><br /></p>-->
    		     			</div>
    		     			<br />
    		     			<a class="button" style="padding-top:5px; padding-bottom:5px; margin-left:0px; font-size:15px;" ng-click="addExperience(2, education.title, education.description, addStartingDate, addEndingDate); showAddEducation = false">
    		     				Add Education
    		     			</a>
    		     			<!--<input type="submit" class="button" id="addExperienceButton" name="addWorkExperience" value="+Add Experience">-->
    		     			<a id="cancelWorkEducation" class="button" style="padding-top:5px; padding-bottom:5px; margin-left:10px; font-size:15px;" ng-click="showAddEducation = false">
    		     				Close
    		     			</a>
    		   			</div> <!-- /add education form -->
    		  		</div><!-- /col-md-12 -->
    		  	</div><!-- /row add education -->
			</div> <!-- /education box -->
    		<br clear="both"/>
		</div> <!-- /experience box -->
		<br clear="both"/>
		<br />
		<!-- button to step 3 -->
		<div class="col-md-12 text-center">
			<button type="submit" class="button" ng-click="clickableTab2 = 0; selectedIndex = 2">Next Step</button>
		</div>
		<!-- /button to step 3 -->
		<br/>
	</div> <!-- /atab closing -->
</md-tab> <!-- /mtab closing "add your keywords and experience" -->