<!-- tab 3 "Upload or embed your cvideo" -->
<md-tab ng-disabled="clickableTab3" label="Step 3 - Upload or embed a your CVideo">
	<div class="aTab tab{[3]}">
		<div class="col-md-2"></div> 
		<div class="col-md-8">
			<div class="text-center">
				<!--<h4 style="font-size:22px">The video presentation is the most important part of your profile.</h4>-->
        <p class="text-left"><strong>The video presentation is the most important part of your profile.</strong></p>
				<p class="text-justify">Please create a 60-90 seconds video where you present yourself, tell what you are good at and what kind of job you’re looking for. Let your personality shine through! Please see our Blog (link) for more instructions.</p>
				<p class="text-justify">If you are not ready for the full video, then just record yourself for 10-15 seconds stating your name and background and a “thank you for watching”</p>
				<br /><br />
			</div>
			<!-- cvideon -->
			<div class="cvideon" ng-if="user.videos.length > 0 && user.videos[0].key == videoType.path">
				<videogular vg-theme="config.theme">
					<vg-media vg-src="config.sources" vg-tracks="config.tracks"></vg-media>
       	  <vg-controls>
						<vg-play-pause-button></vg-play-pause-button>
						<vg-time-display>{[ currentTime | date:'mm:ss' ]}</vg-time-display>
						<vg-scrub-bar>
							<vg-scrub-bar-current-time></vg-scrub-bar-current-time>
       	  	</vg-scrub-bar>
       	  	<vg-time-display>{[ timeLeft | date:'mm:ss' ]}</vg-time-display>
       	  		<vg-volume>
       	  			<vg-mute-button></vg-mute-button>
       	  			<vg-volume-bar></vg-volume-bar>
       	  		</vg-volume>
       	  	<vg-fullscreen-button></vg-fullscreen-button>
       	  </vg-controls>
       	  <vg-overlay-play></vg-overlay-play>
       	  <vg-buffering></vg-buffering>
       	</videogular>
      </div>
      <div class="cvideon" ng-if="user.videos.length > 0 && user.videos[0].key == videoType.url">
		<iframe style="width:100%; height:100%; min-height:320px;" ng-src="{[ user.videos[0].value ]}" frameborder="0" allowfullscreen>
		</iframe>
      </div>
			<!-- /cvideon -->
			<div ng-if="user.videos.length" ng-show="hoverEdit">
       	<br />
       	<div class="col-md-12">
       		<table style="width:100%; text-align:center; margin-top:-7px;" ng-hide="showEmbedVideo">
       			<tr ng-hide="replaceVideo">
       				<td>
       					<a class="button" style="font-size:12px;" ng-click="deleteVideo( user.videos[0].id )">
       						Remove video
       					</a>
       				</td>
       				<td>
       					<a class="button uploadVideoBtn" style="font-size:12px;" ngf-select="upload($file, '/profile/addVideo')" 
       						ng-model="file" name="file" ngf-pattern="'video/*'"
       						accept="video/*" ngf-max-size="70MB">
       						Upload new video
       					</a>
       				</td>
       				<td>
       					<a class="button" style="font-size:12px;" ng-click="showEmbedVideo = true">Embed new video</a>
       				</td>
       			</tr> 
       		</table>  
       		<table style="width:100%; text-align:center; margin-top:-7px;" ng-show="showEmbedVideo">
       			<tr ng-hide="replaceVideo">
       				<td style="width:70%;">
       					<input type="text" ng-model="userChanges.newEmbededVideo" class="form-control pull-left"
       						name="embedLink" style="width:100%;" placeholder="Youtube or Vimeo link">
       				</td>
       				<td>
       					<button ng-click="embedNewVideo()" class="form-control button pull-right"
       						style="width:100%; margin-top:10px; padding-top:8px;">
       						Embed
       					</button>
       				</td>
       			</tr> 
       		</table> 
      	</div>
      	<div class="col-md-1"></div>
    	</div>

			<div class="cvideon container-right-margin" style="min-height:310px;" ng-if="!user.videos.length">
				<div class="col-md-1"></div>
    		<div class="col-md-10">
    			<div ng-hide="showEmbedVideo">
            <div class="uploadVideoTable" ng-hide="loadingVideoUpload" ng-if="ownsProfile">
                <div class="col-md-12">

                  <div class="col-md-6 video-form-options">
                    <a class="button" ngf-select="loadingVideoUpload = true; upload($file, '/profile/addVideo')" ng-model="file" name="file" ngf-pattern="'video/*'" accept="video/*" ngf-max-size="100MB">Upload Video</a>
                  </div>

                  <div class="col-md-6 video-form-options">
                      <a class="button" ng-click="openEmbedVideo()">Embed Video</a>
                  </div>

                  <div class="clearfix spacer"></div>
                    <p><i>* Upload your CVideo of maximum 90 seconds or 70 mb. <br /> or <br />* Embed one of your videos from Youtube or Vimeo</i></p>
                </div>
            </div>
    			</div> <!-- /show embed video -->
    			<table style="width:100%; text-align:center; margin-top:120px; font-family:'Raleway-Regular';" ng-show="loadingVideoUpload">
    				<tr><td><img src="assets/images/loader.gif"></td></tr>
    				<tr><td><p style="padding-top:10px; color:#5a1d4f;">Please wait while your video is uploading...</p><td></tr> 
    			</table>
    			<div class="embedForm" ng-show="showEmbedVideo">
    				<input type="text" ng-model="userChanges.newEmbededVideo" class="form-control pull-left" 
    					name="embedLink" style="width:70%;"  placeholder="Youtube or Vimeo link">
    				<button ng-click="embedVideo()" class="form-control button pull-right" style="width:29%; padding-top: 8px;">Embed</button>
    			</div>
    		</div> <!-- /col-md-10 -->
    		<div class="col-md-2"></div>
    		<br clear="both"/>
  		</div> <!-- /cvideon container -->
        <br clear="both"/>
        <br />
      <div class="text-center">
          <button type="submit" class="button" ng-click="goToUserProfile(user.id)">Go To Your profile</button>            
      </div>
  	</div> <!-- /col-md-8 -->
  </div> <!-- /a tab -->	
</md-tab> <!-- /tab 3 "Upload or embed your cvideo" -->