<!-- tab 1 "complete your profile info"-->
<md-tab ng-disabled="clickableTab1" label="Step 1 - Complete your profile info">
	<div class="aTab tab{[1]}">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<p class="categoryTitle">Your specialization</p>
			<p class="categoryText">
				<input class="form-control" ng-model="userChanges.userable.job_title" type="text" name="status_quo" 
				ng-value="userChanges.userable.job_title" placeholder="Your specialization">
			</p>
			<p class="categoryTitle">Actual Situation:</p>
			<p class="categoryText">
				<input class="form-control" ng-model="userChanges.userable.status_quo" type="text" name="status_quo" 
				ng-value="userChanges.userable.status_quo" placeholder="Your job status">
			</p>
			<p class="categoryTitle">Main Category:</p>
			<p class="categoryText">
				<select id="JobCategories" class="form-control" name="category_id" ng-model="userChanges.category" 
				ng-options="category.name for category in allCategories"></select>
			</p>
			<p class="categoryTitle">Subcategories:</p>
			<p class="categoryText">
				<div     
					isteven-multi-select
					input-model="userChanges.category.sub_categories"
					output-model="userChanges.sub_categories"
					button-label="icon name"
					item-label="icon name maker"
					tick-property="ticked">
				</div>
			</p>
			<p class="categoryTitle">Location:</p>
			<p class="categoryText">
				<select class="form-control" name="country_id" ng-model="userChanges.country_id" 
				ng-options="country.id as country.name for country in allCountries"></select>
			</p>
			<p class="categoryTitle">Languages:</p>
			<p class="categoryText">
				<div     
					isteven-multi-select
					input-model="languages"
					output-model="selectedLanguages"
					button-label="icon name"
					item-label="icon name maker"
					tick-property="ticked"
				>
				</div>
			</p>
			<p class="categoryTitle">Goal:</p>
			<p class="categoryText">
				<textarea name="goal" class="form-control" style="min-height:100px;"ng-model="userChanges.userable.goal" 
				placeholder="Write the goal of your jobseeking, your dream job or why you want to present yourself to companies">
				</textarea>
				<br />
			</p>
			<input type="hidden" name="id" value="{[user.id]}" />
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			<div class="text-center">
				<!--<pre>{[someInputEmpty]}</pre>-->
				<button type="submit" class="button" ng-click="submitFirstStep(); selectedIndex=1" ng-disabled="someInputEmpty">Next Step</button>
			</div>
			<div class="spacer"></div>
		</div> <!-- /col-md-8 -->
		<div class="col-md-2"></div>
	</div> <!-- /atab closing -->
</md-tab> <!-- /mtab closing "complete your profile info" -->