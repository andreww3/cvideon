@inject('countries', 'App\Country')
@inject('categories', 'App\Category')

@extends('layouts.default')

@section('title', 'Messages')

@section('header')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" type="text/css"/>

@endsection
<div ng-controller="messagingCtrl">
@section('menu')

    @include('profile.menu')

@endsection

@section('content')
<hr/>

<div class="col-md-12">
    <!--<h3 class="filters-title text-center" style="width:103%; margin-left:-15px;">
    Your messages
    </h3>-->
        <div class="col-md-4">
            <a ng-click="openInbox()">
                <h3 class="filters-title" style="width:116%">Inbox 
                    <span ng-show="unreadMessages != 0">
                      ({[unreadMessages]} )
                    </span></h3></a>
                
                <div ng-if="showInboxMessages == true" 
                    class="messageList">
                    <ul>
                        <li ng-if="noReceivedMessages" 
                            style="cursor:default; opacity:0.9;">
                            <i><b>Empty: </b>No messages here for now now</i>    
                        </li>

                        <li ng-repeat="message in received"
                            ng-click="selectMessage(message)"
                            ng-class="{ 'unreadMessage': message.read == 0, 
                                        'readMessage': message.read == 1 }">

                            From: <i><b>{[ message.sender.name ]}</b></i> <br />
                            Subject: <b>{[ message.subject ]}</b> </li>
                            
                        <div class="clearfix"></div>
                    </ul>
                </div>

            <a ng-click="openSent()">
                <h3 class="filters-title" style="width:116%">Sent</h3></a>
                
                <div ng-if="showSentMessages == true"
                     class="messageList">
                    <ul>
                        <li ng-if="noSentMessages"
                            style="cursor:default; opacity:0.9;">
                            <i><b>Empty: </b>No messages here for now now</i>    
                        </li>
                        <li ng-repeat="message in sent" 
                            ng-click="selectMessage(message)"
                            ng-class="{ 'unreadMessage': message.read == 0, 
                                        'readMessage': message.read == 1 }">

                            To: <i><b>{[ message.receiver.name ]}</b></i><br />
                            Subject: <b>{[ message.subject ]}</b> </li>

                        <div class="clearfix"></div>
                    </ul>
                </div>
        </div>

        <div class="col-md-8">

            <!-- | If there are no messages | -->
            <div class="message-result" ng-if="noReceivedMessages">

                <div class="message-title">
                    <p style="font-size:24px;">Empty for now</p>
                    <p>You don't have any messages in your inbox right now.</p>
                </div>

                <div class="message-footer">
                    <p> <a href="search/companies" target="_self">
                        Find and contact a company here</a>! </p>
                    
                    <div class="clearfix"></div>
                </div>
            </div>

            <!-- | Reply box | -->
            <div class="message-result" ng-if="replying">
                <div ng-if="messageSent" class="alert alert-success">
                    <strong>Success!</strong> Your reply has been sent.
                </div>
                
                <div class="message-title">     
                    <p style="float:left; font-size:24px;">Replying to: <b>
                    {[ messageInfo.receiver.name ]}<span ng-if="messageInfo.receiver.company">
                    | {[ messageInfo.receiver.company ]}
                    </span></b>
                    </p>

                    <p style="float:right; font-size:18px;">
                     
                        <a class="btn options-button btn-block" 
                            href="" ng-click="closeReply()"> 
                            Delete </a> 

                    </p>
                    <div class="clearfix"></div>
                </div>

                <div class="message-content">
                    <p><b>Subject:</b><br/>
                        <input type="text" class="form-control" 
                        style="font-size:20px;" ng-model="messageInfo.subject" ng-value="messageInfo.subject" placeholder="You can change the subject?">
                    </p>

                    <p><textarea class="form-control" style="min-height:170px;" ng-model="messageInfo.message">
                    </textarea></p>

                    <p style="float:left;">From:<br/> 
                       <b> {[ messageInfo.sender.name ]}
                    
                        <span ng-if="messageInfo.sender.company">
                            | {[ messageInfo.sender.company ]}
                        </span>
                        </b>
                    </p>

                    <button class="btn search-button btn-block"       
                    href="" ng-click="sendReply()"> Send Reply</button>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="message-footer">

                </div>
            </div>

            <!-- | Show a selected message | -->       
            <div class="message-result" ng-if="selectedMessage">
                <div class="message-title"> 

                    <p style="float:left; font-size:24px;">
                    {[ selectedMessage.subject ]}</p>

                    <p style="float:right; font-size:18px;">
                        <a class="btn options-button" href="" 
                            style="float:left;"
                            ng-hide="replying"
                            ng-click="reply(selectedMessage.id)"> Reply </a> 
                        
                        <a ng-if="selectedMessage.read == 0"
                            class="btn options-button" href="" 
                            style="float:left; margin-left:10px"
                            ng-click="markAsRead(selectedMessage.id)"> Mark as read </a> 

                        <a ng-if="selectedMessage.read == 1"
                            class="btn options-button" href="" 
                            style="float:left; margin-left:10px"
                            ng-click="delete(selectedMessage.id, loggedInUser)"> Delete </a> 
                    </p>
                    <div class="clearfix"></div>
                </div>

                <div class="message-content">
                    
                    <p>"<i>{[ selectedMessage.message ]}</i> "</p>
                    
                </div>

                <div class="message-footer">
                    <p style="float:left;">From: <b>{[ selectedMessage.sender.name ]}
                    <span ng-if="message.sender.company">
                    , {[ selectedMessage.sender.company ]}
                    </span></b>
                    </p>
                    <p style="float:right;" class="text-right"><i>
                        Sent on {[ selectedMessage.created_at | date:'fullDate' ]}
                    </i></p>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    <div class="clearfix spacer"></div>
</div>
</div> <!-- Closing Controller -->

@endsection


@section('javascript')
    <script>
        $(document).ready(function() {


        });

    </script>
@endsection