<div class="modal-footer" ng-show="messageSent">
    <div class="alert alert-success">
      <strong>Success!</strong> Your message has been sent.
    </div>
</div>

<div class="modal-footer" ng-hide="messageSent">
    <button type="button" class="button btn-default" ng-click="openContact(selectedResult)" ng-hide="sendMessageToUser">Contact me</button>   
    <div class="contactUserPannel" ng-show="sendMessageToUser">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><i class="fa fa-times"></i></span>
    </button>
        <h3 ng-if="selectedResult.userable.company.name">Contact {[ selectedResult.userable.company.name ]}</h3>
        <h3 ng-if="selectedResult.userable.company.name == false">Contact {[ selectedResult.name ]}</h3>
        <br />                               
        <form class="contactUserForm">
            <h4>Subject</h4>
                <input class="form-control" type="text" ng-model="message.subject">
            <br />
            <h4>Message</h4>
                <textarea class="form-control" ng-model="message.message"></textarea>
            <br />
            <div class="form-group">
                <h4>From:</h4>
                <input class="form-control" type="text" ng-model="message.from.name" ng-value="user.name" disabled>
                <input class="form-control" type="text" ng-if="message.from.company" ng-model="message.from.company" ng-value="user.userable.company.name" disabled>
                <input class="form-control" type="text" ng-model="message.from.email" ng-value="user.email">

                <div class="spacer"></div>
            </div>
            
            <input class="form-control btn search-button" value="Send Message" ng-click="contactUser()"> 
            <div class="clearfix spacer"></div>
        </form>
    </div>
</div>