<div id="messaging-box" class="messaging-popup">
    <a href="" class="close-messaging"><i class="fa fa-close"></i></a>
    <div class="modal-header">
	<h3>Create message...</h3>
    </div>
    <div class="modal-body">
	    <form class="contactUserForm">                              
	        <div class="form-group">
	            <h4>From:</h4>
	            <input class="form-control" type="text" ng-value="message.sender.name" disabled>
	            <input class="form-control" type="text" ng-if="message.sender.company" ng-value="message.sender.company" disabled>
	            <input class="form-control" type="text" ng-value="message.sender.email">

	            <div class="spacer"></div>
	        </div>

	        <h4>Subject</h4>
	            <input class="form-control" type="text" ng-model="message.subject">
	        <br />

	        <h4>Message</h4>
	            <textarea class="form-control" ng-model="message.message"></textarea>
	        <br />

	        
	        <a class="btn search-button close-messaging" style="opacity:0.8;" ng-click="contactUser()">
	        	Send Message
	        </a> 
	        <div class="clearfix spacer"></div>
	    </form>
	</div>    
</div>