
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header ">
                <button type="button" class="navbar-toggle collapsed home-menu-top-margin" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand col-lg-3 col-md-3 col-sm-3 hidden-xs"><a href="{{ url('/') }}"><img class="img-responsive header-logo-image" src="/assets/images/CVideon-logo-white.png"></a></div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right home-menu-top-margin">
                    <li><a href="/blog" target="_self">BLOG</a></li>
                    @if (Auth::check())
                        
                        @if (Auth::user()->hasRole('edit_article'))
                        <li><a href="admin" target="_self">ADMIN PANEL</a></li>
                        @endif
                        <li><a href="user/profile" target="_self">MY PROFILE</a></li>
                        <li><a href="/auth/logout" target="_self">LOGOUT</a></li>
                    @else
                    <li><a href="{{ url('/register') }}">SIGN UP</a></li>
                    <li><a href="#login-box" class="login-window">LOG IN</a></li> 
                    @endif
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav> 
