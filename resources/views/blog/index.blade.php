@extends('layouts.default')

@section('title', 'Blog')

@section('content')
</div> <!-- <<< Closing Container little hack >>> -->

    <div id="home-bg-cover" style="height:340px; background-image:url('/assets/images/About.png'); background-size:cover;">        
        
        @include('blog.header')

        <div id="home-top-content">
            <div class="text-center">
            <div class="visible-xs">
                <img class="img-responsive col-xs-6 col-xs-push-3" src="/assets/images/CVideon-logo-white.png">
            </div>

            <div class="clearfix spacer"></div>   
            <div class="spacer"></div> 

            <div class="hidden-xs"><a href="/blog" target="_self"><h1>{{ config('blog.title') }}</h1></a></div>
                <h3 class="hidden-xs">{{ config('blog.description') }}</h3>
            <div class="clearfix spacer"></div>
            </div>

        </div>
    </div>

    @include('auth.login-panel')

    <div class="container spacer" ng-controller="blogCtrl">
        <div class="col-md-12"> 
            <div class="col-md-12 text-center">
                <h4> 
                    <a href ng-click="get6Posts()" style="padding:10px;">Latest</a> |
                @foreach ($categories as $category)
                    <a href ng-click="selectCategory({{$category->id}})" style="padding:10px;">{{ $category->name }}</a> |
                @endforeach </i>
                    <a href ng-click="getAllPosts()" style="padding:10px;">All</a>               
                </h4>
                <hr>
            </div>
            <div class="clearfix spacer"></div>
            <!--<h1 class="text-center" style="font-size:40px; color:#1e3c64">Articles</h1> 
            <h4 class="text-center" style="color:#337ab7">
                <a href="#">Category</a>  |
            </h4>
            <div class="clearfix spacer"></div>-->
            <div class="article-list">
                <div ng-repeat="post in posts" class="col-md-6 article-box">
                    <h3><a target="_self" ng-href="blog/{[ post.slug ]}">
                        {[ post.title ]}</a></h3>
                    
                    <em>{[ post.published_at ]}</em>
                    <p ng-bind-html="(post.content_html | limitTo:300) + '...'"></p>
                </div>                
            </div>
            <div class="clearfix"></div>

            <hr>
        </div>

        <div class="clearfix"></div>
        <div class="spacer"></div>
    </div>
    <div class="spacer"></div> <div class="spacer"></div> 
    </div> <!-- <<< Closing Container little hack >>> -->
    <div class="clearfix"></div>
    <div class="row spacer" id="home-sub-description">
        <div class="spacer"></div> 

        <div class="container">
        <div class="spacer"></div>    
        <div class="spacer"></div>
            <div class="col-md-12"> 
                <h1 class="text-center" style="font-size:50px;">Step by Step cVideo Tutorial</h1>
                <h3 class="text-center" style="color:white;">Videos are the future of jobseeking. Therefore we will help you create one and upload it on CVideon to attach it in your job application.</h3>
            
                <div class="clearfix spacer"></div>   
                <div class="spacer"></div><div class="spacer"></div>
                    <div id="boxes">

                        <div class="col-md-4">
                            <div class="box">
                                <p>Step 1 <br />
                                Your Profile</p>
                                <div class="overlay">
                                    <div class="inBoxParagraph">
                                        <p>Have you ever heard of the <br /> saying: <br />
                                        “a minute of video is worth 1.8 <br /> million words?”</p>
                                        <br />
                                        <p>Well this is especially true for <br /> one of a well created video <br /> CV. </p>
                                    </div>
                                    <a class="close-overlay hidden">x</a>
                                </div>
                            </div>  
                        </div>

                        <div class="col-md-4">
                            <div class="box">
                                <p>Step 2 <br />
                                Video CV</p>
                                <div class="overlay"> 
                                    <div class="inBoxParagraph">
                                        <p>Statistics show that employers <br />are highly interested in your <br />personality and not just in your qualifications.</p>
                                        <br />
                                        <p>There is no better way to express <br />your individuality than on<br /> a video CV. </p>
                                    </div>
                                    <a class="close-overlay hidden">x</a>
                                </div>                  
                            </div>
                        </div>

                        <div class="col-md-4">                        
                            <div class="box"> 
                                <p>Step 3 <br />
                                Job Search</p>
                                <div class="overlay">
                                    <div class="inBoxParagraph">
                                        <p>Around 9 of 10 employers <br />believe that paper based CVs <br />do not show<br /> the full picture of the candidate.</p>
                                        <br />
                                        <p>Video CVs show your character <br /> and charm - which cannot <br /> be captured on paper</p>
                                    </div>
                                    <a class="close-overlay hidden">x</a>
                                </div>                  
                            </div>
                        </div>
                        
                    </div>
                    <div class="clearfix spacer"></div>

                <div class="spacer"></div> <div class="spacer"></div> 

                <div class="half-spacer"></div> 
                <div class="text-center">
                    <a href="{{ url('/register') }}" class="button front-page-button hidden-xs" style="font-size:25px; margin:0 auto;">Join our platform for free</a> 
                </div>

                <div class="spacer"></div><div class="clearfix spacer"></div>   
            </div>
        </div> 
           

@endsection

@section('footer')
    
    @include('layouts.footer')

@endsection

@section('javascript')
    <script>
        $(document).ready(function(){
            if (Modernizr.touch) {
                // show the close overlay button
                $(".close-overlay").removeClass("hidden");
                // handle the adding of hover class when clicked
                $(".box").click(function(e){
                    if (!$(this).hasClass("hover")) {
                        $(this).addClass("hover");
                    }
                });
                // handle the closing of the overlay
                $(".close-overlay").click(function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    if ($(this).closest(".box").hasClass("hover")) {
                        $(this).closest(".box").removeClass("hover");
                    }
                });
            } else {
                // handle the mouseenter functionality
                $(".box").mouseenter(function(){
                    $(this).addClass("hover");

                })
                // handle the mouseleave functionality
                .mouseleave(function(){
                    $(this).removeClass("hover");
                });
            }
        });     

        $(document).ready(function() {
            $('a.login-window').click(function() {
                //Getting the variable's value from a link
                var loginBox = $(this).attr('href');

                //Fade in the Popup
                $(loginBox).fadeIn(300);

                //Set the center alignment padding + border see css style
                var popMargTop = ($(loginBox).height() + 24) / 2;
                var popMargLeft = ($(loginBox).width() + 24) / 2;

                $(loginBox).css({
                    'margin-top' : -popMargTop,
                    'margin-left' : -popMargLeft
                });

                // Add the mask to body
                $('body').append('<div id="mask"></div>');
                $('#mask').fadeIn(300);

                return false;
            });

            // When clicking on the button close or the mask layer the popup closed
            $('a.close-login, #mask').on('click', function() {
                $('#mask , .login-popup').fadeOut(300 , function() {
                    $('#mask').remove();
                });
                return false;
            });

        });

    </script>
@endsection