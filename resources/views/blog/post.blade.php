@extends('layouts.default')

@section('title', 'Blog')

@section('content')
</div> <!-- <<< Closing Container little hack >>> -->
    <div id="home-bg-cover" style="height:120px; background-image:url('/assets/images/About.png'); background-size:cover;">

        @include('blog.header')

    </div>
        
    @include('auth.login-panel')

    <div class="spacer"></div>
    <div class="container">
        <div class="col-md-8"> 
            <h1 style="font-size:30px; color:#1e3c64">{{ $post->title }}</h1>
            <hr>
                <p>{!! html_entity_decode($post->content_raw) !!}</p>
            <hr>
            <br />
            <h5>{{ $post->published_at->format('M jS Y g:ia') }}</h5>
            <br />
            <p>Tags:           
              @foreach ($post->tags as $tag)
                  {{ $tag->name }}
              @endforeach
            </p>
            <div class="clearfix spacer"></div>

            <button class="btn btn-primary" onclick="history.go(-1)">
              « Back
            </button>            
        </div>
        <div class="col-md-4">
            
            <div class="sidebarBox">
                <h3>Latest Articles</h3>

                <ul>
                  @foreach ($posts as $post)
                      <li><a href="blog/{{ $post->slug }}" target="_self">{{ $post->title }}</a></li>
                  @endforeach
                </ul><hr>
            </div>

            <div class="sidebarBox">
                <h3>Categories</h3>
                
                <ul>
                  @foreach ($categories as $category)
                      <li>{{ $category->name }}</li>
                  @endforeach
                </ul><hr>
                
            </div>
            <div class="clearfix"></div>
        </div>
        
        <div class="spacer"></div><div class="spacer"></div>
    </div>

    <div class="spacer"></div> <div class="spacer"></div> 

    <div class="clearfix"></div>

@endsection

@section('footer')
    
    @include('layouts.footer')

@endsection

@section('javascript')

@endsection