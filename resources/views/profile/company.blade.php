@inject('countries', 'App\Country')
@inject('categories', 'App\Category')

@extends('layouts.default')

@section('title', 'Profile Page')

@section('header')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" type="text/css"/>

@endsection

<div ng-controller="profileCtrl">
@section('menu')

    @include('profile.menu')

@endsection

@section('content')

<div ng-show="user_loaded">
    <!-- Welcome message for new users -->
    <div class="alert alert-success alert-dismissible" role="alert" id="welcome-alert" ng-show="showWelcome">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>{[welcomeMsgNewUser]}</strong>
    </div>
    <!-- ./welcome message-->

    <div class="profile-cover" ng-if="user.cover_picture" ng-style="{'background-image': 'url( {[user.cover_picture]} )'}" ng-mouseover="hoverIn()" ng-mouseleave="hoverOut()">
        <div class="profile-picture">
            <div ng-hide="uploadPictureForm" ng-mouseover="hoverIn()" ng-mouseleave="hoverOut()">
                <img ng-src="{[ user.profile_picture ]}">
                <a ng-if="ownsProfile" id="changePp" class="button animate-show" data-toggle="modal" data-target="#picture-upload-modal" ng-show="hoverEdit">Change profile picture</a>
            </div>
        </div>
        <a ng-if="ownsProfile" id="addProfileCover" class="button" data-toggle="modal" data-target="#cover-upload-modal" ng-show="hoverEdit">Add Profile Cover</a>
    </div>

    <div class="profile-cover" ng-if="!user.cover_picture" ng-style="{'background-color': '#C4D8E2'}" ng-mouseover="hoverIn()" ng-mouseleave="hoverOut()">
        <div class="profile-picture">
            <div ng-hide="uploadPictureForm">
                <img ng-src="{[ user.profile_picture ]}">
                <a ng-if="ownsProfile" id="changePp" class="button animate-show" data-toggle="modal" data-target="#picture-upload-modal" ng-show="hoverEdit">Change profile picture</a>
            </div>
        </div>
        <a ng-if="ownsProfile" id="addProfileCover" class="button"  data-toggle="modal" data-target="#cover-upload-modal"  ng-show="hoverEdit">Add Profile Cover</a>
    </div>
    <!-- Profile Picture Cropping Dialog -->
    <div ng-if="ownsProfile" id="picture-upload-modal" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times"></i></span>
                    </button>
                    <h4 class="modal-title">Adjust profile picture</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-7">
                        <input type="file" img-cropper-fileread image="ppCropper.sourceImage" />
                        <div>
                             <canvas width="300" height="300" id="canvas" image-cropper image="ppCropper.sourceImage" cropped-image="ppCropper.croppedImage" crop-width="220" crop-height="220" keep-aspect="true" touch-radius="30" crop-area-bounds="bounds"></canvas>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <p>Picture preview:</p>
                        <div ng-show="ppCropper.croppedImage!=null"><img ng-src="{[ppCropper.croppedImage]}" /></div>
                    </div>
                        <br clear="both"/>
                    <form method="post" action="profile/edit/uploadProfilePicture" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="userId" value="{[ user.id ]}">
                        <input type="hidden" name="image" ng-value="ppCropper.croppedImage">
                        <input type="submit"> 
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Profile Cover Cropping Dialog -->
    <div ng-if="ownsProfile" id="cover-upload-modal" class="modal fade">
        <div class="modal-dialog" role="document" style="width:95%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times"></i></span>
                    </button>
                    <h4 class="modal-title">Adjust profile cover</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="profile/edit/uploadProfileCover" enctype="multipart/form-data">
                        <input type="file" img-cropper-fileread image="pcCropper.sourceImage" />
                        
                        <div>
                             <canvas width="1200" height="350" id="canvas" image-cropper image="pcCropper.sourceImage" cropped-image="pcCropper.croppedImage" crop-width="1140" crop-height="300" keep-aspect="true" touch-radius="30" crop-area-bounds="bounds"></canvas>
                        </div>

                        <br clear="both"/>
                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="userId" value="{[ user.id ]}">
                        <input type="hidden" name="image" ng-value="pcCropper.croppedImage">
                        <input type="submit"> 
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    @include('messaging.panel')

    @include('auth.login-panel')

    <div class="top-bar" ng-mouseover="hoverIn()" ng-mouseleave="hoverOut()">
        <div ng-hide="editTitleForm" >

            <h1>{[ user.userable.company.name ]}</h1>
            <h2>{[ user.userable.company.title ]}</h2>

            <span ng-if="ownsProfile" ng-show="hoverEdit">
                <a class="pull-left a-link animate-show" ng-click="showEditTitle()" ng-show="hoverEdit"><div class="glyphicon glyphicon-pencil"></div></a>
            </span>
        </div> 

        <div ng-show="editTitleForm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <h1>
                <input type="text" name="name" value="{[ user.userable.company.name ]}" ng-model="userChanges.userable.company.name">
            </h1>
            <h2>
                <input type="text" name="title" value="{[ user.userable.company.title ]}" ng-model="userChanges.userable.company.title">                
            </h2>
            
            <input type="hidden" name="id" value="{[user.id]}" />
            
            <button type="submit" class="button pull-left" style="margin-left: 10px; margin-top: 6.5px;" ng-click="update(); editTitleForm = false"><i class="fa fa-save"></i>Save</button>
            <a class="pull-left a-link" ng-click="editTitleForm = false"><i class="fa fa-close fa-lg"></i></a>       
        </div>

        <div class="dropdown container-right-margin pull-right">
            <button class="btn btn-primary dropdown-toggle" style="background-color:#5a1d4f;"type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="fa fa-align-justify fa-1.7x"></i>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="#messaging" class="messaging-window" ng-click="openContact()">Contact us</a></li>
            </ul>
        </div>
    
    </div>

    <div class="grey-section container-top-padding">
        <div class="col-md-6 info">
            <div class="container-left-margin">
                <div class="info-section" ng-hide="showEditInfoForm" ng-mouseover="hoverIn()" ng-mouseleave="hoverOut()">
                    <!-- Edit Controller -->
                    <a ng-if="ownsProfile" class="pull-right a-link" ng-show="hoverEdit" ng-click="openEditInfoForm()"><div class="glyphicon glyphicon-pencil"></div></a>
                    <!--       End       -->
                    
                    <h3>INFO</h3>

                    <p class="categoryTitle">Main Category:</p>
                    <p class="categoryText">{[ user.category.name ]}</p>

                    <p class="categoryTitle">Subcategory:</p>
                    <p class="categoryText"><span ng-repeat="subcategory in user.sub_categories">{[subcategory.name]}{[$last ? '' : ', ']}</span></p>

                    <p class="categoryTitle">Location:</p>
                    <p class="categoryText">{[ user.country.name ]}</p>

                    <p class="categoryTitle">Web page:</p>
                    <p class="categoryText"><a href="http://{[ user.userable.company.website ]}" target="_blank">{[ user.userable.company.website ]}</a></p>

                    <p class="categoryTitle">Number of Employees:</p>
                    <p class="categoryText">{[ user.userable.company.number_of_employees ]}</p>

                </div>

                <div ng-if="ownsProfile" ng-show="showEditInfoForm">
                    <a class="pull-right a-link" ng-show="showEditInfoForm" ng-click="cancelEditForm()"><i class="fa fa-close fa-lg"></i></a>
                    
                    <h3>EDIT INFO</h3>

                    <p class="categoryTitle">Main Category:</p>
                    <p class="categoryText">
                        <select id="JobCategories" class="form-control" name="category_id" ng-model="userChanges.category" ng-options="category.name for category in allCategories">
                        </select>
                    </p>

                    <p class="categoryTitle">Subcategories:</p>
                    <p class="categoryText">
                    <div     
                        isteven-multi-select
                        input-model="userChanges.category.sub_categories"
                        output-model="userChanges.sub_categories"
                        button-label="icon name"
                        item-label="icon name maker"
                        tick-property="ticked">
                        </div>
                    </p>
                    
                    <p class="categoryTitle">Location:</p>
                    <p class="categoryText">
                        <select class="form-control" name="country_id" ng-model="userChanges.country_id" ng-options="country.id as country.name for country in allCountries">
                        </select>
                    </p>


                    <p class="categoryTitle">Website:</p>
                    <p class="categoryText"><input class="form-control" ng-model="userChanges.userable.company.website" type="text" name="website" ng-value="userChanges.userable.company.website"></p>
                    <p class="categoryTitle">Number of Employees:</p>
                    <p class="categoryText">
                        <select class="form-control" name="number_of_employees" ng-model="userChanges.userable.company.number_of_employees">
                            <option value="1-10">1-10</option>
                            <option value="11-50">11-50</option>
                            <option value="51-200">51-200</option>
                            <option value="201-500">201-500</option>
                            <option value="501-1000">501-1000</option>
                            <option value="1001-5000">1001-5000</option>
                            <option value="5001-10000">5001-10000</option>
                            <option value="10000+">10000+</option>
                        </select>
                    </p>

                    <br />
                    <input type="hidden" name="id" value="{[user.id]}" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="button" ng-click="submitEditForm();"><i class="fa fa-save"></i> Save</button>
                </div>
                <br clear="both"/>
            </div>
        </div>

        <div class="col-md-6" ng-mouseover="hoverIn()" ng-mouseleave="hoverOut()">
            <div class="container-right-margin"> 
                <div class="cvideon" ng-if="user.videos.length > 0 && user.videos[0].key == videoType.path" ng-hide="loadingVideoUpload">
                    <videogular vg-theme="config.theme">
                        <vg-media vg-src="config.sources"
                                  vg-tracks="config.tracks">
                        </vg-media>

                        <vg-controls>
                            <vg-play-pause-button></vg-play-pause-button>
                            <vg-time-display>{[ currentTime | date:'mm:ss' ]}</vg-time-display>
                            <vg-scrub-bar>
                                <vg-scrub-bar-current-time></vg-scrub-bar-current-time>
                            </vg-scrub-bar>
                            <vg-time-display>{[ timeLeft | date:'mm:ss' ]}</vg-time-display>
                            <vg-volume>
                                <vg-mute-button></vg-mute-button>
                                <vg-volume-bar></vg-volume-bar>
                            </vg-volume>
                            <vg-fullscreen-button></vg-fullscreen-button>
                        </vg-controls>

                        <vg-overlay-play></vg-overlay-play>
                        <vg-buffering></vg-buffering>
                    </videogular>
                </div>

                <div class="cvideon" ng-if="user.videos.length > 0 && user.videos[0].key == videoType.url" ng-hide="loadingVideoUpload">
                    <iframe style="width:100%; min-height:310px;" ng-src="{[ user.videos[0].value ]}" frameborder="0" allowfullscreen></iframe>
                </div>

                <div ng-if="user.videos.length && ownsProfile" ng-show="hoverEdit">
                    <br />
                    <div class="col-md-12">
                        <table style="width:100%; text-align:center; margin-top:0px; margin-bottom:10px;" ng-hide="showEmbedVideo">
                            <tr ng-hide="replaceVideo">
                                <td><a class="button" style="font-size:12px;" ng-click="deleteVideo( user.videos[0].id )">Remove video</a></td>
                                <td><a class="button uploadVideoBtn" style="font-size:12px;" ngf-select="loadingVideoUpload = true; upload($file, '/profile/addVideo')" ng-model="file" name="file" ngf-pattern="'video/*'"
                                            accept="video/*" ngf-max-size="100MB">Upload new video</a></td>
                                <td><a class="button" style="font-size:12px;" ng-click="showEmbedVideo = true">Embed new video</a></td>
                            </tr> 
                        </table>  

                        <table style="width:100%; text-align:center; margin-top:-7px;" ng-show="showEmbedVideo">
                            <tr ng-hide="replaceVideo">
                                <td style="width:70%;"><input type="text" ng-model="userChanges.newEmbededVideo" class="form-control pull-left" name="embedLink" style="width:100%;" placeholder="Youtube or Vimeo link"></td>
                                <td><button ng-click="embedNewVideo()" class="form-control button pull-left" style="width:80%; margin-top:10px; padding-top:8px;">Embed</button>
                                <a class="pull-right a-link" ng-click="showEmbedVideo = false"><i class="fa fa-close fa-lg"></i></a>
                                </td>
                            </tr> 
                        </table> 
                    </div>
                    <div class="col-md-1"></div>
                </div>

                <div class="cvideon container-right-margin" ng-if="!user.videos.length">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div ng-hide="showEmbedVideo">
                            <div class="uploadVideoTable" ng-hide="loadingVideoUpload" ng-if="ownsProfile">
                                <div class="col-md-12">
   
                                    <div class="col-md-6 video-form-options">
                                        <a class="button" ngf-select="loadingVideoUpload = true; upload($file, '/profile/addVideo')" ng-model="file" name="file" ngf-pattern="'video/*'" accept="video/*" ngf-max-size="100MB">Upload Video</a>
                                    </div>

                                    <div class="col-md-6 video-form-options">
                                        <a class="button" ng-click="openEmbedVideo()">Embed Video</a>
                                    </div>

                                    <div class="clearfix spacer"></div>
                                    <p><i>* Upload your CVideo of maximum 90 seconds or 70 mb.</i></p>
                                </div>
                            </div>
                        </div>
                        <table style="width:100%; text-align:center; margin-top:120px; font-family:'Raleway-Regular';" ng-show="loadingVideoUpload">
                            <tr>
                                <td><img src="assets/images/loader.gif"></td>
                            </tr>
                            <tr>
                                <td><p style="padding-top:10px; color:#5a1d4f;">Please wait while your video is uploading...</p><td>
                            </tr>
                        </table>
                        <div class="embedForm" ng-show="showEmbedVideo">
                            <input type="text" ng-model="userChanges.newEmbededVideo" class="form-control pull-left" name="embedLink" style="width:70%;"  placeholder="Youtube or Vimeo link">
                            <button ng-click="embedVideo()" class="form-control button pull-left" style="width:26%; padding-top: 8px;">Embed</button>
                            <a class="pull-right a-link" ng-click="showEmbedVideo = false"><i class="fa fa-close fa-lg"></i></a>
                        </div>
                    </div>
                    <br />
                    
                </div>
            </div>
        </div>
        <br clear="both"/>
    </div>
    <div class="description container-fluid container-right-margin container-left-margin">
        
        <div ng-hide="showEditGoal" ng-mouseover="hoverIn()" ng-mouseleave="hoverOut()">
            <h3 class="pull-left" style="width:100%;">SUMMARY <a ng-if="ownsProfile" class="a-link" style="margin-left:150px; font-size:16px; margin-top:5px;" ng-click="openEditGoal()" ng-show="hoverEdit"><div class="glyphicon glyphicon-pencil"></div></a></h3>

            <p>{[ user.userable.company.description ]}</p>
        </div>
        <div ng-if="ownsProfile" ng-show="showEditGoal">
            <h3 class="pull-left" style="width:100%;">EDIT SUMMARY <a class="pull-right a-link" style="font-size:16px; margin-top:5px;" ng-show="showEditGoal" ng-click="closeEditGoal()"><i class="fa fa-close fa-lg"></i></a></h3>

            <textarea name="summary" class="form-control" ng-model="userChanges.userable.company.description">{[ user.userable.company.description ]}</textarea><br />
            <button type="submit" class="btn btn-success btn-block" ng-click="update(); showEditGoal = false"><i class="fa fa-save"></i> Save</button>
        </div>

        <div class="spacer"></div>
        <div class="keywordsTitle">
            <h3>KEYWORDS</h3>
            <div class="clearfix"></div> 
        </div>
        <ul class="profile-keywords">
            <div id="professionalKeywords">
                <li class="keyword" ng-repeat="keyword in user.tags | filter: { tag_type_id: '4' }" ng-mouseover="hoverIn()" ng-mouseleave="hoverOut()">
                    {[ keyword.name ]}
                    <a ng-if="ownsProfile" class="pull-right" style="margin-right:10px;" ng-click="deleteKeyword(keyword.id)" ng-show="hoverEdit">
                        <div class="glyphicon glyphicon-remove" style="color:grey;"></div>
                    </a>
                </li>
                <li ng-if="ownsProfile" class="keyword" style="padding:5px;">
                    <a id="addProfessionalKeyword" ng-hide="addCompanyKeyword" ng-click="addCompanyKeyword = true">+Add Keyword</a>
                        <div ng-show="addCompanyKeyword">
                            <input autofocus type="text" class="form-control pull-left keyword-input" placeholder="Add keyword..."
                            name="companyTag"
                            ng-model="newCompanyTagName"
                            uib-typeahead="obj as obj.text for obj in getSuggestions($viewValue, 'company_tag')"
                            typeahead-on-select="onSelect($item, $model, $label); newCompanyTagName = ''"
                            uib-tooltip="You can't add a duplicate keyword. Please add a new one."
                            tooltip-class="tooltip-error"
                            tooltip-placement="top"
                            tooltip-trigger="focus"
                            tooltip-enable="duplicateCompanyTag"
                            tooltip-is-open="duplicateCompanyTag || false">
                            <a href="#" class="pull-right" style="padding-top:7px; padding-right:3px; color:#5a1d4f;"
                            ng-click="addKeyword(newCompanyTagName, 4); newCompanyTagName = ''">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <br />
                </li>
            </div>
        </ul>

        <div class="clearfix"></div>
        <div class="spacer"></div>

    </div>
    <div class="spacer"></div>
</div>
</div><!-- closing controller -->

@endsection

@section('footer')
    <footer>
        @include('layouts.footer')
    </footer>
@endsection

@section('javascript')
    <script>
        $(function(){
            $("a#replaceVideo").click(function(){
                $("a#replaceVideo").css({"display": "none"});
                $("#uploadVideoForm").css({"display": "block"});
            });
 
        });

        $(document).ready(function() {
            // ---- Display login pop-up
            $('a.login-window').click(function() {
                //Getting the variable's value from a link
                var loginBox = $(this).attr('href');

                //Fade in the Popup
                $(loginBox).fadeIn(300);

                //Set the center alignment padding + border see css style
                var popMargTop = ($(loginBox).height() + 24) / 2;
                var popMargLeft = ($(loginBox).width() + 24) / 2;

                $(loginBox).css({
                    'margin-top' : -popMargTop,
                    'margin-left' : -popMargLeft
                });

                // Add the mask to body
                $('body').append('<div id="mask"></div>');
                $('#mask').fadeIn(300);

                return false;
            });

            // When clicking on the button close or the mask layer the popup closed
            $('a.close-login, #mask').on('click', function() {
                $('#mask , .login-popup').fadeOut(300 , function() {
                    $('#mask').remove();
                });
                return false;
            });

            // ---- Display messaging pop-up
            $('a.messaging-window').click(function() {
                //Getting the variable's value from a link
                var loginBox = $(this).attr('href');

                //Fade in the Popup
                $('#messaging-box').fadeIn(300);

                //Set the center alignment padding + border see css style
                var popMargTop = ($('#messaging-box').height() + 24) / 2;
                var popMargLeft = ($('#messaging-box').width() + 24) / 2;

                $('#messaging-box').css({
                    'margin-top' : -popMargTop,
                    'margin-left' : -popMargLeft
                });

                // Add the mask to body
                $('body').append('<div id="mask"></div>');
                $('#mask').fadeIn(300);

                return false;
            });

            // When clicking on the button close or the mask layer the popup closed
            $('.close-messaging, #mask').on('click', function() {
                $('#mask , .messaging-popup').fadeOut(300 , function() {
                    $('#mask').remove();
                });
                return false;
            });

            // -- Display welcome message when loggin in
            $("#welcome-alert").hide();
            $("#welcome-alert").alert();
            $("#welcome-alert").fadeTo(10000, 500).slideUp(1000, function(){
                $("#welcome-alert").alert('close');
            });      

        });

    </script>
@endsection