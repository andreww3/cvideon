<nav class="navbar navbar-default profile-navbar" controller="profileCtrl">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header ">
            <button type="button" class="navbar-toggle collapsed home-menu-top-margin" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand col-lg-3 col-md-3 col-sm-3 hidden-xs"><a href="/" id="logo" target="_self"><img style="margin-top:3px" src="/assets/images/CVideon-logo.png"></a></div>
        </div>


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            @if ( Auth::check() )
            <ul class="nav navbar-nav navbar-right profile-menu-top-margin">
                <!--<li><a href="#">My CVideon</a></li>-->
                <li><a href="user/profile" target="_self">My CVideon</a></li>

                <li>  
                    <a href="/messages" target="_self">
                    Messages
                    <span ng-show="unreadMessages != 0">
                      ({[unreadMessages]} )
                    </span>
                    </a>
                </li>

                @if ( Auth::user()->userable_type == "App\Seeker" )
                    
                    <li><a href="/search/companies" target="_self">Companies</a></li>
                @endif

                @if ( Auth::user()->userable_type == "App\CompanyUser" )
                    <li><a href="/search/seekers" target="_self">Job Seekers</a></li>
                @endif

                <!--<li><a href="#">Jobs</a></li>-->
                <li><a href="/auth/logout" target="_self">Logout</a></li>
            </ul>
            @else
            <ul class="nav navbar-nav navbar-right profile-menu-top-margin">
                <li><a href="{{ url('/register') }}">SIGN UP</a></li>
                <li><a href="#login-box" class="login-window">LOG IN</a></li>
            </ul>
            @endif
            <div class="clearfix spacer"></div>
        </div><!-- /.navbar-collapse -->
        <div class="visible-xs" style="margin-top:-20px; margin-bottom:15px">
            <a href="{{ url('/') }}"><img id="mobile-logo" class="img-responsive col-xs-7" style="margin-left:-15px; margin-top:-15px" src="/assets/images/CVideon-logo.png"></a>
        </div>
    </div><!-- /.container-fluid -->
</nav>

