<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
    	<a href="http://cvideon.com/"><img width="150px" src="http://cvideon.com/assets/images/CVideon-logo.png"/></a>
	    <div>
	    	<p>Hi {{ $name }},</p>

	    	<p>Thanks for confirming your account and joining our community!</p>
	    	<p>Here's a summary of what you can accomplish with CVideon:</p>
	    	<br />
	    	<h3>Branding</h3>
	    	<h4>Upload your very own video CV.</h4>
	    	<p>Employers are keen to hear about your background and qualifications, but most importantly they are interested in your personality. Make sure it shines through!</p>
	    	<br />
	    	<h3>Browse</h3>
	    	<h4>Search for companies that are of interest to you</h4>
	    	<p>Hundreds of companies are looking for someone just like you! Find a company that matches with gyour interests.</p>
	    	<br />
	    	<h4>We’re here to help!</h4>
	    	<p><i>If you run into any problems, contact us at info@cvideon.com and our support team will make sure to solve any issues you have encountered.</i></p>
	    	<br />
	    	<p>Great to have you on board!</p>
	    	<p>The CVideon team</p>
	    	<a href="http://cvideon.com/"><img width="100px" src="http://cvideon.com/assets/images/CVideon-logo.png"/></a>
	    </div>
    </body>
</html>



