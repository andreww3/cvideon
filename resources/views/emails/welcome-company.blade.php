<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
    	<a href="http://cvideon.com/"><img width="150px" src="http://cvideon.com/assets/images/CVideon-logo.png"/></a>
	    <div>
	    	<p>Hi {{ $name }},</p>

	    	<p>Thanks for confirming your company account and joining our community!</p>
	    	<p>Here's a summary of what you can accomplish:</p>
	    	<br />
	    	<h3>Promotion</h3>
	    	<h4>Upload company video material</h4>
	    	<p>Job seekers are keen to hear about your your company’s vision and work environment, so make sure to upload video material that describes your business in the best way possible!</p>
	    	<br />
	    	<h3>Browse</h3>
	    	<h4>Search for candidates that match your needs</h4>
	    	<p>Hundreds of candidates are looking to work for a company just like yours! Browse through video CVs and find the right candidate for your business.</p>
	    	<br />
	    	<h4>We’re here to help!</h4>
	    	<p><i>If you run into any problems, contact us at info@cvideon.com and our support team will make sure to solve any issues you have encountered.</i></p>
	    	<br />
	    	<p>Great to have you on board!</p>
	    	<p>The CVideon team</p>
	    	<a href="http://cvideon.com/"><img width="100px" src="http://cvideon.com/assets/images/CVideon-logo.png"/></a>
	    </div>
    </body>
</html>



