<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
    	<a href="http://cvideon.com/"><img width="150px" src="http://cvideon.com/assets/images/CVideon-logo.png"/></a>
	    <div>
	    	<p>Hi {{ $receiverName }},</p>

	    	<p>You have just received a message on CVideon from 
	    	<?php
	    	if($senderCompany != NULL)
	    		return $senderCompany;
	    	
	    	else
	    		return $senderName;
	    	?>
			!</p>

	    	<br />
	    	<h3>Subject: {{ $subject }}</h3>
	    	<h4>Message: </h4>
	    	<p>{{ Str::limit($message, 50) }}</p>
	    	<a href="http://cvideon.com/messages/">See your messages here.. </a>
	    	<br />
	    	<h4>We’re here to help!</h4>
	    	<p><i>If you run into any problems, contact us at info@cvideon.com and our support team will make sure to solve any issues you have encountered.</i></p>
	    	<br />
	    	<p>Kind regards,</p>
	    	<p>The CVideon team</p>
	    	<a href="http://cvideon.com/"><img width="100px" src="http://cvideon.com/assets/images/CVideon-logo.png"/></a>
	    </div>
    </body>
</html>



