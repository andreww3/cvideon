<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
    	<a href="http://cvideon.com/"><img width="150px" src="http://cvideon.com/assets/images/CVideon-logo.png"/></a>
	    <h1>Reset Your Password</h1>
	    <div>
	        <br />
	        <p>Please follow the link below to reset your password:</p>
	        <a href="http://cvideon.com/password/reset/{{ $token }}">http://cvideon.com/password/reset/{{ $token }}</a>
	    	<br />
	    	<br />
	    	<h5>We’re here to help!</h5>
	    	<p><i>If you run into any problems, contact us at info@cvideon.com and our support team will make sure to solve any issues you have encountered.</i></p>
	    	<br />
	    	<p>Great to have you on board!</p>
	    	<p>The CVideon team</p>
	    	<a href="http://cvideon.com/"><img width="100px" src="http://cvideon.com/assets/images/CVideon-logo.png"/></a>
	    </div>

    </body>
</html>