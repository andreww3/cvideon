var app = angular.module('cvideon', [
    'ngRoute',
    'ui.bootstrap',
    'ngAnimate',
    'ngMaterial',
    'checklist-model',
    'isteven-multi-select',
    'ngFileUpload',
    'angular-img-cropper',
    'ngSanitize',
    'com.2fdevs.videogular',
    'com.2fdevs.videogular.plugins.controls',
    'com.2fdevs.videogular.plugins.overlayplay',
    'com.2fdevs.videogular.plugins.buffering',
    'angularMoment'
]);

app.config(['$routeProvider', '$locationProvider', '$interpolateProvider', '$sceDelegateProvider', function ($routeProvider, $locationProvider, $interpolateProvider, $sceDelegateProvider) {

    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');

    // routing rules
    $routeProvider.when('/registration/seeker/' +
        ':id', {
        controller: 'regCtrl'
    }).
    when('/registration/company/' +
        ':id', {
        controller: 'regCtrl'
    }).
    when('/user/' +
        ':id/' +
        ':welcome', {
        controller: 'profileCtrl'
    }).
    when('/company/' +
        ':id/' +
        ':welcome', {
        controller: 'profileCtrl'
    }).
    when('/user/' +
        ':id', {
        controller: 'profileCtrl'
    }).
    when('/videoCV/'+ ':userId', {
        controller: 'videoCtrl'
    }).
    when('/company/' +
        ':id', {
        controller: 'profileCtrl'
    }).
    when('user/business-card/' +
        ':userId',{
        controller: 'bCardController'
    });

    // html5Mode
    $locationProvider.html5Mode(true);

    $sceDelegateProvider.resourceUrlWhitelist([
        'self', '*://www.youtube.com/**', '*://www.facebook.com/**','*://player.vimeo.com/video/**'
    ]);
}]);
