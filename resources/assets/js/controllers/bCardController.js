angular.module('cvideon').controller('bCardController', 
	['$rootScope', '$scope', '$routeParams', '$route', '$http', '$filter', '$controller', '$sce', '$window', 'Upload',
		function ($rootScope, $scope, $routeParams, $route, $http, $filter, $controller, $sce, $window, Upload) {

			// imports the scope of user controller
			//var profileCtrl = $controller('profileCtrl', {$scope: $scope});

            $rootScope.$on('$routeChangeSuccess', function () {
                $http.get('/api/user/' + $routeParams.userId).success(function (data) {
                    $scope.user = angular.copy(data);                    
                }); 
            });

		}]);