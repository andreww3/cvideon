angular.module('cvideon').controller('regCtrl', 
	['$rootScope', '$scope', '$routeParams', '$route', '$http', '$filter', '$controller', '$sce', '$window','$compile', 'Upload',
		function ($rootScope, $scope, $routeParams, $route, $http, $filter, $controller, $sce, $window, $compile, Upload) {

			// imports the scope of user controller
			var profileCtrl = $controller('profileCtrl', {$scope: $scope});

			// Scopes for the user signup
			$scope.defaultCategory = "Select a category model";
			$scope.signupErrorMap = {};
			$scope.signupCompanyErrorMap = {};

			$scope.processForm = function(form) { 
				$http.post('/auth/register', $scope.userForm).then(function successCallback(response){
 					var loggedInUser;
                	$http.get('/showLoggedInUser').success(function (data) {
                    	var loggedInUser = angular.copy(data);
                      	//var redirectLink = 'registration/seeker/' + loggedInUser.trim();                  	
                    	document.location.href=('/thanks');
                    }); 
				}, function errorCallback(response){
					// Baking the error response 
					console.log("Error!");
					//console.log(response.data);
					$scope.signupErrorMap = response.data; 
				});
			};

			$scope.removeErrorMsg = function(paramString){
				$scope.signupErrorMap[paramString] = false;
			};

			// Scopes for company signup
			$scope.processCompanyForm = function(form){
				//console.log(form);
				$http.post('/auth/register', $scope.companyForm).then(function successCallback(response){
 					var loggedInUser;
                	$http.get('/showLoggedInUser').success(function(data) {
                    	var loggedInUser = angular.copy(data);

                    	//var redirectLink = 'registration/company/' + loggedInUser.trim();                 	
                    	document.location.href=('/thanks');
                    });
				}, function errorCallback(response){
					// Baking the error response 
					console.log("Awww Error!");
					console.log(response.data);
					$scope.signupCompanyErrorMap = response.data;
				});
			};

			$scope.removeCompanyErrorMsg = function(paramString){
				$scope.signupCompanyErrorMap[paramString] = false;
			};

		    // Scopes for Registration Tabs
		    $scope.clickableTab0= "'true'"
		    $scope.clickableTab1= "'true'"
		    $scope.clickableTab2= "'true'"
		    $scope.clickableTab3= "'true'"
		    
		    var tabs = [],
		    	selected = null,
		    	previous = null;

		    $scope.tabs = tabs;
		    $scope.selectedIndex = 0;

		    $scope.$watch('selectedIndex', function(current, old){	
		    	previous = selected;
		    	selected = tabs[current];
		    });

		    $scope.submitFirstStep = function () { 
                $scope.userChanges.languages = angular.copy($scope.selectedLanguages);
                $scope.update();
		    	$scope.clickableTab1 = 0; 
		    }

		    /* Variable to enable/disable Go to step 2 button */
		    //$scope.someInputEmpty = !($scope.userChanges.userable.job_title || userChanges.userable.status_quo);
		    console.log("CHECKING IF SCOPE IS IMPORTED, JOB TITLE: " + $scope.userChanges);

            $scope.upload = function (file, url) {
            	$scope.loadingVideoUpload = true;
                Upload.upload({
                    url: url,
                    data: {file: file}
                }).then(function (resp) {
		    		$http.get('api/user/' + $routeParams.id).success(function (data) {
                    $scope.user = angular.copy(data);
                    $scope.resetUserData();

                    angular.forEach($scope.user.videos, function(video, key){
                        var extension = video.value.split("?")[0].split(".").pop();

                        $scope.config.sources.push({
                            src: $sce.trustAsResourceUrl(video.value),
                            type: "video/" + extension
                        })
                    });

                    $scope.upload_loaded = true;
                    $scope.loadingVideoUpload = false;
                });
                    //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                }, function (evt) {
                    //var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            }

		    $scope.goToUserProfile = function (user) {
		    	document.location.href=('/user/'+ user + '/welcome');
			};

			$scope.goToCompanyProfile = function (user) {
		    	document.location.href=('/company/'+ user + '/welcome');
			};

		}]);