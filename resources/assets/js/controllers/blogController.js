angular.module('cvideon').controller('blogCtrl',
	['$rootScope', '$scope', '$route', '$http', 'filterFilter',
	 function ($rootScope, $scope, $route, $http, filterFilter) {

	/* *
	  * * * Initialize posts * * * *
	   						      */

    $scope.get6Posts = function () {
        $http({
            method: 'GET',
            url: '/api/6/posts'
        }).then(function (response) {
            $scope.posts = response.data;
            //console.log($scope.projects);
        }, function (response) {
            console.log(response)
        });

       $scope.showingAllPosts = false;
    }

    $scope.get6Posts();

    $scope.getAllPosts = function () {

        $http({
            method: 'GET',
            url: '/api/posts'
        }).then(function (response) {
            $scope.posts = response.data;
            //console.log($scope.projects);
        }, function (response) {
            console.log(response)
        });

       $scope.showingAllPosts = true;
    }
    

    /* *
      * End * *
             */ 


	/* *
	  * * * Change category * * * *
	   						     * */

	$scope.selectCategory = function($id) {

        $http({
            method: 'GET',
            url: '/api/posts'
        }).then(function (response) {
            $scope.posts = response.data;

			$scope.posts = filterFilter($scope.posts, {category_id:$id});

			console.log($scope.posts);

        }, function (response) {
            console.log(response)
        });

	}

    /* *
      * End * *
             */ 
   						           
	 }]);