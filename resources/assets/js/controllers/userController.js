angular.module('cvideon').controller('profileCtrl',
    ['$rootScope', '$scope', '$routeParams', '$route', '$http', '$controller', 'Upload', '$sce', '$filter',
        function ($rootScope, $scope, $routeParams, $route, $http, $controller, Upload, $sce, $filter) {

            $scope.user = undefined;
            $scope.userChanges = undefined;
            $scope.selectedSubcategories = [];
            $scope.selectedLanguages = [];
            $scope.education = [];
            $scope.experience = [];
            $scope.user_loaded = false;
            $scope.keyToDel = undefined;
            $scope.embedVideo = false;
            $scope.unreadMessages = 0;

            $scope.config = {
                sources: [],
                preload: "none",
                theme: "/css/custom.css",
            };

            $scope.userType = {
                'seeker': 'App\\Seeker',
                'company': 'App\\CompanyUser'
            }

            $scope.videoType = {
                'path': "path",
                'url' : "url"
            }

            /* Handling newly user first profile visit */
            $scope.welcomeMsgNewUser = "Welcome to your CVideon profile! To complete your registration please upload a profile picture and a cover!";
            $scope.showWelcome = false;

            if($routeParams.id == 'business-card'){
                console.log('it is there');
                console.log($routeParams);

            };

            $rootScope.$on('$routeChangeSuccess', function () {

                $http.get('/showLoggedInUser').success(function (data) {
                    $scope.loggedInUser = angular.copy(data);
                    $scope.getUnreadMessages($scope.loggedInUser);
                });
                
                $http.get('/api/user/' + $routeParams.id).success(function (data) {
                    $scope.user = angular.copy(data);
                    $scope.resetUserData();

                    $scope.profileForUser = $scope.user.id;

                    angular.forEach($scope.user.videos, function(video, key){
                        var extension = video.value.split("?")[0].split(".").pop();

                        $scope.config.sources.push({
                            src: $sce.trustAsResourceUrl(video.value),
                            type: "video/" + extension
                        })
                    });
                    
                    /* Initializes the languages selection with user's registration data, if any */
                    angular.forEach($scope.user.languages, function (userLanguageObj, userLanguageIndex) {
                        angular.forEach($scope.languages, function (dBlanguageObj, dBlanguageIndex) {                        
                            if($scope.languages[dBlanguageIndex].id == $scope.user.languages[userLanguageIndex].id){
                                $scope.languages[dBlanguageIndex].ticked = true; 
                            }
                        });
                    }); 

                    /* Initializes the subcategory selection with user's registration data, if any */
                    angular.forEach($scope.user.category.sub_categories, function (value1, index1) {
                        angular.forEach($scope.userChanges.sub_categories, function (value2, index2) {
                            if (value2.id == value1.id) {
                                $scope.userChanges.sub_categories[index2] = value1;
                                $scope.userChanges.sub_categories[index2].ticked = true;                        
                            }
                        });
                    });  

                    $scope.user_loaded = true; 

                    //The route parameter is defined only from registration. Prints the welcome message.
                    if($routeParams.welcome){
                        $scope.showWelcome = true;
                    };

                    // ---> Checking if the logged in user is on his own profile
                    if ($scope.profileForUser == $scope.loggedInUser) {
                        $scope.ownsProfile = true;

                    } else {
                        $scope.ownsProfile = false;
                    }
                });                  
            });

            // --> get number of unread Messages
            $scope.getUnreadMessages = function (loggedInUser) {

                $http.post('messages/getUnreadMessages', loggedInUser).success(function(data) {
                    $scope.unreadMessages = angular.copy(data);             

                });
            }
            
            // --- Print links
            $scope.getBcSrc = function (userId) {
                return '/user/business-card/' + userId;
            };

            /* Ng click events for controller */
            $scope.showEditTitle = function () {
                $scope.editTitleForm = true;
            }

            $scope.openEditInfoForm = function () {
                $scope.showEditInfoForm = true;
            }

            $scope.cancelEditForm = function () {
                $scope.resetUserData();
                $scope.showEditInfoForm = false;
            }

            $scope.openEmbedVideo = function () {
                $scope.showEmbedVideo = true;
            }

            $scope.openEditGoal = function () {
                $scope.showEditGoal = true;
            }

            $scope.closeEditGoal = function () {
                $scope.showEditGoal = false;
            }

            // ----> Experience add/edit open/close functions

            $scope.openAddExperience = function () {
                $scope.showAddExperience = true;
            }

            $scope.closeAddExperience = function () {
                $scope.showAddExperience = false;
            }

            $scope.openAddEducation = function(startingDate, endingDate) {
                $scope.addStartingDate = new Date();
                $scope.addEndingDate = new Date();
                $scope.showAddEducation = true;
            }

            $scope.closeAddEducation = function () {
                $scope.showAddEducation = false;
            }

            // --> get default data to populate the forms

            $scope.getCategories = function () {
                $http.get('api/category/all-categories').success(function (data) {
                    $scope.allCategories = data;
                });
            }; 

            $scope.getSubCategories = function () {
                $http.get('api/category/all-sub-categories').success(function (data) {
                    $scope.allSubCategories = data;
                });
            };
            
            $scope.getCountries = function () {
                $http.get('api/countries').success(function (data) {
                    $scope.allCountries = data;
                });
            };

            $scope.getLanguages = function () {
                $http.get('api/languages').success(function (data) {
                    $scope.allLanguages = data;
                });
            };

            $scope.hoverEdit = false;
            
            $scope.hoverIn = function () {
                this.hoverEdit = true;
            };

            $scope.hoverOut = function () {
                this.hoverEdit = false;
            };         
            

            // --- Image cropper

            $scope.ppCropper = {};
            $scope.ppCropper.sourceImage = null;
            $scope.ppCropper.croppedImage   = null;

            $scope.pcCropper = {};
            $scope.pcCropper.sourceImage = null;
            $scope.pcCropper.croppedImage   = null;

            $scope.myProfilePicture='';
            $scope.myCroppedProfilePicture='';

            var handlePictureSelect=function(evt) {
              var file=evt.currentTarget.files[0];
              var reader = new FileReader();
              reader.onload = function (evt) {
                $scope.$apply(function($scope){
                  $scope.myProfilePicture=evt.target.result;
                });
              };
              reader.readAsDataURL(file);
            };

            angular.element(document.querySelector('#profilePictureInput')).on('change',handlePictureSelect);


            $scope.myProfileCover='';
            $scope.myCroppedProfileCover='';

            var handleCoverSelect=function(evt) {
              var file = evt.currentTarget.files[0];
              var reader = new FileReader();
              reader.onload = function (evt) {
                $scope.$apply(function($scope){
                  $scope.myProfileCover = evt.target.result;
                });
              };
              reader.readAsDataURL(file);
            };

            angular.element(document.querySelector('#profileCoverInput')).on('change',handleCoverSelect);

            $scope.openContact = function() {

                $scope.receiver = {
                    'type': $scope.user.userable_type,
                    'id': $scope.user.id,
                    'name': $scope.user.name,
                    'email': $scope.user.email,
                }
                console.log($scope.receiver);

                $http.get('/showLoggedInUser').success(function (data) {
                    $scope.loggedInUser = angular.copy(data);

                }).then(function (response) {

                    var loggedInUserDataLink = '/api/user/' + $scope.loggedInUser;
                    loggedInUserDataLink = loggedInUserDataLink.replace(/[\s]/g, '');
                    $http.get(loggedInUserDataLink).success(function (data) {
                        $scope.sender = angular.copy(data);

                        if ($scope.sender.userable.company) {
                            $scope.theCompany = $scope.sender.userable.company.name;    
                        } else {
                            $scope.theCompany = undefined;
                        }

                        $scope.message = {
                            subject: undefined,
                            message: undefined,
                            receiver: $scope.receiver,
                            sender: {
                                type: $scope.sender.userable_type,
                                id: $scope.sender.id,
                                name: $scope.sender.name,
                                company: $scope.theCompany,    
                                email: $scope.sender.email
                            }
                        }
                        console.log($scope.message);
                    });
                          
                });                
            }

            $scope.contactUser = function() {

                var data = $scope.message;
                console.log(data);

                $http.post("/sendMessage", data)
                    .then( //success callback
                    function (response) {
                        if (response.data.success) {
                            console.log('Success!');
                            $scope.message = {};
                        }
                        else {
                            console.log(response.data);
                        }
                        return response;
                    }, //error callback
                    function (error) {
                        console.log("error: " + error);
                    }
                );
                $scope.messageSent = true;  
            }
            // --- Add Information to the profile page
            $scope.add = function() {
                var data = $scope.userChanges;
                data['_token'] = $('meta[name="csrf-token"]').attr('content');

                $http.post("/profile/add/info", data)
                    .then( //success callback
                    function (response) {
                        if (response.data.success) {
                            $scope.user = $scope.userChanges;
                            $scope.showEditInfoForm = false;
                        }
                        else {
                            //console.log(response.data);
                        }
                        return response;
                    }, //error callback
                    function (error) {
                        console.log("error: " + error);
                    }
                );
            }

            // --- Delete Video
            $scope.deleteVideo = function($videoId) {
                $scope.userChanges.deleteVideoId = $videoId;

                var videos = $scope.userChanges.videos;

                //filter the array
                var foundItem = $filter('filter')(videos, { id: $videoId }, true)[0];
                //get the index
                var index = videos.indexOf(foundItem);

                $scope.userChanges.videos.splice(index, 1);

                $scope.update();
            }

            // --- Embed Video ---- |

            $scope.embedVideo = function() {

                if ($scope.userChanges.newEmbededVideo.indexOf('vimeo') !== -1) {
                        $scope.userChanges.newVideo = $scope.userChanges.newEmbededVideo.replace('vimeo.com', 'player.vimeo.com/video');
                    }

                if ($scope.userChanges.newEmbededVideo.indexOf('youtube') !== -1) {
                        $scope.userChanges.newVideo = $scope.userChanges.newEmbededVideo.replace('watch?v=', 'embed/');
                    }
                
                $scope.userChanges.videos.push({
                    key: "url",
                    value: $scope.userChanges.newVideo
                });      
                console.log($scope.userChanges.newVideo);
                $scope.add();
            }

            $scope.embedNewVideo = function() {

                if ($scope.userChanges.newEmbededVideo.indexOf('vimeo') !== -1) {
                        $scope.userChanges.newVideo = $scope.userChanges.newEmbededVideo.replace('vimeo.com', 'player.vimeo.com/video');
                    }

                if ($scope.userChanges.newEmbededVideo.indexOf('youtube') !== -1) {
                        $scope.userChanges.newVideo = $scope.userChanges.newEmbededVideo.replace('watch?v=', 'embed/');
                    }          

                $scope.userChanges.videos = [{
                    key: "url",
                    value: $scope.userChanges.newVideo
                }]; 

                console.log($scope.userChanges.newVideo);
                $scope.showEmbedVideo = false; 
                $scope.add();

            }

            // --- KEYWORDS ADD, ASSOCIATE & DELETE

            $scope.searchInput = undefined;
            $scope.duplicatePersonalTag = false;
            $scope.duplicateProfessionalTag = false;
            $scope.newProfTagName = '';
            $scope.newPersTagName = '';
            $scope.newCompanyTagName = '';

            $scope.onSelect = function ($item, $model, $label) {
                $scope.searchInput = $item;
                var tagExists = -1;
                angular.forEach($scope.user.tags, function (value, index) {
                    if (value.name === $scope.searchInput.payload.name) {
                        console.log("TAG EXISTS!");
                        tagExists++;
                    } 
                });
                if (tagExists !== -1) {
                    // It's a personal tag, focus to trigger tooltip error
                    if ($scope.searchInput.payload.type.id == 1) {
                        $scope.duplicatePersonalTag = true;
                        angular.element("[name='personalTag']").focus();
                    }

                    // It's a professional tag, focus to trigger tooltip error
                    if ($scope.searchInput.payload.type.id == 2) {
                        $scope.duplicateProfessionalTag = true;
                        angular.element("[name='professionalTag']").focus();
                    }

                    // It's a job tag, focus to trigger tooltip error
                    /*
                    if ($scope.searchInput.payload.type.id == 3) {
                        $scope.duplicateJobTag = true;
                        angular.element("[name='jobTag']").focus();
                    } 
                    */

                    // It's a company tag, focus to trigger tooltip error
                    if ($scope.searchInput.payload.type.id == 4) {
                        $scope.duplicateCompanyTag = true;
                        angular.element("[name='companyTag']").focus();
                    }

                } else {

                    $scope.userChanges.newtagid = $scope.searchInput.payload.id;
                    
                    // Resetting the duplicated errors 
                    if ($scope.searchInput.payload.type.id == 1) {
                        $scope.duplicatePersonalTag = false;
                        angular.element("[name='personalTag']").focus();
                    }

                    if ($scope.searchInput.payload.type.id == 2) {
                        $scope.duplicateProfessionalTag = false;
                        angular.element("[name='professionalTag']").focus();
                    }     

                    /*
                    if ($scope.searchInput.payload.type.id == 3) {
                        $scope.duplicateJobTag = false;
                        angular.element("[name='jobTag']").focus();
                    } 
                    */

                    if ($scope.searchInput.payload.type.id == 4) {
                        $scope.duplicateCompanyTag = false;
                        angular.element("[name='companyTag']").focus();
                    }          

                    //$scope.add($scope.userChanges);

                    var data = $scope.userChanges;
                    data['_token'] = $('meta[name="csrf-token"]').attr('content');

                    $http.post("/profile/add/info", data)
                        .then( function successCallback(response) {
                            console.log("SUCCESS! Data posted");
                                //getting the new user data to update the scope
                                $http.get('api/user/' + $routeParams.id).then(function successCallback(response){
                                    $scope.showEditInfoForm = false;
                                    $scope.user.tags = response.data.tags;                
                                    //Clear the user changes to avoid tag repetition
                                    if($scope.userChanges.newtagid){
                                        delete $scope.userChanges.newtagid;
                                    }
                                }, function errorCallback(response){
                                    console.log("Awww error in the get! " + response);
                                });
                        }, function errorCallback(response) {
                            console.log("Awww error! " + response);
                        }
                    );
                }
            };


            $scope.addKeyword = function ($tag, $tagType) {
                $scope.duplicateKeyword = false;

                // Code repetition here, same as onSelect, move out in a function
                var tagExists = -1;
                angular.forEach($scope.user.tags, function (value, index) {
                    if (value.name === $tag) {
                        tagExists++;
                    } 
                });

                if (tagExists !== -1) { 
                    //console.log('Tag already exists');
                    if ($tagType == 1) {
                        $scope.duplicatePersonalTag = true;
                        angular.element("[name='personalTag']").focus();
                    }

                    if ($tagType == 2) {
                        $scope.duplicateProfessionalTag = true;
                        angular.element("[name='professionalTag']").focus();
                    }                     

                } else {
                    //console.log('It is a new tag');
                    if ($tagType == 1) {
                        $scope.duplicatePersonalTag = false;
                        angular.element("[name='personalTag']").focus();
                    }

                    if ($tagType == 2) {
                        $scope.duplicateProfessionalTag = false;
                        angular.element("[name='professionalTag']").focus();
                    }                     

                    $scope.userChanges.newTag = {
                        name: $tag,
                        tag_type_id: $tagType
                    }
                    
                    //$scope.add();

                    var data = $scope.userChanges;
                    data['_token'] = $('meta[name="csrf-token"]').attr('content');

                    $http.post("/profile/add/info", data)
                        .then( function successCallback(response) {
                            console.log("SUCCESS! Data posted");
                                //getting the new user data to update the scope
                                $http.get('api/user/' + $routeParams.id).then(function successCallback(response){
                                    $scope.showEditInfoForm = false;
                                    $scope.user.tags = response.data.tags;

                                    //Clear the user changes to avoid tag repetition
                                    if($scope.userChanges.newTag){
                                        delete $scope.userChanges.newTag;
                                    }
                                }, function errorCallback(response){
                                    console.log("Awww error in the get! " + response);
                                });
                        }, function errorCallback(response) {
                            console.log("Awww error! " + response);
                        }
                    );                    
                }
            };

            $scope.$watch("newProfTagName", function() {
                if ($scope.newProfTagName.length > 2 && $scope.duplicateProfessionalTag == true) {
                    $scope.duplicateProfessionalTag = false;
                }
            });
  
            $scope.$watch("newPersTagName", function() {
                if ($scope.newPersTagName.length > 2 && $scope.duplicatePersonalTag == true) {
                    $scope.duplicatePersonalTag = false;
                }
            });

            
            /*
            $scope.$watch("newJobTagName", function() {
                if ($scope.newJobTagName.length > 2 && $scope.duplicateJobTag == true) {
                    $scope.duplicateJobTag = false;
                }
            });
            */
            
            $scope.$watch("newCompanyTagName", function() {
                if ($scope.newCompanyTagName.length > 2 && $scope.duplicateCompanyTag == true) {
                    $scope.duplicateCompanyTag = false;
                }
            });


            $scope.$watch('userChanges.newExperience', function (newVal, oldVal) {
                if (newVal) {
                    //$scope.endDateMinDate = newVal;
                    console.log("watch here: userChanges.newExperience has changed");
                    $scope.$broadcast('refreshDatepickers');
                }
            });

            $scope.deleteKeyword = function($tagId) {

                $scope.userChanges.tagToDelete = $tagId;

                var data = $scope.userChanges;
                data['_token'] = $('meta[name="csrf-token"]').attr('content');

                $http.post("/profile/edit/info", data)
                    .then(function successCallback(response) {
                        //getting the new user data to update the scope
                        $http.get('api/user/' + $routeParams.id).then(function successCallback(response){
                            $scope.user.tags = response.data.tags;
                            //Clear the user changes to avoid tag repetition
                            if( $scope.userChanges.tagToDelete){
                                delete $scope.userChanges.tagToDelete;
                            }
                        }, function errorCallback(response){
                            console.log("Awww error in the get! " + response);
                        });
                    },function errorCallback(response) {
                        console.log("Awww update error!");
                        console.log("error: " + response);
                    }
                );
            }

            // --- END KEYWORDS

            // --- Add experience & education

            $scope.showAddExperience = false;

            $scope.status = {
                startingDateOpened: false,
                endingDateOpened: false
            };

            // this is called to access the calendar scope
            $scope.parent = { addStartingDate : '', addStartingDate : '' };

            $scope.openEditEducation = function(educationId){

                // Close the other edit forms and open the one actively clicked
                $("div" + ".editEducationForm").attr("hidden","true");
                $("div" + "#" + educationId + ".editEducationForm").prop("hidden", null);
                
                var eduEntry = $("div" + ".row .educationEntry");
                var catchMyParent = $("div" + ".row .educationEntry").parent();
                catchMyParent.removeClass("ng-hide");

                var eduEntryId = $("div#" + educationId + ".row.educationEntry").parent();
                eduEntryId.addClass("ng-hide");
            };

            $scope.startingDateOpen = function($event) {
                //console.log($event);
                $scope.status.startingDateOpened = true;
            };

            $scope.endingDateOpen = function($event) {
                $scope.status.endingDateOpened = true;
            };            

            $scope.addExperience = function($type, $title, $description, $startingDate, $endingDate, $isCurrent) {
                $scope.education = {};
                $scope.experience = {};

                $startingDate = $filter('date')($startingDate, "yyyy/MM/dd");

                if($isCurrent){
                    $endingDate = null;
                } else {
                    $endingDate = $filter('date')($endingDate, "yyyy/MM/dd");
                }
                
                $scope.userChanges.newExperience = {
                    experience_type_id : $type,
                    title: $title,
                    description: $description,
                    started_at: $startingDate,
                    ended_at: $endingDate
                }

                //posting the user changes
                var data = $scope.userChanges;
                data['_token'] = $('meta[name="csrf-token"]').attr('content');

                $http.post("/profile/add/info", data).then(
                    function successCallback(response) {
                        $scope.user = $scope.userChanges;
                        $scope.showEditInfoForm = false;

                        $http.get('api/user/' + $routeParams.id).then(
                            function successCallback(response) {
                                
                                $scope.user.experiences = angular.copy(response.data.experiences);

                                $scope.userChanges.experiences = angular.copy($scope.user.experiences);

                                //Clear the user changes to avoid entry repetition
                                if( $scope.userChanges.newExperience ){
                                    delete $scope.userChanges.newExperience;
                                }

                                $scope.parent.addStartingDate = new Date();
                                $scope.parent.addEndingDate = new Date();
                                $scope.isCurrentEducation = null;
                                
                        }, function errorCallback(response){
                            console.log("Awww Error!" + response);
                        });
                    }, //error callback
                    function errorCallback(response) {
                        console.log("error: " + response);
                    }
                );
                /* Resetting the global variables */
                // to be able to open add experience/education again, not working otherwise
                $type == 1 ? $scope.showAddExperience = false : $scope.showAddEducation = false;

            }

            $scope.editExperience = function($id, $type, $title, $description, $startingDate, $endingDate, $startedAt, $endedAt, $isCurrent) {

                console.log("Edit experience here...");

                //starting date
                if ( $startingDate )  {
                    $startingDate = $filter('date')($startingDate, "yyyy/MM/dd");
                } else {
                    $startingDate = $filter('date')($startedAt, "yyyy/MM/dd");
                }
                

                //ending date
                if ($endedAt && !$endingDate) {

                }


                /*
                if($isCurrent){
                    $endingDate = null;
                } else {
                    $endingDate = $filter('date')($endingDate, "yyyy/MM/dd");
                }
                */


                console.log("startingDate: ");
                console.log($startingDate);

                console.log("endingDate: ");
                console.log($endingDate);

                var experiences = $scope.userChanges.experiences;
                //filter the array
                var foundItem = $filter('filter')(experiences, { id: $id  }, true)[0];
                //get the index
                var index = experiences.indexOf(foundItem);

                $scope.userChanges.experience = {
                    id: $id,
                    experience_type_id: $type,
                    title: $title,
                    description: $description,
                    started_at: $startingDate,
                    ended_at: $endingDate
                }

                $scope.update();
                
                $http.get('api/user/' + $routeParams.id).success(function (data) {
                    $scope.user.experiences = angular.copy(data.experiences);
                    $scope.userChanges.experiences = angular.copy($scope.user.experiences);
                });
            };

            $scope.toggleEditEducationEnd = function() {
                console.log("toggle edit education end");

                if( $scope.parent.editEndingDate ) {
                    $scope.parent.editEndingDate = null;
                } else {
                    $scope.parent.editEndingDate = new Date();
                }
            };

            $scope.deleteExperience = function($experienceId) {

                $scope.userChanges.experienceToDelete = $experienceId;

                var experiences = $scope.userChanges.experiences;

                // --- filter the array
                var foundItem = $filter('filter')(experiences, { id: $experienceId  }, true)[0];
                
                // --- get the index
                var index = experiences.indexOf(foundItem);

                // --- Delete the object from the array
                $scope.userChanges.experiences.splice(index, 1);

                $scope.update();
            }

            // --- Update information from the profile page
            $scope.update = function () {

                console.log("In the update function, data to be posted:");

                var data = $scope.userChanges;
                data['_token'] = $('meta[name="csrf-token"]').attr('content');

                console.log(data);

                $http.post("/profile/edit/info", data)
                    .then(
                    function (response) {
                        console.log("Update Success!");
                        if (response.data.success) {
                            $scope.user = $scope.userChanges;
                        } else {
                            console.log(data.errors)
                        }
                        return response;
                    },
                    function (error) {
                        console.log("Awww update error!");
                        console.log("error: " + error);
                    }
                );
            };
 
            $scope.getCategories();
            $scope.getSubCategories();
            $scope.getCountries();
            $scope.getLanguages();


            $scope.resetUserData = function () {
                $scope.userChanges = angular.copy($scope.user);
                $scope.languages = $scope.allLanguages;

                angular.forEach($scope.allCategories, function (value, index) {
                    if (value.id == $scope.user.category_id) {
                        $scope.user.category = value;
                        $scope.userChanges.category = value;
                    }  
                }); 

                /* Initializes the languages selection with user's registration data, if any */
                angular.forEach($scope.user.languages, function (userLanguageObj, userLanguageIndex) {
                    angular.forEach($scope.languages, function (dBlanguageObj, dBlanguageIndex) {                        
                        if($scope.languages[dBlanguageIndex].id == $scope.user.languages[userLanguageIndex].id){
                            $scope.languages[dBlanguageIndex].ticked = true; 
                        }
                    });
                });                

                /* Initializes the subcategory selection with user's registration data, if any */
                angular.forEach($scope.user.category.sub_categories, function (value1, index1) {
                    angular.forEach($scope.userChanges.sub_categories, function (value2, index2) {
                        if (value2.id == value1.id) {
                            $scope.userChanges.sub_categories[index2] = value1;
                            $scope.userChanges.sub_categories[index2].ticked = true;                        
                        }
                    });
                });

            }


            $scope.submitEditForm = function() {
                $scope.update();
                $scope.showEditInfoForm = false;
            }

            $scope.showingPersonalKeywords = false;
            // --- Display keywords

            $scope.getSuggestions = function (value, type) {
                return $http.post("/search/suggest",
                    {
                        "_token": $('meta[name="csrf-token"]').attr('content'),
                        "text": value,
                        "type": type
                    })
                    .then(
                    function (response) {
                        return response.data.suggestions[0].options;
                    },
                    function (error) {
                        console.log("error: " + error);
                    }
                );
            };

            $scope.upload = function (userId, file, url) {
                $scope.loadingVideoUpload = true;
                Upload.upload({
                    user: userId,
                    url: url,
                    data: {file: file}
                }).then(function (resp) {
                    $http.get('api/user/' + $routeParams.id).success(function (data) {
                        $scope.user = angular.copy(data);
                        $scope.resetUserData();

                        angular.forEach($scope.user.videos, function(video, key){
                            var extension = video.value.split("?")[0].split(".").pop();

                            $scope.config.sources.push({
                                src: $sce.trustAsResourceUrl(video.value),
                                type: "video/" + extension
                            })
                        });
                        $scope.loadingVideoUpload = false;
                        console.log($scope.user.videos);
                        $scope.user_loaded = true;
                    });

                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                }, function (evt) {
                    //var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            };

            // Datepicker Scope

            $scope.hackClearDatepicker = function() {
                console.log("hacking date with jquery");
                $('#inputDatepickStartEdu').val('');
                $('#inputDatepickEndEdu').val('');
               $scope.startingDate = null;
               $scope.endingDate = null;
            };

            $scope.today = function($event){
                console.log("today clicked!");
                console.log($event);
                $scope.startingDate = new Date();
                $scope.endingDate = new Date();
                $scope.addStartingDate = new Date();
                $scope.addEndingDate = new Date();

                $scope.parent.addStartingDate = new Date();
                $scope.parent.addEndingDate = new Date();
            };

            $scope.today();

            $scope.clear = function(){
               $scope.startingDate = null;
               $scope.endingDate = null;
            };

            $scope.open =  function($event) {
               $scope.status.opened = true;
            };

            $scope.dateOptions = {
               formatYear: 'yy',
               startingDate: 1
            };

            $scope.format = 'yyyy/MM/dd';

            $scope.getDayClass = function(date, mode) {
                if (mode === 'month') {
                  var MonthToCheck = new Date(date).setHours(0,0,0,0);

                  for (var i=0;i<$scope.events.length;i++){
                    var currentMonth = new Date($scope.events[i].date).setHours(0,0,0,0);

                    if (MonthToCheck === currentMonth) {
                      return $scope.events[i].status;
                    }
                  }
                }
                return '';
            };
}]);

/**
 * Decorates the ui-bootstrap datepicker directive's controller to allow
 * refreshing the datepicker view (and rerunning invalid dates function) 
 * upon an event trigger: `$scope.$broadcast('refreshDatepickers');`
 * 
 * Works with inline and popup. Include this after `ui.bootstrap` js
 */
 /*
angular.module('ui.bootstrap.datepicker')
    .config(function($provide) {
        $provide.decorator('uibDatepickerDirective', function($delegate) {
            var directive = $delegate[0];
            var link = directive.link;

            directive.compile = function() {
                return function(scope, element, attrs, ctrls) {
                    link.apply(this, arguments);

                    var datepickerCtrl = ctrls[0];
                    var ngModelCtrl = ctrls[1];

                    if (ngModelCtrl) {
                        // Listen for 'refreshDatepickers' event...
                        scope.$on('refreshDatepickers', function refreshView() {
                            console.log("refreshDatepickers called!");
                            datepickerCtrl.refreshView();
                        });
                    }
                }
            };
            return $delegate;
        });
    });
*/