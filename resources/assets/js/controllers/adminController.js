app.controller('adminCtrl', 
	['$scope', '$route', '$http', '$controller', '$sce', '$window','$compile', 'Upload',
		function ($scope, $route, $http, $controller, $sce, $window, $compile, Upload) {

            $scope.upload_loaded = false;

            $scope.upload = function (userId, file, url) {
            	$scope.loadingVideoUpload = true;
                Upload.upload({
                    url: url,
                    data: {
                        user: userId,
                        file: file
                    }
                }).then(function (resp) {
                    console.log(resp);

                    $scope.upload_loaded = true;
                    $scope.loadingVideoUpload = false;

                    //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                }, function (resp) {
                    //console.log('Error status: ' + resp.status);
                }, function (evt) {
                    //var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            }


            // --- Delete Video
            $scope.deleteVideo = function($videoId) {

                var data = $videoId;
                data['_token'] = $('meta[name="csrf-token"]').attr('content');

                $http.post("admin/delete/video", data)
                    .then(
                    function (response) {
                        console.log("Update Success!");
                        if (response.data.success) {
                            console.log('video deleted')
                        } else {
                            console.log(data.errors)
                        }
                        return response;
                    },
                    function (error) {
                        console.log("Awww update error!");
                        return error;
                    }
                );

            }

		}]);