angular.module('cvideon').controller('searchSeekers', ['$scope', '$http', '$sce', '$controller', function ($scope, $http, $sce, $controller) {

    /***
      * * * Expand the main search controller * * *
                                                 **

    // Commented out because you can set multiple controllers in the view
    
    // Providing the scope, and using (true) in order to create an isolated scope.
    var theSearchCtrl = $scope.$new();

    // In this case it is the child scope of this scope.
    $controller('searchCtrl', { $scope : theSearchCtrl });

    // And call the method on the newScope.
    theSearchCtrl.myMethod(); */


    // -------> Random results

    $scope.getRandomProfiles = function () {
        $http({
            method: 'GET',
            url: 'random/user/profiles'
            }).then(function (response) {
            $scope.searchResults = response.data;
            }, function (response) {
            console.log(response)
        });               
    }

    $scope.getRandom = $scope.getRandomProfiles();

    // --> Display only the first name of a Seeker
    $scope.getFirstName = function (str) {
        if (str.indexOf(' ') === -1)
            return str;
        else
            return str.substr(0, str.indexOf(' '));
    };

    $scope.onSelect = function ($item, $model, $label) {
        $scope.searchinput = $item;
        $scope.searchSeekers();
    };

    $scope.getSuggestions = function (value, type) {
        return $http.post("/search/suggest",
            {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                "text": value,
                "type": type
            })
            .then(
            function (response) {
                return response.data.suggestions[0].options;
            },
            function (error) {
                console.log("error: " + error);
            }
        );
    };
    
    $scope.$watch('category', function (value) {
        $scope.shownCategory = value;
        $scope.subcategories = [];
    }, true);
    
    // --> The search for seekers function
    $scope.searchSeekers = function () {

        var searchForLanguages = [];
        var searchForTags = [];

        var data = {
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "text": $scope.searchinput,
        }; 

        if ($scope.shownCategory != undefined)
            data.category_id = $scope.shownCategory;

        if ($scope.subcategories.length)
            data.subcategories = $scope.subcategories;

        if ($scope.country != undefined)
            data.country_id = $scope.country;

        if ($scope.search.languages.length) {
            angular.forEach($scope.search.languages, function (languageObj, languageIndex) {
                searchForLanguages.push($scope.search.languages[languageIndex].id); 
            }); 
            data.languages = searchForLanguages;
        }

        if ($scope.search.personalTags.length || $scope.search.professionalTags.length) {

            angular.forEach($scope.search.personalTags, function (personalTagObj, personalTagIndex) {
                searchForTags.push($scope.search.personalTags[personalTagIndex].id); 
            }); 

            angular.forEach($scope.search.professionalTags, function (professionalTagObj, professionalTagIndex) {
                searchForTags.push($scope.search.professionalTags[professionalTagIndex].id); 
            }); 
        
            data.tags = searchForTags;
 
        }

        data.onlyWithVideos = $scope.videoFilterModel;

        return $http.post("/search/search-users", data)
            .then(
            function (response) { 
                $scope.searchResults = response.data;
                console.log($scope.searchResults);
                $scope.zeroMatches = response.data.length > 0;
                return response.data;
            },
            function (error) {
                console.log("error: " + error);
            }
        );
    };
}]);