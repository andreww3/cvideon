angular.module('cvideon').controller('searchCtrl', ['$scope', '$http', '$sce', function ($scope, $http, $sce, $controller) {
    
    $scope.categories = undefined;
    $scope.category = undefined;
    $scope.shownCategory = undefined;
    $scope.searchinput = undefined;
    $scope.subcategories = [];
    $scope.videoFilterModel = true;
    $scope.selectedResult = undefined;
    $scope.zeroMatches = true;
    $scope.message = []; 

    $scope.config = {
        sources: [],
        preload: "none",
        theme: "/css/custom.css",
    };

    $http({
        method: 'GET',
        url: '/api/category/all-categories'
    }).then(function (response) {
        $scope.categories = response.data;
    }, function (response) {
        console.log(response)
    });


    $http({
        method: 'GET',
        url: '/api/countries'
    }).then(function (response) {
        $scope.countries = response.data;
    }, function (response) {
        console.log(response)
    });

    $http({
        method: 'GET',
        url: 'api/languages'
    }).then(function (response) {
        $scope.languages = response.data;
    }, function (response) {
        console.log(response)
    });


    $http({
        method: 'GET',
        url: 'api/personalTags'
    }).then(function (response) {
        $scope.personalTags = response.data;
    }, function (response) {
        console.log(response)
    });

    $http({
        method: 'GET',
        url: 'api/professionalTags'
    }).then(function (response) {
        $scope.professionalTags = response.data;
    }, function (response) {
        console.log(response)
    });


    // --> Display only the first name
    $scope.getFirstName = function (str) {
        if (str.indexOf(' ') === -1)
            return str;
        else
            return str.substr(0, str.indexOf(' '));
    };
 
    // --> Open only contact 
    $scope.openOnlyContact = function (receiver) {

        $scope.contactOnly = true;        
        $scope.receiver = angular.copy(receiver);
        $scope.openContact($scope.receiver);

    };

    // --> Open contact with user data
    $scope.openContact = function(receiver){

        $scope.receiver = angular.copy(receiver);

        $scope.sendMessageToUser = true;

        $http.get('/showLoggedInUser').success(function (data) {
            $scope.loggedInUser = angular.copy(data);
        }).then(function (response) {

            var loggedInUserDataLink = '/api/user/' + $scope.loggedInUser;
            loggedInUserDataLink = loggedInUserDataLink.replace(/[\s]/g, '');
            $http.get(loggedInUserDataLink).success(function (data) {
                $scope.user = angular.copy(data);

                $scope.message = {
                    receiver: {
                        type: $scope.receiver.userable_type,
                        id: $scope.receiver.id,
                        name: $scope.receiver.name,
                        email: $scope.receiver.email
                    },
                    sender: {
                        type: $scope.user.userable_type,
                        id: $scope.user.id,
                        name: $scope.user.name,
                        email: $scope.user.email
                    },
                    subject: '',
                    message: '',
                    company:''
                }
                console.log($scope.message)
            });
                  
        }); 


    };


    // --> Contact user function
    $scope.contactUser = function () {

        var data = $scope.message;

        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        console.log(data);
        
        $http.post("/sendMessage", data)
            .then(
            function (response) {
                console.log("Success!");
                return response;
            },
            function (error) {
                console.log("Awww error!");
                console.log("error: " + error);
            }
        );

        $scope.messageSent = true;
    };
    // --> Check for checked subcategories
    $scope.check = function (value, checked) {
        var idx = $scope.subcategories.indexOf(value);
        if (idx >= 0 && !checked) {
            $scope.subcategories.splice(idx, 1);
        }
        if (idx < 0 && checked) {
            $scope.subcategories.push(value);
        }
    };  


    $scope.selectResult = function (obj) {
        
        $scope.config.sources = [];

        angular.forEach(obj.videos, function(video, key){
            var extension = video.value.split("?")[0].split(".").pop();

            $scope.config.sources.push({
                src: $sce.trustAsResourceUrl(video.value),
                type: "video/" + extension
            })
        });

        $scope.selectedResult = obj;
    };

}]);