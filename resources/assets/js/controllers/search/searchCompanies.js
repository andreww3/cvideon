angular.module('cvideon').controller('searchCompanies', ['$scope', '$http', '$sce', '$controller', function ($scope, $http, $sce, $controller) {

    /***
      * * * Expand the main search controller * * *
                                                 **
    // Commented out because you can set multiple controllers in the view
    
    // Providing the scope, and using (true) in order to create an isolated scope.
    var theSearchCtrl = $scope.$new(true);

    // In this case it is the child scope of this scope.
    $controller('searchCtrl',{$scope : theSearchCtrl });

    // And call the method on the newScope.
    theSearchCtrl.myMethod(); 
    */

    
    // -------> Random results
    $scope.getRandomProfiles = function () {
        $http({
            method: 'GET',
            url: 'random/company/profiles'
        	}).then(function (response) {
            $scope.searchResults = response.data;
        	}, function (response) { 
            console.log(response)
        });               
    }

    $scope.getRandom = $scope.getRandomProfiles();

    $scope.$watch('category', function (value) {
        $scope.shownCategory = value;
        $scope.subcategories = [];
    }, true);
    

    $scope.onSelect = function ($item, $model, $label) {
        $scope.searchinput = $item;
        $scope.searchCompanies();
    };

    $scope.getSuggestions = function (value, type) {
        return $http.post("/search/suggest",
            {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                "text": value,
                "type": type
            })
            .then(
            function (response) {
                return response.data.suggestions[0].options;
            },
            function (error) {
                console.log("error: " + error);
            }
        );
    };

    
    // --> The search for companies function
    $scope.searchCompanies = function () {

        var searchForTags = [];

        var data = {
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "text": $scope.searchinput,
        }; 

        if ($scope.shownCategory != undefined)
            data.category_id = $scope.shownCategory;

        if ($scope.subcategories.length)
            data.subcategories = $scope.subcategories;

        if ($scope.country != undefined)
            data.country_id = $scope.country;


        if ($scope.search.companyTags.length) {

            angular.forEach($scope.search.companyTags, function (companyTagObj, companyTagIndex) {
                searchForTags.push($scope.search.companyTags[companyTagIndex].id); 
            }); 
        
            data.tags = searchForTags;
 
        } 

        data.onlyWithVideos = $scope.videoFilterModel;
        
        return $http.post("/search/search-companies", data)
            .then(
            function (response) { 
                $scope.searchResults = response.data;
                console.log($scope.searchResults);
                $scope.zeroMatches = response.data.length > 0;
                return response.data;
            },
            function (error) {
                console.log("error: " + error);
            }
        );
    };

}]);