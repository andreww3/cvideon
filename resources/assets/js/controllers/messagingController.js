angular.module('cvideon').controller('messagingCtrl', ['$scope', '$http', '$sce', '$controller', function ($scope, $http, $sce, $controller) {

    /* *
      * * * * * Messaging Controller * * * * *
                                            * */

    $scope.unreadMessages = 0;
    
    // --> get default in user id
    $scope.getLoggedInUser = function () {

        $http.get('/showLoggedInUser').success(function(data) {
            $scope.loggedInUser = angular.copy(data);             

            var loggedInUser = $scope.loggedInUser;
            
            // --> select default message
            $scope.replying = undefined;

            $scope.getLastMessage(loggedInUser);

            $scope.getUnreadMessages(loggedInUser);

        });
    }
    
    $scope.getLoggedInUser(); 

    // --> get number of unread Messages
    $scope.getUnreadMessages = function (loggedInUser) {

        $http.post('messages/getUnreadMessages', loggedInUser).success(function(data) {
            $scope.unreadMessages = angular.copy(data);             

        });
    }

    // --> get the last unread message
    $scope.getLastMessage = function (loggedInUser) {
        $http.post('messages/getLastMessage', loggedInUser).success(
            function (data) {

                if (data == 0) {
                    console.log('No messages');

                    $scope.noReceivedMessages = true;

                } else {

                    $scope.selectedMessage = angular.copy(data);

                    $scope.openInbox();
                }

            },
            function (error) {
                console.log("error: " + error);
        });
    }

    // --> Open Inbox
    $scope.openInbox = function () {

    	$scope.showInboxMessages = true;

    	$http.post('messages/getInbox', $scope.loggedInUser).success(

            function (data) {

                if (data == 0) {

                    console.log('No messages');

                    $scope.noInboxMessages = true;

                } else {

                    $scope.received = angular.copy(data);

                }

            },
            function (error) {
                console.log("error: " + error);
        });
              	
    }  

    // --> Open Sentbox
    $scope.openSent = function () {
    	$scope.showSentMessages = true;


        $http.post('messages/getSent', $scope.loggedInUser).success(
            
            function (data) {
                console.log(data);

                if (data == 0) {

                    console.log('No messages');

                    $scope.noSentMessages = true;

                } else {

                    $scope.sent = angular.copy(data);
                    
                }

            },
            function (error) {
                console.log("error: " + error);
        });                	

    }

    /* *
      * * * * * Selected Message * * * * *
                                        * */

    // --> View selected message
    $scope.selectMessage = function (message) {

        $scope.selectedMessage = angular.copy(message);

        if ($scope.noReceivedMessages == true) {
            $scope.noReceivedMessages = false;  
        }
    }


    // --> Mark message as read
    $scope.markAsRead = function (messageId) {
        
        $http.post('read/message', messageId).success( function(data) {
            $scope.selectedMessage = angular.copy(data);

            $scope.openInbox();
            $scope.getUnreadMessages($scope.loggedInUser);
        });       

    }

    // --> Reply to Message
    $scope.reply = function (messageId) {
        
        $http.post('message/getUsers', messageId).success( function(data) {
            $scope.replying = true;
            $scope.messageInfo = data;
        });     

    }

    // --> Send reply to Message
    $scope.sendReply = function() {

        var data = $scope.messageInfo;
        console.log(data);
        
        $http.post("/sendMessage", data)
            .then( //success callback
            function (response) {
                console.log(response);
                if (response.data.success) {
                    $scope.messageSent = true;
                    console.log('Message Sent!');
                    $scope.messageInfo = {};         
  
                    $scope.replying = false;
                }
                else {
                    console.log(response.data);
                }
                return response;
            }, //error callback
            function (error) {
                console.log("error: " + error);
            }
        );

    }
        

    // --> Close reply to Message
    $scope.closeReply = function () {

        $scope.messageInfo = {};
        $scope.replying = false;

    }

    // --> Delete message
    $scope.delete = function (messageId, userId) {
        
        $http.post('delete/message', messageId).success( function(data) {
            console.log(data);
            $scope.getLastMessage(userId);

        });
    }

}]);
