var gulp = require('gulp');
var rename = require('gulp-rename');
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var paths = {
    'bootstrap': './vendor/bower_components/bootstrap-sass/assets/',
    'fontawesome': './vendor/bower_components/font-awesome/',
    'jquery': './vendor/bower_components/jquery/',
    'angular': './vendor/bower_components/angular/',
    'angularui': './vendor/bower_components/angular-bootstrap/',
    'angulararia': './vendor/bower_components/angular-aria/',
    'angularanimate': './vendor/bower_components/angular-animate/',
    'angularmaterial': './vendor/bower_components/angular-material/',
    'angularroute': './vendor/bower_components/angular-route/',
    'angularsanitize': './vendor/bower_components/angular-sanitize/',
    'angularmultiselect': './vendor/bower_components/isteven-angular-multiselect/',
    'moment': './vendor/bower_components/moment/',
    'angularmoment': './vendor/bower_components/angular-moment/',
    'checklist': './vendor/bower_components/checklist-model/',
    'fileupload': './vendor/bower_components/ng-file-upload/',
    'angularimagecropper': './vendor/bower_components/angular-img-cropper/dist/',
    'videogular': './vendor/bower_components/videogular/',
    'videogularthemesdefault': './vendor/bower_components/videogular-themes-default/',
    'videogularbuffering': './vendor/bower_components/videogular-buffering/',
    'videogularcontrols': './vendor/bower_components/videogular-controls/',
    'videogularoverlayplay': './vendor/bower_components/videogular-overlay-play/',
    'modernizr': './vendor/bower_components/modernizr/',
    'datatables': './vendor/bower_components/datatables/',
}

elixir(function (mix) {
    mix.styles([
        'main.css',
        paths.videogularthemesdefault + 'videogular.css',
        paths.angularmaterial + 'angular-material.css',
        paths.angularmultiselect + 'isteven-multi-select.css'
    ], 'public/css/custom.css')
        .sass('app.scss', 'public/css/', {
            includePaths: [
                paths.bootstrap + 'stylesheets/',
                paths.fontawesome + 'scss/'
            ]
        }) 
        .copy(paths.bootstrap + 'fonts/bootstrap/**', 'public/fonts/bootstrap')
        .copy(paths.fontawesome + 'fonts/**', 'public/fonts')
        .copy(paths.videogularthemesdefault + 'fonts/**', 'public/css/fonts')
        .scriptsIn('resources/assets/js', 'public/js/cvideon.js')
        .scripts([
            //'./public/assets/js/admin.js',
            
            paths.jquery + 'dist/jquery.js',
            paths.bootstrap + 'javascripts/bootstrap.js',
            paths.angular + 'angular.js',
            paths.angularui + 'ui-bootstrap.js',
            paths.angularui + 'ui-bootstrap-tpls.js',
            paths.angularsanitize + 'angular-sanitize.js',
            paths.angulararia + 'angular-aria.js',
            paths.angularanimate + 'angular-animate.js',
            paths.angularmaterial + 'angular-material.js',
            paths.angularmultiselect + 'isteven-multi-select.js',
            paths.angularroute + 'angular-route.js',
            paths.moment + 'min/moment-with-locales.js',
            paths.modernizr + 'modernizr.js',
            paths.angularmoment + 'angular-moment.js',
            paths.checklist + 'checklist-model.js',
            paths.fileupload + 'ng-file-upload-all.js',
            paths.angularimagecropper + 'angular-img-cropper.min.js',
            paths.videogular + 'videogular.js',
            paths.videogularbuffering + 'vg-buffering.js',
            paths.videogularcontrols + 'vg-controls.js',
            paths.videogularoverlayplay + 'vg-overlay-play.js',
            /*
            paths.datatables + '/media/js/jquery.dataTables.js',
            paths.datatables + '/media/js/dataTables.bootstrap.js'
            */
        ], 'public/js/app.js');

    mix.version(['css/custom.css', 'js/cvideon.js'])
        .copy(paths.bootstrap + 'fonts/bootstrap/**', 'public/build/fonts/bootstrap')
        .copy(paths.fontawesome + 'fonts/**', 'public/build/fonts')
        .copy(paths.videogularthemesdefault + 'fonts/**', 'public/build/css/fonts')
});

/* Copy any needed files.
 *
 * Do a 'gulp copyfiles' after bower updates
 */
 
gulp.task("copyfiles", function() {

  // Copy jQuery, Bootstrap, and FontAwesome
  gulp.src("vendor/bower_components/jquery/dist/jquery.js")
      .pipe(gulp.dest("resources/assets/blog/js/"));


  gulp.src("vendor/bower_components/bootstrap/dist/js/bootstrap.js")
      .pipe(gulp.dest("resources/assets/blog/js/"));

  gulp.src("vendor/bower_components/bootstrap/less/**")
      .pipe(gulp.dest("resources/assets/less/bootstrap"));

  gulp.src("vendor/bower_components/bootstrap/dist/fonts/**")
      .pipe(gulp.dest("public/assets/fonts"));

  gulp.src("vendor/bower_components/fontawesome/less/**")
      .pipe(gulp.dest("resources/assets/less/fontawesome"));

  gulp.src("vendor/bower_components/fontawesome/fonts/**")
      .pipe(gulp.dest("public/assets/fonts"));


  // Copy datatables
  var dtDir = 'vendor/bower_components/datatables-plugins/integration/';

  gulp.src("vendor/bower_components/datatables/media/js/jquery.dataTables.js")
      .pipe(gulp.dest('resources/assets/blog/js/'));

  gulp.src(dtDir + 'bootstrap/3/dataTables.bootstrap.css')
      .pipe(rename('dataTables.bootstrap.less'))
      .pipe(gulp.dest('resources/assets/less/others/'));

  gulp.src(dtDir + 'bootstrap/3/dataTables.bootstrap.js')
      .pipe(gulp.dest('resources/assets/blog/js/'));


  // Copy selectize
  gulp.src("vendor/bower_components/selectize/dist/css/**")
      .pipe(gulp.dest("public/assets/selectize/css"));

  gulp.src("vendor/bower_components/selectize/dist/js/standalone/selectize.min.js")
      .pipe(gulp.dest("public/assets/selectize/"));


  // Copy pickadate
  gulp.src("vendor/bower_components/pickadate/lib/compressed/themes/**")
      .pipe(gulp.dest("public/assets/pickadate/themes/"));

  gulp.src("vendor/bower_components/pickadate/lib/compressed/picker.js")
      .pipe(gulp.dest("public/assets/pickadate/"));

  gulp.src("vendor/bower_components/pickadate/lib/compressed/picker.date.js")
      .pipe(gulp.dest("public/assets/pickadate/"));

  gulp.src("vendor/bower_components/pickadate/lib/compressed/picker.time.js")
      .pipe(gulp.dest("public/assets/pickadate/"));

});

/**
 * Default gulp is to run this elixir stuff
 */

elixir(function(mix) {

  // Combine scripts
  mix.scripts([
      './public/js/app.js',
      './vendor/bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
      './vendor/bower_components/datatables/media/js/jquery.dataTables.js',
      './vendor/bower_components/datatables/media/js/dataTables.bootstrap.js',
      
    ],
    'public/assets/js/admin.js',
    'resources/assets'
);

  // Compile Less
  mix.less('admin.less', 'public/assets/css/admin.css');

}); 

