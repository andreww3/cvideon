<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_id')->index();
            $table->boolean('is_active');
            $table->boolean('is_admin');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });

        Schema::create('categoryables', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sub_category_id')->index();
            $table->morphs('categoryable');

            $table->foreign('sub_category_id')->references('id')->on('sub_categories');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categoryables');
        Schema::drop('company_users');
    }
}
